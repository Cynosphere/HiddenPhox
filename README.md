# HiddenPhox
HiddenPhox is a kitchen sink bot of sorts for things I or friends want.

## The future of this project
The original bot account is being sunsetted due to Message Content intents. I do not have a unique enough use case to get Message Content intent nor as much motivation to throw away work just to move to Discord's slash command system that should've been in place from the very beginning of bot support. I want to continue work on this branch (`rewrite`) with the framework I already have put in place.

**From now on the bot is completely private from being invited.** If you absolutely need the invite, shoot me an email if you're not a close friend, otherwise just DM me on Discord or elsewhere you have contact with me.

It should also be obvious that the `master` branch is fully out of support, and the `slash-commands` branch will probably never have work done.

RIP HiddenPhox#0240 (`152172984373608449`), 2016-2022
