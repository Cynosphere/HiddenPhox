hf.database.run("CREATE TABLE IF NOT EXISTS user_data (key TEXT PRIMARY KEY, value TEXT NOT NULL) WITHOUT ROWID");

function setUserData(id, key, value) {
  return new Promise((resolve, reject) => {
    hf.database.run(
      "REPLACE INTO user_data VALUES ($key,$value)",
      {
        $value: value,
        $key: `${id}[${key}]`,
      },
      (err) => {
        if (err == null) {
          resolve(true);
        } else {
          reject(err);
        }
      }
    );
  });
}

function getUserData(id, key, fallback = null) {
  return new Promise((resolve, reject) => {
    hf.database.get(
      "SELECT value FROM user_data WHERE key = $key",
      {
        $key: `${id}[${key}]`,
      },
      (err, row) => {
        if (err == null) {
          resolve(row?.value || fallback);
        } else {
          reject(err);
        }
      }
    );
  });
}

function deleteUserData(id, key) {
  return new Promise((resolve, reject) => {
    hf.database.run(
      "DELETE FROM user_data WHERE key = $key",
      {
        $key: `${id}[${key}]`,
      },
      (err) => {
        if (err == null) {
          resolve(true);
        } else {
          reject(err);
        }
      }
    );
  });
}

module.exports = {
  setUserData,
  getUserData,
  deleteUserData,
};
