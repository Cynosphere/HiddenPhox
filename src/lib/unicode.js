const mappings = {};

async function cacheList() {
  const data = await fetch(
    "https://www.unicode.org/Public/UNIDATA/UnicodeData.txt"
  ).then((res) => res.text());

  data
    .split("\n")
    .map((line) => line.split(";").splice(0, 2))
    .forEach(([character, description]) => {
      if (character != "") mappings[character.toLowerCase()] = description;
    });

  global.____unicode_data = mappings;
}

async function getNamesFromString(string) {
  if (!global.____unicode_data) await cacheList();

  const codes = [...string].map((char) => char.codePointAt().toString(16));

  return codes.map((code) => [
    code.padStart(4, "0"),
    global.____unicode_data[code.padStart(4, "0")],
  ]);
}

module.exports = {
  cacheList,
  getNamesFromString,
};
