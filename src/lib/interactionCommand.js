const {ApplicationCommandTypes, ApplicationCommandOptionTypes} = require("../util/dconstants.js");

class InteractionCommand {
  constructor(name) {
    this.name = name;
    this.type = ApplicationCommandTypes.CHAT_INPUT;
    this.helpText = "No description provided.";
    this.guildOnly = false;
    this.options = {
      send: {
        type: ApplicationCommandOptionTypes.BOOLEAN,
        name: "send",
        description: "Should the output be sent normally or ephemeral",
        required: false,
        default: true,
      },
    };
  }

  getOption(interaction, key) {
    return interaction.data.options?.find((o) => o.name === key)?.value ?? this.options[key].default;
  }

  callback() {
    return "Callback not overwritten.";
  }
}

module.exports = InteractionCommand;
