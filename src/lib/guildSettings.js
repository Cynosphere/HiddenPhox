const {getGuildData, setGuildData} = require("./guildData.js");

const flags = Object.freeze({
  codePreviews: 1 << 0,
  tweetUnrolling: 1 << 1,
  fedimbed: 1 << 2,
});

async function getFlags(guildId) {
  const value = await getGuildData(guildId, "settings_flags", 0);

  const out = {};
  for (const key in flags) {
    out[key] = (value & flags[key]) !== 0;
  }

  return out;
}

async function hasFlag(guildId, key) {
  return (
    ((await getGuildData(guildId, "settings_flags", 0)) & flags[key]) !== 0
  );
}

async function enableFlag(guildId, key) {
  let value = await getGuildData(guildId, "settings_flags", 0);

  value |= flags[key];

  await setGuildData(guildId, "settings_flags", value);
}

async function disableFlag(guildId, key) {
  let value = await getGuildData(guildId, "settings_flags", 0);

  value ^= flags[key];

  await setGuildData(guildId, "settings_flags", value);
}

module.exports = {
  flags,
  getFlags,
  hasFlag,
  enableFlag,
  disableFlag,
};
