const ESCAPE = "\x1b";

const NORMAL = "[3";
const BRIGHT = "[9";
const NORMAL_BG = "[4";
const BRIGHT_BG = "[10";

const NAMES = [
  "Black",
  "Red",
  "Green",
  "Yellow",
  "Blue",
  "Magenta",
  "Cyan",
  "White",
];

const COLORS = {};
for (const index in NAMES) {
  COLORS[NAMES[index].toUpperCase()] = index;
}

COLORS.DEFAULT = 9;

function color(name, bright = false, bg = false) {
  name = name.toUpperCase();

  let out = "" + ESCAPE;
  if (bg) {
    if (bright && name != "DEFAULT") {
      out += BRIGHT_BG;
    } else {
      out += NORMAL_BG;
    }
  } else {
    if (bright && name != "DEFAULT") {
      out += BRIGHT;
    } else {
      out += NORMAL;
    }
  }

  out += COLORS[name] + "m";

  return out;
}

const LEVEL_COLORS = {
  silly: ["magenta", true],
  verbose: ["green", true],
  debug: ["cyan", true],
  log: ["blue", false],
  info: ["blue", false],
  warn: ["yellow", false],
  error: ["red", false],
};

function baseLogger(prefix, level, ...message) {
  level = level || "info";

  let out = "";

  const [col, bright] = LEVEL_COLORS[level];
  out += `${color(col, bright)}[${level.toUpperCase()}] `;

  out += `${color("magenta")}[${prefix}] ${color("default")}`;

  out += [...message].map((m) => m?.toString()).join(" ");

  console.log(out);
}

module.exports = {};
for (const level in LEVEL_COLORS) {
  module.exports[level] = (prefix, ...message) =>
    baseLogger.apply(null, [prefix, level, ...message]);
}
