const logger = require("./logger.js");

const timerTable = {};

function getTable() {
  return timerTable;
}

function add(identifier, callback, delay) {
  if (timerTable[identifier] != null) {
    clearInterval(timerTable[identifier]);
  }

  function wrapper() {
    try {
      callback();
    } catch (error) {
      logger.error("hf:timer", `Timer "${identifier}" failed: ${error}`);
    }
  }

  timerTable[identifier] = setInterval(wrapper, delay);
}

function remove(identifier) {
  if (timerTable[identifier] != null) {
    clearInterval(timerTable[identifier]);
  }
}

module.exports = {
  getTable,
  add,
  remove,
};
