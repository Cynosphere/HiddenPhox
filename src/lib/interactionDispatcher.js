const logger = require("./logger.js");
const {MessageFlags} = require("../util/dconstants.js");
const {pastelize, getTopColor} = require("../util/misc.js");

async function runCommand(interaction, command) {
  if (command.ownerOnly && interaction.user.id != hf.config.owner_id) {
    return {
      content: "No\n-# Sent from my iPhone",
      flags: MessageFlags.EPHEMERAL,
    };
  }

  if (command.elevatedOnly && !hf.config.elevated.includes(interaction.user.id)) {
    return {
      content: "No\n-# Sent from my iPhone",
      flags: MessageFlags.EPHEMERAL,
    };
  }

  try {
    const ret = command.callback(interaction);
    if (ret instanceof Promise) {
      return await ret;
    } else {
      return ret;
    }
  } catch (err) {
    logger.error("hf:cmd:" + command.name, err + "\n" + err.stack);
    return {
      content: ":warning: An internal error occurred.",
      flags: MessageFlags.EPHEMERAL,
    };
  }
}

async function InteractionDispatcher(interaction) {
  const command = hf.interactionCommands.get(interaction.data.name);
  const shouldSend = command.getOption(interaction, "send");
  const ack = interaction.acknowledge(shouldSend ? 0 : MessageFlags.EPHEMERAL);

  try {
    const response = await runCommand(interaction, command);
    if (response != null) {
      if (response.file) {
        const newFile = response.file;
        delete response.file;
        if (newFile.contents) {
          newFile.file = newFile.contents;
          delete newFile.contents;
        }
        if (newFile.name) {
          newFile.filename = newFile.name;
          delete newFile.name;
        }
        const files = response.attachments ?? [];
        files.push(newFile);
        response.attachments = files;
      }
      if (response.files) {
        response.attachments = response.files;
        delete response.files;
        for (const attachment of response.attachments) {
          if (attachment.contents) {
            attachment.file = attachment.contents;
            delete attachment.contents;
          }
          if (attachment.name) {
            attachment.filename = attachment.name;
            delete attachment.name;
          }
        }
      }
      if (response.embed) {
        response.embeds = [...(response.embeds ?? []), response.embed];
        delete response.embed;
      }
      if (response.embeds) {
        for (const embed of response.embeds) {
          embed.color = embed.color ?? getTopColor(interaction, hf.bot.user.id, pastelize(hf.bot.user.id));
        }
      }

      try {
        await ack;
        await interaction.createMessage(
          Object.assign(typeof response === "string" ? {content: response} : response, {
            flags: shouldSend ? response.flags : MessageFlags.EPHEMERAL,
            allowedMentions: {
              repliedUser: false,
            },
          })
        );
      } catch (err) {
        await ack;
        await interaction.createMessage({
          content: `:warning: An error has occurred:\n\`\`\`${err}\`\`\``,
          flags: MessageFlags.EPHEMERAL,
          allowedMentions: {
            repliedUser: false,
          },
        });
      }
    }
  } catch (err) {
    await ack;
    await interaction.createMessage({
      content: `:warning: An error has occurred:\n\`\`\`${err}\`\`\``,
      flags: MessageFlags.EPHEMERAL,
      allowedMentions: {
        repliedUser: false,
      },
    });
  }
}

module.exports = {InteractionDispatcher};
