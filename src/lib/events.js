const logger = require("./logger.js");

const eventTable = {};

function getTable() {
  return eventTable;
}

function add(event, identifier, callback) {
  if (eventTable[event] == null) eventTable[event] = {};

  if (eventTable[event][identifier] != null) {
    hf.bot.off(event, eventTable[event][identifier]);
  }

  function wrapper() {
    try {
      callback.apply(this, arguments);
    } catch (error) {
      logger.error(
        "hf:event",
        `Event "${event}:${identifier}" failed: ${error}`
      );
    }
  }

  eventTable[event][identifier] = wrapper;
  hf.bot.on(event, wrapper);
}

function remove(event, identifier) {
  if (eventTable[event] == null) eventTable[event] = {};

  if (eventTable[event][identifier] != null) {
    hf.bot.off(event, eventTable[event][identifier]);
  }
}

module.exports = {
  getTable,
  add,
  remove,
};
