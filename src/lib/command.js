class Command {
  constructor(name) {
    this.name = name;
    this.aliases = [];
    this.helpText = "No description provided.";
    this.category = "unsorted";
    this.guildOnly = false;
  }

  addAlias(alias) {
    this.aliases.push(alias);
  }
  getAliases() {
    return this.aliases;
  }
  hasAlias(alias) {
    return this.aliases.includes(alias);
  }

  callback() {
    return "Callback not overwritten.";
  }
}

module.exports = Command;
