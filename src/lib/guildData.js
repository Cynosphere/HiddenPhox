hf.database.run(
  "CREATE TABLE IF NOT EXISTS guild_data (key TEXT PRIMARY KEY, value TEXT NOT NULL) WITHOUT ROWID"
);

function setGuildData(id, key, value) {
  return new Promise((resolve, reject) => {
    hf.database.run(
      "REPLACE INTO guild_data VALUES ($key,$value)",
      {
        $value: value,
        $key: `${id}[${key}]`,
      },
      (err) => {
        if (err == null) {
          resolve(true);
        } else {
          reject(err);
        }
      }
    );
  });
}

function getGuildData(id, key, fallback = null) {
  return new Promise((resolve, reject) => {
    hf.database.get(
      "SELECT value FROM guild_data WHERE key = $key",
      {
        $key: `${id}[${key}]`,
      },
      (err, row) => {
        if (err == null) {
          resolve(row?.value || fallback);
        } else {
          reject(err);
        }
      }
    );
  });
}

function deleteGuildData(id, key) {
  return new Promise((resolve, reject) => {
    hf.database.run(
      "DELETE FROM guild_data WHERE key = $key",
      {
        $key: `${id}[${key}]`,
      },
      (err) => {
        if (err == null) {
          resolve(true);
        } else {
          reject(err);
        }
      }
    );
  });
}

module.exports = {
  setGuildData,
  getGuildData,
  deleteGuildData,
};
