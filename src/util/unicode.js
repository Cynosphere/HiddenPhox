const mappings = {};

async function cacheList() {
  const data = await fetch("https://www.unicode.org/Public/UNIDATA/UnicodeData.txt").then((res) => res.text());

  data
    .split("\n")
    .map((line) => line.split(";").splice(0, 2))
    .forEach(([character, description]) => {
      if (character != "") mappings[character.toLowerCase()] = description;
    });

  global.____unicode_data = mappings;
}

async function getNamesFromString(string) {
  if (!global.____unicode_data) await cacheList();

  const codes = [...string].map((char) => char.codePointAt().toString(16));

  return codes.map((code) => {
    const padded = code.padStart(4, "0");
    return [padded, global.____unicode_data[padded]];
  });
}

module.exports = {
  cacheList,
  getNamesFromString,
};
