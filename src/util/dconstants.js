const {Constants} = require("@projectdysnomia/dysnomia");
const Endpoints = require("@projectdysnomia/dysnomia/lib/rest/Endpoints.js");

module.exports = {...Constants};

module.exports.ActivityTypeNames = [
  "Playing",
  "Streaming",
  "Listening to",
  "Watching",
  "Custom Status",
  "Competing in",
  "Hang Status",
];

const APIEndpoints = {...Endpoints};

delete APIEndpoints.BASE_URL;
delete APIEndpoints.CDN_URL;
delete APIEndpoints.CLIENT_URL;

delete APIEndpoints.APPLICATION_ASSET;
delete APIEndpoints.APPLICATION_ICON;
delete APIEndpoints.BANNER;
delete APIEndpoints.CHANNEL_ICON;
delete APIEndpoints.CUSTOM_EMOJI;
delete APIEndpoints.DEFAULT_USER_AVATAR;
delete APIEndpoints.GUILD_AVATAR;
delete APIEndpoints.GUILD_DISCOVERY_SPLASH;
delete APIEndpoints.GUILD_ICON;
delete APIEndpoints.GUILD_SCHEDULED_EVENT_COVER;
delete APIEndpoints.GUILD_SPLASH;
delete APIEndpoints.ROLE_ICON;
delete APIEndpoints.TEAM_ICON;
delete APIEndpoints.USER_AVATAR;
delete APIEndpoints.USER_AVATAR_DECORATION_PRESET;

delete APIEndpoints.MESSAGE_LINK;

APIEndpoints.APPLICATION_ASSETS        = (applicationID) => `/oauth2/applications/${applicationID}/assets`; // prettier-ignore
APIEndpoints.APPLICATION_RPC           = (applicationID) => `/applications/${applicationID}/rpc`; // prettier-ignore
APIEndpoints.ATTACHMENT_REFRESH        =                    "/attachments/refresh-urls"; // prettier-ignore
APIEndpoints.CLAN                      =       (guildID) => `/discovery/${guildID}/clan`; // prettier-ignore
APIEndpoints.DISCOVERY_SLUG            =       (guildID) => `/discovery/${guildID}`; // prettier-ignore
APIEndpoints.GUILD_MEMBER_VERIFICATION =       (guildID) => `/guilds/${guildID}/member-verification`; // prettier-ignore
APIEndpoints.POMELO_UNAUTHED           =                    "/unique-username/username-attempt-unauthed"; // prettier-ignore
APIEndpoints.STORE_PUBLISHED_LISTING   =         (skuID) => `/store/published-listings/skus/${skuID}`; // prettier-ignore

module.exports.APIEndpoints = APIEndpoints;

module.exports.API_URL = Endpoints.CLIENT_URL + Endpoints.BASE_URL;

module.exports.ApplicationFlags.EMBEDDED_RELEASED                  = 1 << 1; // prettier-ignore
module.exports.ApplicationFlags.MANAGED_EMOJI                      = 1 << 2; // prettier-ignore
module.exports.ApplicationFlags.EMBEDDED_IAP                       = 1 << 3; // prettier-ignore
module.exports.ApplicationFlags.GROUP_DM_CREATE                    = 1 << 4; // prettier-ignore
module.exports.ApplicationFlags.RPC_PRIVATE_BETA                   = 1 << 5; // prettier-ignore
module.exports.ApplicationFlags.ALLOW_ASSETS                       = 1 << 8; // prettier-ignore
module.exports.ApplicationFlags.ALLOW_ACTIVITY_ACTION_SPECTATE     = 1 << 9; // prettier-ignore
module.exports.ApplicationFlags.ALLOW_ACTIVITY_ACTION_JOIN_REQUEST = 1 << 10; // prettier-ignore
module.exports.ApplicationFlags.RPC_HAS_CONNECTED                  = 1 << 11; // prettier-ignore
module.exports.ApplicationFlags.EMBEDDED_FIRST_PARTY               = 1 << 20; // prettier-ignore
module.exports.ApplicationFlags.ACTIVE                             = 1 << 24; // prettier-ignore
module.exports.ApplicationFlags.IFRAME_MODAL                       = 1 << 26; // prettier-ignore
module.exports.ApplicationFlags.SOCIAL_LAYER_INTEGRATION           = 1 << 27; // prettier-ignore
module.exports.ApplicationFlags.PROMOTED                           = 1 << 29; // prettier-ignore
module.exports.ApplicationFlags.PARTNER                            = 1 << 30; // prettier-ignore

module.exports.ApplicationTypes = ["Normal", "Game", "Music", "Ticketed Event", "Creator Monetization"];

module.exports.BadgeURLs = {
  staff: "https://discord.com/company",
  partner: "https://discord.com/partners",
  certified_moderator: "https://discord.com/safety",
  hypesquad: "https://discord.com/hypesquad", // dead link but idc, saves characters; new link: https://support.discord.com/hc/en-us/articles/360035962891-Profile-Badges-101#h_01GM67K5EJ16ZHYZQ5MPRW3JT3
  hypesquad_house_1: "https://discord.com/settings/hypesquad-online",
  hypesquad_house_2: "https://discord.com/settings/hypesquad-online",
  hypesquad_house_3: "https://discord.com/settings/hypesquad-online",
  bug_hunter_level_1: "https://support.discord.com/hc/en-us/articles/360046057772-Discord-Bugs",
  bug_hunter_level_2: "https://support.discord.com/hc/en-us/articles/360046057772-Discord-Bugs",
  active_developer: "https://support-dev.discord.com/hc/en-us/articles/10113997751447?ref=badge",
  early_supporter: "https://discord.com/settings/premium",
  premium: "https://discord.com/settings/premium",
  guild_booster_lvl1: "https://discord.com/settings/premium",
  guild_booster_lvl2: "https://discord.com/settings/premium",
  guild_booster_lvl3: "https://discord.com/settings/premium",
  guild_booster_lvl4: "https://discord.com/settings/premium",
  guild_booster_lvl5: "https://discord.com/settings/premium",
  guild_booster_lvl6: "https://discord.com/settings/premium",
  guild_booster_lvl7: "https://discord.com/settings/premium",
  guild_booster_lvl8: "https://discord.com/settings/premium",
  guild_booster_lvl9: "https://discord.com/settings/premium",
  bot_commands: "https://discord.com/blog/welcome-to-the-new-era-of-discord-apps?ref=badge",
  quest_completed: "https://discord.com/settings/inventory",
};

module.exports.BASE_URL = Endpoints.BASE_URL;

const CDNEndpoints = {};
CDNEndpoints.APP_ASSET           =                    (applicationID, asset) => `${Endpoints.CDN_URL}/app-assets/${applicationID}/${asset}.png`; // prettier-ignore
CDNEndpoints.APP_ICON            =        (applicationID, icon, size = 4096) => `${Endpoints.CDN_URL}/app-icons/${applicationID}/${icon}.png?size=${size}`; // prettier-ignore
CDNEndpoints.AVATAR_DECORATION   =                 (decoration, size = 4096) => `${Endpoints.CDN_URL}/avatar-decoration-presets/${decoration}.png?size=${size}&passthrough=true`; // prettier-ignore
CDNEndpoints.BANNER              =                 (id, banner, size = 4096) => `${Endpoints.CDN_URL}/banners/${id}/${banner}.${banner.startsWith("a_") ? `gif?size=${size}&_=.gif` : `png?size=${size}`}`; // prettier-ignore
CDNEndpoints.CLAN_BADGE          =             (guildID, badge, size = 4096) => `${Endpoints.CDN_URL}/clan-badges/${guildID}/${badge}.png?size=${size}`; // prettier-ignore
CDNEndpoints.CLAN_BANNER         =            (guildID, banner, size = 4096) => `${Endpoints.CDN_URL}/clan-banners/${guildID}/${banner}.png?size=${size}`; // prettier-ignore
CDNEndpoints.DEFAULT_AVATAR      =                      (index, size = 4096) => `${Endpoints.CDN_URL}/embed/avatars/${index}.png?size=${size}`; // prettier-ignore
CDNEndpoints.DISCOVERY_SPLASH    =            (guildID, splash, size = 4096) => `${Endpoints.CDN_URL}/discovery-splashes/${guildID}/${splash}.png?size=${size}`; // prettier-ignore
CDNEndpoints.EMOJI               = (emojiID, animated = false, webp = false) => `${Endpoints.CDN_URL}/emojis/${emojiID}.${webp ? "webp" : animated ? "gif?_=.gif" : "png"}`; // prettier-ignore
CDNEndpoints.GDM_ICON            =            (channelID, icon, size = 4096) => `${Endpoints.CDN_URL}/channel-icons/${channelID}/${icon}.${icon.startsWith("a_") ? `gif?size=${size}&_=.gif` : `png?size=${size}`}`; // prettier-ignore
CDNEndpoints.GUILD_ICON          =              (guildID, icon, size = 4096) => `${Endpoints.CDN_URL}/icons/${guildID}/${icon}.${icon.startsWith("a_") ? `gif?size=${size}&_=.gif` : `png?size=${size}`}`; // prettier-ignore
CDNEndpoints.GUILD_MEMBER_AVATAR =    (guildID, userID, avatar, size = 4096) => `${Endpoints.CDN_URL}/guilds/${guildID}/users/${userID}/avatars/${avatar}.${avatar.startsWith("a_") ? `gif?size=${size}&_=.gif` : `png?size=${size}`}`; // prettier-ignore
CDNEndpoints.GUILD_MEMBER_BANNER =    (guildID, userID, banner, size = 4096) => `${Endpoints.CDN_URL}/guilds/${guildID}/users/${userID}/banners/${banner}.${banner.startsWith("a_") ? `gif?size=${size}&_=.gif` : `png?size=${size}`}`; // prettier-ignore
CDNEndpoints.GUILD_SPLASH        =            (guildID, splash, size = 4096) => `${Endpoints.CDN_URL}/splashes/${guildID}/${splash}.png?size=${size}`; // prettier-ignore
CDNEndpoints.USER_AVATAR         =             (userID, avatar, size = 4096) => `${Endpoints.CDN_URL}/avatars/${userID}/${avatar}.${avatar.startsWith("a_") ? `gif?size=${size}&_=.gif` : `png?size=${size}`}`; // prettier-ignore
module.exports.CDNEndpoints = CDNEndpoints;

module.exports.CDN_URL = Endpoints.CDN_URL;
module.exports.CLIENT_URL = Endpoints.CLIENT_URL;

module.exports.ClanPlaystyle = ["None", "Very Casual", "Casual", "Competitive", "Creative", "Very Competitive"];

module.exports.DEFAULT_GROUP_DM_AVATARS = [
  "/assets/ee9275c5a437f7dc7f9430ba95f12ebd.png",
  "/assets/9baf45aac2a0ec2e2dab288333acb9d9.png",
  "/assets/7ba11ffb1900fa2b088cb31324242047.png",
  "/assets/f90fca70610c4898bc57b58bce92f587.png",
  "/assets/e2779af34b8d9126b77420e5f09213ce.png",
  "/assets/c6851bd0b03f1cca5a8c1e720ea6ea17.png",
  "/assets/f7e38ac976a2a696161c923502a8345b.png",
  "/assets/3cb840d03313467838d658bbec801fcd.png",
].map((i) => Endpoints.CLIENT_URL + i);

module.exports.ExplicitContentFilterStrings = ["Disabled", "Members without roles", "All members"];

module.exports.Games = require("#root/data/games.json");

module.exports.HANG_STATUS_ICONS = Object.fromEntries(
  Object.entries({
    chilling: "/assets/d5df7edf0b2f38954140.svg",
    gaming: "/assets/0c7aa94c8471f12c1583.svg",
    focusing: "/assets/5ba60eea3fdfa7b63f2e.svg",
    brb: "/assets/194dda5f6443bc84d703.svg",
    eating: "/assets/5f41131fa82e3a74fbf7.svg",
    "in-transit": "/assets/0cb37cec5b33d664373a.svg",
    watching: "/assets/b7832fa2d0b75e26e279.svg",
  }).map((x) => ((x[1] = Endpoints.CLIENT_URL + x[1]), x))
);

module.exports.HangStatusStrings = {
  chilling: "Chilling",
  gaming: "GAMING",
  focusing: "In the zone",
  brb: "Gonna BRB",
  eating: "Grubbin",
  "in-transit": "Wandering IRL",
  watching: "Watchin' stuff",
};

module.exports.UPLOAD_LIMIT = 26214400;
module.exports.UPLOAD_LIMIT_TIER_2 = 52428800;
module.exports.UPLOAD_LIMIT_TIER_3 = 104857600;

module.exports.UserFlags.MFA_SMS                              = 1 << 4; // prettier-ignore
module.exports.UserFlags.PREMIUM_PROMO_DISMISSED              = 1 << 5; // prettier-ignore
module.exports.UserFlags.IS_HUBSPOT_CONTACT                   = 1 << 11; // prettier-ignore
module.exports.UserFlags.HAS_UNREAD_URGENT_MESSAGES           = 1 << 13; // prettier-ignore
module.exports.UserFlags.UNDERAGE_DELETED                     = 1 << 15; // prettier-ignore
module.exports.UserFlags.DISABLE_PREMIUM                      = 1 << 21; // prettier-ignore
module.exports.UserFlags.PROVISIONAL_ACCOUNT                  = 1 << 23; // prettier-ignore
module.exports.UserFlags.QUARANTINED                          = Number(1n << 44n); // prettier-ignore
module.exports.UserFlags.PREMIUM_ELIGIBLE_FOR_UNIQUE_USERNAME = Number(1n << 47n); // prettier-ignore
module.exports.UserFlags.COLLABORATOR                         = Number(1n << 50n); // prettier-ignore
module.exports.UserFlags.RESTRICTED_COLLABORATOR              = Number(1n << 51n); // prettier-ignore

// admin panel leak aug 2022
module.exports.UserFlags.HIGH_GLOBAL_RATE_LIMIT               = Number(1n << 33n); // prettier-ignore
module.exports.UserFlags.DELETED                              = Number(1n << 34n); // prettier-ignore
module.exports.UserFlags.DISABLED_SUSPICIOUS_ACTIVITY         = Number(1n << 35n); // prettier-ignore
module.exports.UserFlags.SELF_DELETED                         = Number(1n << 36n); // prettier-ignore
module.exports.UserFlags.PREMIUM_DISCRIMINATOR                = Number(1n << 37n); // prettier-ignore
module.exports.UserFlags.USED_DESKTOP_CLIENT                  = Number(1n << 38n); // prettier-ignore
module.exports.UserFlags.USED_WEB_CLIENT                      = Number(1n << 39n); // prettier-ignore
module.exports.UserFlags.USED_MOBILE_CLIENT                   = Number(1n << 40n); // prettier-ignore
module.exports.UserFlags.HAS_SESSION_STARTED                  = Number(1n << 43n); // prettier-ignore

module.exports.VerificationLevelStrings = [
  "None",
  "Low",
  "Medium",
  "(╯°□°）╯︵ ┻━┻ (High)",
  "┻━┻ ﾐヽ(ಠ益ಠ)ノ彡┻━┻ (Very High/Phone)",
];
