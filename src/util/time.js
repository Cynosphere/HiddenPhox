function formatTime(number) {
  let seconds = parseInt(number) / 1000;
  const days = Math.floor(seconds / 86400);
  seconds = seconds % 86400;
  const hours = Math.floor(seconds / 3600);
  seconds = seconds % 3600;
  const minutes = Math.floor(seconds / 60);
  seconds = Math.floor(seconds % 60);

  return (
    (days !== 0 ? `${days.toString().padStart(2, "0")}:` : "") +
    (hours !== 0 ? `${hours.toString().padStart(2, "0")}:` : "") +
    `${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`
  );
}

function readableTime(number) {
  const seconds = number / 1000;
  const days = seconds / 60 / 60 / 24;
  const years = days / 365.25;

  if (years >= 1) {
    return `${years.toFixed(2)} years`;
  } else {
    return `${days.toFixed(2)} days`;
  }
}

const TWITTER_EPOCH = 1288834974657;
const DISCORD_EPOCH = 1420070400000;
function snowflakeToTimestamp(snowflake, twitter = false) {
  return Math.floor(Number(snowflake) / Math.pow(2, 22)) + (twitter == true ? TWITTER_EPOCH : DISCORD_EPOCH);
}

module.exports = {
  formatTime,
  readableTime,
  snowflakeToTimestamp,
};
