const {Collection} = require("@projectdysnomia/dysnomia");

const logger = require("#lib/logger.js");
const {formatUsername} = require("#util/misc.js");
const {APIEndpoints} = require("#util/dconstants.js");
const {
  RegExp: {Snowflake: REGEX_SNOWFLAKE},
} = require("#util/constants.js");

if (!hf.selectionMessages) hf.selectionMessages = new Collection();
async function selectionMessage(msg, heading, options, timeout = 30000, maxItems = 1) {
  const data = {
    content: heading,
    allowedMentions: {
      repliedUser: false,
    },
    messageReference: {
      messageID: msg.id,
    },
    components: [
      {
        type: 1,
        components: [
          {
            type: 3,
            custom_id: msg.id,
            options: [],
            max_values: maxItems,
          },
        ],
      },
      {
        type: 1,
        components: [
          {
            type: 2,
            style: 4,
            label: "Cancel",
            custom_id: "cancel",
          },
        ],
      },
    ],
  };
  options.slice(0, 25).forEach((value) => {
    data.components[0].components[0].options.push({
      label: value.display,
      value: value.key,
      description: value.description,
    });
  });
  if (options.length > 25) {
    data.content += `\n\nDisplaying 25/${options.length} results`;
  }

  const displayMessage = await msg.channel
    .createMessage(data)
    .catch((err) => logger.error("selectionMessage", "Failed to create selection message: " + err));

  return await new Promise((resolve, reject) => {
    function listener(interaction) {
      const user = interaction.member.user || interaction.user;

      if (
        user.id == msg.author.id &&
        interaction.channel.id == msg.channel.id &&
        interaction.message.components[0].components[0].custom_id == msg.id
      ) {
        if (interaction.data.custom_id == "cancel") {
          hf.events.remove("interactionCreate", `selection.${msg.id}`);
          clearTimeout(hf.selectionMessages.get(msg.id));
          hf.selectionMessages.delete(msg.id);

          interaction.deferUpdate();

          displayMessage.delete();

          reject("Canceled");
        } else {
          hf.events.remove("interactionCreate", `selection.${msg.id}`);
          clearTimeout(hf.selectionMessages.get(msg.id));
          hf.selectionMessages.delete(msg.id);

          interaction.deferUpdate();

          displayMessage.delete();

          let result;

          if (maxItems > 1) {
            result = options.filter((opt) => interaction.data.values.includes(opt.key)).map((opt) => opt.key);
          } else {
            result = options.filter((opt) => opt.key == interaction.data.values[0])[0].value;
          }

          resolve(result);
        }
      }
    }
    hf.events.add("interactionCreate", `selection.${msg.id}`, listener);
    hf.selectionMessages.set(
      msg.id,
      setTimeout(() => {
        hf.events.remove("interactionCreate", `selection.${msg.id}`);
        hf.selectionMessages.delete(msg.id);

        reject("Request timed out");
      }, timeout)
    );
  });
}

async function lookupUser(msg, str, filter) {
  if (REGEX_SNOWFLAKE.test(str)) {
    try {
      return await hf.bot.requestHandler.request("GET", APIEndpoints.USER(str.match(REGEX_SNOWFLAKE)[1]), true);
    } catch (err) {
      return err.message;
    }
  }

  let users;
  if (filter) {
    users = hf.bot.users.filter(filter).values();
  } else if (msg.guildID) {
    const guild = msg.channel.guild || hf.bot.guilds.get(msg.guildID);
    users = guild.members.values();
  } else {
    users = hf.bot.users.values();
  }

  if (/(.+?)#([0-9]{4})/.test(str)) {
    const [_, name, discrim] = str.match(/(.+?)#([0-9]{4})/);
    for (const user of users) {
      if (user.username.toLowerCase() == name.toLowerCase() && user.discriminator == discrim) {
        return user;
      }
    }
  }

  const selection = [];
  for (const user of users) {
    if (user.username.toLowerCase() == str.toLowerCase() || (user.nickname && user.nickname == str.toLowerCase())) {
      return user;
    } else if (
      user.username.toLowerCase().indexOf(str.toLowerCase()) > -1 ||
      (user.nick && user.nick.toLowerCase().indexOf(str.toLowerCase()) > -1) ||
      (user.globalName && user.globalName.toLowerCase().indexOf(str.toLowerCase()) > -1)
    ) {
      selection.push({
        value: user,
        key: user.id,
        display: `${formatUsername(user)}${
          user.nick ? ` (${user.nick})` : user.globalName ? ` (${user.globalName})` : ""
        }`,
      });
    }
  }

  selection.sort((a, b) => a.display - b.display);

  if (selection.length == 0) {
    return "No results";
  } else if (selection.length == 1) {
    return selection[0].value;
  } else {
    selection.splice(20);

    try {
      return await selectionMessage(msg, "Multiple users found, please pick from this list:", selection);
    } catch (out) {
      return out;
    }
  }
}

module.exports = {selectionMessage, lookupUser};
