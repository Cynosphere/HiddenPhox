const murmurhash = require("murmurhash").v3;
const {tinycolor} = require("@ctrl/tinycolor");

const {APIEndpoints, CDNEndpoints, UPLOAD_LIMIT, UPLOAD_LIMIT_TIER_2, UPLOAD_LIMIT_TIER_3} = require("./dconstants.js");
const {GuildFeaturesFormatted, Icons} = require("#util/constants.js");

function pastelize(id) {
  const hue = murmurhash(id) % 360;
  const hex = tinycolor(`hsl(${hue},75%,60%)`).toHex();
  return parseInt(hex, 16);
}

function formatUsername(user) {
  return user.discriminator && user.discriminator != "0"
    ? `${user.username}#${user.discriminator}`
    : `@${user.username}`;
}

function getTopColor(msg, id, fallback = 0x7289da) {
  if (!msg.guildID) return fallback;
  const guild = msg.channel?.guild ?? hf.bot.guilds.get(msg.guildID);
  if (!guild) return fallback;

  const member = guild.members.get(id);

  if (!member) return fallback;

  const roles = member.roles.map((role) => guild.roles.get(role)).filter((role) => role.color);
  roles.sort((a, b) => b.position - a.position);

  return roles[0]?.color ?? fallback;
}

function safeString(string, newLines = true) {
  string = string ? string.toString() : "";
  string = string.replace(/`/g, "'");
  string = string.replace(/<@/g, "<@\u200b");
  string = string.replace(/<#root/g, "<#\u200b");
  string = string.replace(/<&/g, "<&\u200b");
  if (newLines) string = string.replace(/\n/g, " ");
  return string;
}

async function hastebin(body) {
  const res = await fetch(`${hf.config.haste_provider}/documents`, {
    method: "POST",
    body,
  }).then((r) => r.json());
  return `<${hf.config.haste_provider}/${res.key}>`;
}

function getUploadLimit(guild) {
  if (!guild) return UPLOAD_LIMIT;

  if (guild.premiumTier == 2) return UPLOAD_LIMIT_TIER_2;
  if (guild.premiumTier == 3) return UPLOAD_LIMIT_TIER_3;

  return UPLOAD_LIMIT;
}

async function getGuild(id, noLocal = false) {
  if (hf.bot.guilds.has(id) && !noLocal) {
    return {source: "local", data: hf.bot.guilds.get(id)};
  }

  try {
    const preview = await hf.bot.requestHandler.request("GET", APIEndpoints.GUILD_PREVIEW(id), true);
    if (preview) return {source: "preview", data: preview};
  } catch {
    try {
      const widget = await hf.bot.requestHandler.request("GET", APIEndpoints.GUILD_WIDGET(id), false);
      if (widget) return {source: "widget", data: widget};
    } catch {
      try {
        const discovery = await hf.bot.requestHandler.request("GET", APIEndpoints.DISCOVERY_SLUG(id), false);
        if (discovery) return {source: "discovery", data: discovery};
      } catch {
        try {
          const verification = await hf.bot.requestHandler.request(
            "GET",
            `${APIEndpoints.GUILD_MEMBER_VERIFICATION(id)}?with_guild=true`,
            true
          );
          if (verification?.guild) return {source: "verification", data: verification.guild};
        } catch {
          return null;
        }
      }
    }
  }

  return null;
}

async function tryGetGuild(id) {
  const sources = {
    local: false,
    preview: false,
    widget: false,
    discovery: false,
    verification: false,
    clan: false,
  };

  if (hf.bot.guilds.has(id)) {
    sources.local = true;
  }

  try {
    const preview = await hf.bot.requestHandler.request("GET", APIEndpoints.GUILD_PREVIEW(id), true);
    if (preview) sources.preview = true;
  } catch {
    // noop
  }

  try {
    const widget = await hf.bot.requestHandler.request("GET", APIEndpoints.GUILD_WIDGET(id), false);
    if (widget) sources.widget = true;
  } catch {
    // noop
  }

  try {
    const discovery = await hf.bot.requestHandler.request("GET", APIEndpoints.DISCOVERY_SLUG(id), false);
    if (discovery) sources.discovery = true;
  } catch {
    // noop
  }

  try {
    const verification = await hf.bot.requestHandler.request(
      "GET",
      `${APIEndpoints.GUILD_MEMBER_VERIFICATION(id)}?with_guild=true`,
      true
    );
    if (verification?.guild) sources.verification = true;
  } catch {
    // noop
  }

  try {
    const clan = await hf.bot.requestHandler.request("GET", APIEndpoints.CLAN(id), true);
    if (clan) sources.clan = true;
  } catch {
    // noop
  }

  return sources;
}

function enumKeyToName(key) {
  return key
    .split("_")
    .map((x) => x[0] + x.substring(1).toLowerCase())
    .join(" ");
}

function formatGuildFeatures(features) {
  return features
    .sort((a, b) => {
      const feature_a = GuildFeaturesFormatted[a];
      const feature_b = GuildFeaturesFormatted[b];

      return (feature_a?.name ?? enumKeyToName(a)).localeCompare(feature_b?.name ?? enumKeyToName(b));
    })
    .map(
      (feature) =>
        `${GuildFeaturesFormatted[feature]?.icon ?? Icons.blank} ${
          GuildFeaturesFormatted[feature]?.deprecated ? "~~" : ""
        }${
          GuildFeaturesFormatted[feature]?.name ??
          feature
            .split("_")
            .map((x) => x[0] + x.substring(1).toLowerCase())
            .join(" ")
        }${GuildFeaturesFormatted[feature]?.deprecated ? "~~" : ""}`
    );
}

function getDefaultAvatar(id, discriminator = null, size = 4096) {
  let index = 0;

  if (discriminator && Number(discriminator) > 0) {
    index = Number(discriminator) % 5;
  } else if (id != null) {
    index = Number((BigInt(id) >> 22n) % 6n);
  }

  return CDNEndpoints.DEFAULT_AVATAR(index, size);
}

function flagsFromInt(int, flags, withRaw = true) {
  const bits = int.toString(2);
  const splitBits = bits.split("").reverse();

  const reassignedBits = {};

  for (const shift in splitBits) {
    reassignedBits[shift] = splitBits[shift];
  }

  const assignedFlags = Object.keys(reassignedBits).filter((bit) => reassignedBits[bit] == 1);

  const out = [];

  for (const flag of assignedFlags) {
    out.push(
      `${flags[flag] ?? "<Undocumented Flag>"}${
        withRaw == true || flags[flag] == null ? ` (1 << ${flag}, ${1n << BigInt(flag)})` : ""
      }`
    );
  }

  return out.join("\n");
}

module.exports = {
  pastelize,
  formatUsername,
  getTopColor,
  safeString,
  hastebin,
  getUploadLimit,
  getGuild,
  tryGetGuild,
  enumKeyToName,
  formatGuildFeatures,
  getDefaultAvatar,
  flagsFromInt,
};
