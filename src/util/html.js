// https://stackoverflow.com/a/39243641
const htmlEntities = {
  nbsp: " ",
  cent: "¢",
  pound: "£",
  yen: "¥",
  euro: "€",
  copy: "©",
  reg: "®",
  lt: "<",
  gt: ">",
  quot: '"',
  amp: "&",
  apos: "'",
};

function parseHtmlEntities(str) {
  return str.replace(/&([^;]+);/g, function (entity, entityCode) {
    let match;

    if (entityCode in htmlEntities) {
      return htmlEntities[entityCode];
    } else if ((match = entityCode.match(/^#x([\da-fA-F]+)$/))) {
      return String.fromCharCode(parseInt(match[1], 16));
    } else if ((match = entityCode.match(/^#(\d+)$/))) {
      return String.fromCharCode(~~match[1]);
    } else {
      return entity;
    }
  });
}

function htmlToMarkdown(str, images = true, embed = true) {
  str = str.replaceAll("\\", "\\\\");
  str = str.replace(/<style(\s+[^>]+)?>(.|\n)*?<\/style>/gi, "");
  str = str.replace(/<a (\s+[^>]+)?href="([^"]+?)"(\s+[^>]+)?>(.+?)<\/a>/gi, (_, __, url, ___, text) => {
    url = url.replace(/^\/\//, "https://").replace("\\#", "#");
    return url == text ? url : `[${text}](${embed ? "" : "&lt;"}${url}${embed ? "" : "&gt;"})`;
  });
  if (images)
    str = str.replace(
      /<img (\s+[^>]+)?src="([^"]+?)"(\s+[^>]+)?(alt|title)="([^"]+?)"(\s+[^>]+)?\/>/gi,
      `[$5](${embed ? "" : "&lt;"}$2${embed ? "" : "&gt;"})`
    );
  str = str.replace(/<\/?\s*br\s*\/?>/gi, "\n");
  str = str.replace(
    /<blockquote[^>]*?>((.|\n)*?)<\/blockquote>/gi,
    (_, quote) => "&gt; " + quote.split("\n").join("\n&gt; ")
  );
  str = str.replace(/<\/?p>/gi, "\n");
  str = str.replace(/<dd>((.|\n)*?)<\/dd>/gi, (_, inner) => "\u3000\u3000" + inner.split("\n").join("\n\u3000\u3000"));
  str = str.replace(/<ol(\s+[^>]+)?>((.|\n)*?)<\/ol>/gi, (_, __, inner) => {
    let index = 0;
    return inner
      .replace(/<li>/gi, () => {
        index++;
        return `${index}. `;
      })
      .replace(/<\/li>/gi, "\n")
      .replaceAll("\n\n", "\n");
  });
  str = str.replace(/<ul(\s+[^>]+)?>((.|\n)*?)<\/ul>/gi, (_, __, inner) => {
    let index = 0;
    return inner
      .replace(/<li>/gi, () => {
        index++;
        return `${index}. `;
      })
      .replace(/<\/li>/gi, "\n")
      .replaceAll("\n\n", "\n");
  });
  str = str.replace(/<\/?code(\s+[^>]+)?>/gi, "`");
  str = str.replace(/<\/?em(\s+[^>]+)?>/gi, "_");
  str = str.replace(/<\/?i(\s+[^>]+)?>/gi, "_");
  str = str.replace(/<\/?b(\s+[^>]+)?>/gi, "**");
  str = str.replace(/<\/?u(\s+[^>]+)?>/gi, "__");
  str = str.replace(/<\/?s(\s+[^>]+)?>/gi, "~~");
  str = str.replace(/<h1(\s+[^>]+)?>/gi, "# ");
  str = str.replace(/<h2(\s+[^>]+)?>/gi, "## ");
  str = str.replace(/<h3(\s+[^>]+)?>/gi, "### ");
  str = str.replace(/<\/?h4(\s+[^>]+)?>/gi, "**");
  str = str.replace(/<(math|noscript)(\s+[^>]+)?>((.|\n)*?)<\/(math|noscript)>/gi, "");
  str = str.replace(/<[^>]+?>/gi, "");
  str = parseHtmlEntities(str);
  // whyyyyyyyyyyyy
  str = str.replace(/\[https?:\/\/.+?\]\(<?(https?:\/\/.+?)>?\)/gi, "$1");

  return str;
}

module.exports = {parseHtmlEntities, htmlToMarkdown};
