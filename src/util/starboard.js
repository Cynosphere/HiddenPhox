const {pastelize} = require("#util/misc.js");
const {findSuitableImage} = require("#util/image.js");

async function createBoardMessage(msg, count, threadId = null, fetchAttachment = true) {
  const name = msg.member?.nick ?? msg.author.globalName ?? msg.author.username;
  const embed = {
    title: `${count} \u2b50 - ${msg.jumpLink}`,
    color: pastelize(name),
    description: msg.content,
    timestamp: new Date(msg.timestamp).toISOString(),
  };

  let image;
  if (fetchAttachment) {
    image = await findSuitableImage(msg);
    if (image.url) {
      if (image.video) {
        embed.description += `\n(contains video ${image.attachment ? "attachment" : "embed"})`;
      }
      embed.image = {
        url: image.url,
      };
    }
  }

  return {
    avatarURL: msg.member?.avatarURL ?? msg.author.avatarURL,
    username: name,
    threadID: threadId,
    embeds: [embed],
    attachments: image?.file ? [{file: image.file, filename: "thumb.jpg"}] : null,
    wait: true,
  };
}

module.exports = {createBoardMessage};
