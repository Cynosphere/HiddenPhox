module.exports = {};

const SILK_ICONS = Object.fromEntries(
  Object.entries({
    application_view_tile: "1273119065329631294",
    basket: "1273119074947174430",
    bell: "1273119087664304169",
    bell_delete: "1273119098775146628",
    book_tabs: "1273119114017378314",
    book_tabs_delete: "1273119122669961339",
    bookmark: "1273119132719644733",
    bookmark_delete: "1273119141078896722",
    bug: "1273119149425561632",
    calendar: "1273119158803894363",
    chart_organisation: "1273119178550935552",
    check_checked_green: "1273119189141422140",
    cog: "1273119202395291760",
    cog_delete: "1273119211220238407",
    comment: "1273119233487929405",
    comments: "1273119243805921323",
    compass: "1273119254954250302",
    compress: "1273119265876217907",
    controller: "1273119275724570654",
    controller_delete: "1273119284125630585",
    creditcards: "1273119294225649694",
    cross: "1273119303972946001",
    delete: "1273119315159289896",
    emoticon_smile: "1273119327901454336",
    emoticon_smile_add: "1273119339570139157",
    error: "1273119353000165460",
    eye: "1273119369261486121",
    film: "1273119412693631010",
    flag_blue: "1273119425675132999",
    flag_red: "1273119438635536385",
    group: "1273119451780354139",
    house: "1273119469987696754",
    image_add: "1273119484474818570",
    joystick: "1273119500627349534",
    key: "1273119517353967616",
    link: "1273119531916853269",
    lock: "1273119546068176956",
    magnifier: "1273119565349392444",
    microphone: "1273119582785372160",
    money: "1273119599033843836",
    money_delete: "1273119611122094170",
    newspaper: "1273119623633436785",
    outline: "1273119642084315308",
    page_link: "1273119659205595207",
    palette: "1273119683075244052",
    photo: "1273119700103991328",
    ruby: "1273119720756740106",
    shield: "1273119737043222589",
    snowflake: "1268434236839559219",
    sound: "1273119755376529418",
    star: "1273119776415289377",
    status_away: "1273119793733701715",
    stop: "1273119816181350461",
    tag_blue: "1273119832711237663",
    tick: "1273119852168740884",
    time: "1273119868031467662",
    transmit_blue: "1273119883202269225",
    user_add: "1273119924675416064",
    vcard: "1273119947341561856",
    wand: "1273119971442036746",
    world: "1273119983907639338",
    world_add: "1273119996305997844",
    world_delete: "1273120009593425940",
    world_link: "1273122514675044457",
    world_night_magnify: "1273120024189734943",
  }).map(([key, val]) => [key, `<:i:${val}>`])
);

// https://docs.discord.sex/resources/guild#guild-features
module.exports.GuildFeaturesFormatted = {
  ACTIVITIES_ALPHA: {icon: SILK_ICONS.joystick, name: "Activities (Alpha)"},
  ACTIVITIES_EMPLOYEE: {icon: SILK_ICONS.joystick, name: "Activities (Staff)"},
  ACTIVITIES_INTERNAL_DEV: {
    icon: SILK_ICONS.joystick,
    name: "Activities (Internal Dev)",
  },
  ACTIVITY_FEED_ENABLED_BY_USER: {icon: SILK_ICONS.controller},
  ACTIVITY_FEED_DISABLED_BY_USER: {icon: SILK_ICONS.controller_delete},
  ANIMATED_BANNER: {icon: SILK_ICONS.film},
  ANIMATED_ICON: {icon: SILK_ICONS.film},
  APPLICATION_COMMAND_PERMISSIONS_V2: {
    icon: SILK_ICONS.cog,
    name: "Command Permissions V2",
    deprecated: true,
  },
  AUTO_MODERATION: {icon: SILK_ICONS.shield, name: "AutoMod"},
  AUTOMOD_TRIGGER_KEYWORD_FILTER: {
    icon: SILK_ICONS.shield,
    name: "AutoMod: Keywords",
    deprecated: true,
  },
  AUTOMOD_TRIGGER_ML_SPAM_FILTER: {
    icon: SILK_ICONS.shield,
    name: "AutoMod: Spam",
    deprecated: true,
  },
  AUTOMOD_TRIGGER_SPAM_LINK_FILTER: {
    icon: SILK_ICONS.shield,
    name: "AutoMod: Spam Links",
    deprecated: true,
  },
  AUTOMOD_TRIGGER_USER_PROFILE: {
    icon: SILK_ICONS.vcard,
    name: "AutoMod: User Profiles",
    deprecated: true,
  },
  BANNER: {icon: SILK_ICONS.photo},
  BFG: {icon: SILK_ICONS.world_add, name: "BFG"},
  BOOSTING_TIERS_EXPERIMENT_MEDIUM_GUILD: {
    icon: SILK_ICONS.ruby,
    name: "Boosting: Medium Guild",
    deprecated: true,
  },
  BOOSTING_TIERS_EXPERIMENT_SMALL_GUILD: {
    icon: SILK_ICONS.ruby,
    name: "Boosting: Small Guild",
    deprecated: true,
  },
  BOT_DEVELOPER_EARLY_ACCESS: {icon: SILK_ICONS.cog},
  BURST_REACTIONS: {icon: SILK_ICONS.wand, name: "Super Reactions"},
  CHANNEL_BANNER: {icon: SILK_ICONS.photo, deprecated: true},
  CHANNEL_ICON_EMOJIS_GENERATED: {
    icon: SILK_ICONS.emoticon_smile,
    name: "Channel Icon Emojis",
  },
  CHANNEL_HIGHLIGHTS: {icon: SILK_ICONS.bookmark},
  CHANNEL_HIGHLIGHTS_DISABLED: {icon: SILK_ICONS.bookmark_delete},
  CLAN: {icon: SILK_ICONS.flag_blue},
  CLAN_DISCOVERY_DISABLED: {icon: SILK_ICONS.flag_red},
  CLAN_PILOT_GENSHIN: {
    icon: SILK_ICONS.flag_blue,
    name: "Clan Pilot: Genshin Impact",
  },
  CLAN_PILOT_VALORANT: {
    icon: SILK_ICONS.flag_blue,
    name: "Clan Pilot: Valorant",
  },
  CLAN_PREPILOT_GENSHIN: {
    icon: SILK_ICONS.flag_blue,
    name: "Clan Pre-Pilot: Genshin Impact",
  },
  CLAN_PREPILOT_VALORANT: {
    icon: SILK_ICONS.flag_blue,
    name: "Clan Pre-Pilot: Valorant",
  },
  CLYDE_DISABLED: {icon: SILK_ICONS.cog_delete, deprecated: true},
  CLYDE_ENABLED: {icon: SILK_ICONS.cog, deprecated: true},
  CLYDE_EXPERIMENT_ENABLED: {icon: SILK_ICONS.bug, deprecated: true},
  COMMERCE: {icon: SILK_ICONS.basket},
  COMMUNITY: {icon: SILK_ICONS.group},
  COMMUNITY_CANARY: {icon: SILK_ICONS.bug, name: "Community (Canary)"},
  COMMUNITY_EXP_LARGE_GATED: {
    icon: SILK_ICONS.group,
    name: "Community: Large - Gated",
  },
  COMMUNITY_EXP_LARGE_UNGATED: {
    icon: SILK_ICONS.group,
    name: "Community: Large - Ungated",
  },
  COMMUNITY_EXP_MEDIUM: {icon: SILK_ICONS.group, name: "Community: Medium"},
  CREATOR_ACCEPTED_NEW_TERMS: {icon: SILK_ICONS.check_checked_green},
  CREATOR_MONETIZABLE: {icon: SILK_ICONS.money, name: "Monetization"},
  CREATOR_MONETIZABLE_DISABLED: {
    icon: SILK_ICONS.money_delete,
    name: "Monetization Disabled",
  },
  CREATOR_MONETIZABLE_PENDING_NEW_OWNER_ONBOARDING: {
    icon: SILK_ICONS.money,
    name: "Monetization: Pending Onboarding",
  },
  CREATOR_MONETIZABLE_PROVISIONAL: {
    icon: SILK_ICONS.money,
    name: "Monetization (Provisional)",
  },
  CREATOR_MONETIZABLE_RESTRICTED: {
    icon: SILK_ICONS.money_delete,
    name: "Monetization Restricted",
  },
  CREATOR_MONETIZABLE_WHITEGLOVE: {
    icon: SILK_ICONS.bug,
    name: "Monetization White Glove",
  },
  CREATOR_MONETIZATION_APPLICATION_ALLOWLIST: {
    icon: SILK_ICONS.money,
    name: "Monetization Allow List",
  },
  CREATOR_STORE_PAGE: {icon: SILK_ICONS.basket},
  DEVELOPER_SUPPORT_SERVER: {icon: SILK_ICONS.cog},
  DISCOVERABLE: {icon: SILK_ICONS.world},
  DISCOVERABLE_DISABLED: {icon: SILK_ICONS.world_delete},
  ENABLED_DISCOVERABLE_BEFORE: {icon: SILK_ICONS.world_night_magnify},
  ENABLED_MODERATION_EXPERIENCE_FOR_NON_COMMUNITY: {
    icon: SILK_ICONS.shield,
    name: "Non-Community Members Tab",
  },
  EXPOSED_TO_ACTIVITIES_WTP_EXPERIMENT: {
    icon: SILK_ICONS.bug,
    name: "Activities: WTP Experiment",
  },
  EXPOSED_TO_BOOSTING_TIERS_EXPERIMENT: {
    icon: SILK_ICONS.bug,
    name: "Boosting: Tiers Experiment",
    deprecated: true,
  },
  FEATURABLE: {icon: SILK_ICONS.world, deprecated: true},
  FORCE_RELAY: {icon: SILK_ICONS.transmit_blue, deprecated: true},
  GENSHIN_L30: {icon: SILK_ICONS.controller, name: "Clans: Genshin Impact L30"},
  GUESTS_ENABLED: {icon: SILK_ICONS.status_away},
  GUILD_AUTOMOD_DEFAULT_LIST: {
    icon: SILK_ICONS.shield,
    name: "AutoMod: Default List",
    deprecated: true,
  },
  GUILD_COMMUNICATION_DISABLED_GUILDS: {
    icon: SILK_ICONS.time,
    name: "Member Timeouts",
    deprecated: true,
  },
  GUILD_HOME_DEPRECATION_OVERRIDE: {
    icon: SILK_ICONS.house,
    name: "Home Tab Deprecation Hidden",
  },
  GUILD_HOME_OVERRIDE: {icon: SILK_ICONS.house, name: "Home Tab (Override)"},
  GUILD_HOME_TEST: {icon: SILK_ICONS.bug, name: "Home Tab (Testing)"},
  GUILD_MEMBER_VERIFICATION_EXPERIMENT: {icon: SILK_ICONS.bug},
  GUILD_ONBOARDING: {
    icon: SILK_ICONS.application_view_tile,
    name: "Onboarding",
  },
  GUILD_ONBOARDING_ADMIN_ONLY: {
    icon: SILK_ICONS.application_view_tile,
    name: "Onboarding: Admin Only",
    deprecated: true,
  },
  GUILD_ONBOARDING_EVER_ENABLED: {
    icon: SILK_ICONS.application_view_tile,
    name: "Onboarding: Ever Enabled",
  },
  GUILD_ONBOARDING_HAS_PROMPTS: {
    icon: SILK_ICONS.application_view_tile,
    name: "Onboarding: Has Prompts",
  },
  GUILD_PRODUCTS: {icon: SILK_ICONS.basket, name: "Products"},
  GUILD_PRODUCTS_ALLOW_ARCHIVED_FILE: {
    icon: SILK_ICONS.compress,
    name: "Products: Allow Archives",
  },
  GUILD_ROLE_SUBSCRIPTIONS: {
    icon: SILK_ICONS.creditcards,
    name: "Role Subscriptions",
    deprecated: true,
  },
  GUILD_ROLE_SUBSCRIPTION_PURCHASE_FEEDBACK_LOOP: {
    icon: SILK_ICONS.creditcards,
    name: "Role Subscriptions: Feedback Loop",
    deprecated: true,
  },
  GUILD_ROLE_SUBSCRIPTION_TIER_TEMPLATE: {
    icon: SILK_ICONS.creditcards,
    name: "Role Subscriptions: Tier Template",
    deprecated: true,
  },
  GUILD_ROLE_SUBSCRIPTION_TRIALS: {
    icon: SILK_ICONS.creditcards,
    name: "Role Subscriptions: Trials",
    deprecated: true,
  },
  GUILD_SERVER_GUIDE: {icon: SILK_ICONS.compass, name: "Server Guide"},
  GUILD_WEB_PAGE_VANITY_URL: {
    icon: SILK_ICONS.page_link,
    name: "Guild Web Page Vanity URL",
  },
  HAD_EARLY_ACTIVITIES_ACCESS: {
    icon: SILK_ICONS.joystick,
    name: "Activities: Had Early Access",
  },
  HAS_DIRECTORY_ENTRY: {icon: SILK_ICONS.magnifier},
  HIDE_FROM_EXPERIMENT_UI: {
    icon: SILK_ICONS.bug,
    name: "Hidden from Experiment UI",
  },
  HUB: {icon: SILK_ICONS.chart_organisation, name: "Student Hub"},
  INCREASED_THREAD_LIMIT: {
    icon: SILK_ICONS.comments,
    name: "Threads: Increased Limit",
  },
  INTERNAL_EMPLOYEE_ONLY: {
    icon: SILK_ICONS.key,
    name: "Staff: Internal Employee Only",
  },
  INVITE_SPLASH: {icon: SILK_ICONS.photo},
  INVITES_DISABLED: {icon: SILK_ICONS.lock},
  LINKED_TO_HUB: {icon: SILK_ICONS.chart_organisation, name: "Student Hub: Linked to Hub"},
  LURKABLE: {icon: SILK_ICONS.eye, deprecated: true},
  MARKETPLACES_CONNECTION_ROLES: {icon: SILK_ICONS.link, deprecated: true},
  MEDIA_CHANNEL_ALPHA: {icon: SILK_ICONS.photo, deprecated: true},
  MEMBER_LIST_DISABLED: {icon: SILK_ICONS.delete, deprecated: true},
  MEMBER_PROFILES: {icon: SILK_ICONS.vcard, deprecated: true},
  MEMBER_SAFETY_PAGE_ROLLOUT: {icon: SILK_ICONS.shield},
  MEMBER_VERIFICATION_GATE_ENABLED: {
    icon: SILK_ICONS.shield,
    name: "Member Verification Gate",
  },
  MEMBER_VERIFICATION_MANUAL_APPROVAL: {
    icon: SILK_ICONS.user_add,
    name: "Member Join Requests",
  },
  MOBILE_WEB_ROLE_SUBSCRIPTION_PURCHASE_PAGE: {
    icon: SILK_ICONS.creditcards,
    name: "Role Subscriptions: Mobile Page",
    deprecated: true,
  },
  MONETIZATION_ENABLED: {icon: SILK_ICONS.money, deprecated: true},
  MORE_EMOJI: {icon: SILK_ICONS.emoticon_smile_add},
  MORE_STICKERS: {icon: SILK_ICONS.photo},
  NEWS: {icon: SILK_ICONS.newspaper, name: "Announcement Channels"},
  NEW_THREAD_PERMISSIONS: {
    icon: SILK_ICONS.comments,
    name: "Threads: New Permissions",
    deprecated: true,
  },
  NON_COMMUNITY_RAID_ALERTS: {icon: SILK_ICONS.bell},
  PARTNERED: {icon: SILK_ICONS.star},
  PREMIUM_TIER_3_OVERRIDE: {icon: SILK_ICONS.ruby},
  PREVIEW_ENABLED: {icon: SILK_ICONS.eye},
  PRIVATE_THREADS: {
    icon: SILK_ICONS.comments,
    name: "Threads: Private Threads",
    deprecated: true,
  },
  PRODUCTS_AVAILABLE_FOR_PURCHASE: {
    icon: SILK_ICONS.basket,
    name: "Products: Has Purchasable",
  },
  PUBLIC: {icon: SILK_ICONS.world, deprecated: true},
  PUBLIC_DISABLED: {icon: SILK_ICONS.world_delete, deprecated: true},
  RAID_ALERTS_DISABLED: {icon: SILK_ICONS.bell_delete},
  RAID_ALERTS_ENABLED: {icon: SILK_ICONS.bell, deprecated: true},
  RAPIDASH_TEST: {icon: SILK_ICONS.bug},
  RELAY_ENABLED: {icon: SILK_ICONS.transmit_blue, name: "Sharded"},
  RESTRICT_SPAM_RISK_GUILDS: {icon: SILK_ICONS.stop, deprecated: true},
  ROLE_ICONS: {icon: SILK_ICONS.tag_blue},
  ROLE_SUBSCRIPTIONS_AVAILABLE_FOR_PURCHASE: {
    icon: SILK_ICONS.creditcards,
    name: "Role Subscriptions: Has Purchasable",
  },
  ROLE_SUBSCRIPTIONS_ENABLED: {icon: SILK_ICONS.creditcards},
  ROLE_SUBSCRIPTIONS_ENABLED_FOR_PURCHASE: {
    icon: SILK_ICONS.creditcards,
    name: "Role Subscriptions: Has Purchasable",
    deprecated: true,
  },
  SEVEN_DAY_THREAD_ARCHIVE: {
    icon: SILK_ICONS.comments,
    name: "Threads: Seven Day Archive",
    deprecated: true,
  },
  SHARD: {icon: SILK_ICONS.chart_organisation, name: "Student Hub: Shard"},
  SHARED_CANVAS_FRIENDS_AND_FAMILY_TEST: {
    icon: SILK_ICONS.palette,
    name: "Shared Canvas (Testing)",
  },
  SOUNDBOARD: {icon: SILK_ICONS.sound},
  SUMMARIES_ENABLED: {icon: SILK_ICONS.book_tabs, deprecated: true},
  SUMMARIES_ENABLED_GA: {
    icon: SILK_ICONS.book_tabs,
    name: "Summaries (General Access)",
  },
  SUMMARIES_DISABLED_BY_USER: {icon: SILK_ICONS.book_tabs_delete},
  SUMMARIES_ENABLED_BY_USER: {icon: SILK_ICONS.book_tabs},
  SUMMARIES_LONG_LOOKBACK: {
    icon: SILK_ICONS.book_tabs,
    name: "Summaries: Long Lookback",
  },
  STAFF_LEVEL_COLLABORATOR_REQUIRED: {
    icon: SILK_ICONS.key,
    name: "Staff: Collaborators Only",
  },
  STAFF_LEVEL_RESTRICTED_COLLABORATOR_REQUIRED: {
    icon: SILK_ICONS.key,
    name: "Staff: Restricted Collaborators Only",
  },
  TEXT_IN_STAGE_ENABLED: {icon: SILK_ICONS.comments, deprecated: true},
  TEXT_IN_VOICE_ENABLED: {icon: SILK_ICONS.comments, deprecated: true},
  THREADS_ENABLED: {icon: SILK_ICONS.comments, deprecated: true},
  THREADS_ENABLED_TESTING: {
    icon: SILK_ICONS.comments,
    name: "Threads Enabled (Testing)",
    deprecated: true,
  },
  THREAD_DEFAULT_AUTO_ARCHIVE_DURATION: {
    icon: SILK_ICONS.comments,
    name: "Threads: Default Auto Archive",
  },
  THREADS_ONLY_CHANNEL: {
    icon: SILK_ICONS.comments,
    name: "Forum Channels",
    deprecated: true,
  },
  THREE_DAY_THREAD_ARCHIVE: {
    icon: SILK_ICONS.comments,
    name: "Threads: Three Day Archive",
    deprecated: true,
  },
  TICKETED_EVENTS_ENABLED: {
    icon: SILK_ICONS.calendar,
    name: "Scheduled Events",
    deprecated: true,
  },
  TICKETING_ENABLED: {
    icon: SILK_ICONS.calendar,
    name: "Scheduled Events",
    deprecated: true,
  },
  VALORANT_L30: {icon: SILK_ICONS.controller, name: "Clans: Valorant L30"},
  VANITY_URL: {icon: SILK_ICONS.world_link, name: "Vanity URL"},
  VERIFIED: {icon: SILK_ICONS.tick},
  VIP_REGIONS: {icon: SILK_ICONS.microphone, name: "VIP Voice Regions"},
  VOICE_CHANNEL_EFFECTS: {icon: SILK_ICONS.wand, deprecated: true},
  VOICE_IN_THREADS: {icon: SILK_ICONS.microphone},
  WELCOME_SCREEN_ENABLED: {
    icon: SILK_ICONS.application_view_tile,
    name: "Onboarding: Welcome Screen",
  },
};

const PRESENCE_ICONS = {
  desktop: {
    online: "<:OnlineDesktop:1263597847744352397>",
    idle: "<:IdleDesktop:1263597864089555037>",
    dnd: "<:DNDDesktop:1263597872268705802>",
  },
  mobile: {
    online: "<:OnlineMobile:1263597896549400647>",
    idle: "<:IdleMobile:1263597887518937160>",
    dnd: "<:DNDMobile:1263597880556654672>",
  },
  web: {
    online: "<:OnlineWeb:1263597918841999361>",
    idle: "<:IdleWeb:1263597912571645983>",
    dnd: "<:DNDWeb:1263597905776742470>",
  },
  embedded: {
    online: "<:OnlineEmbedded:1263597941051101186>",
    idle: "<:IdleEmbedded:1263597934252134462>",
    dnd: "<:DNDEmbedded:1263597927310299208>",
  },
};
const OS_ICONS = {
  darwin: "\u{1f34e}",
  win32: "\u{1fa9f}",
  linux: "\u{1f427}",
};

const BADGE_ICONS = {
  staff: "<:Staff:1273105535708696656>",
  partner: "<:Partner:1273105523973165078>",
  certified_moderator_old: "<:CertifiedModerator:1273105504259932281>",
  certified_moderator: "<:ModeratorAlumni:1273105513818624031>",
  hypesquad: "<:HypesquadEvents:1273105667254910976>",
  hypesquad_house_1: "<:HypesquadBravery:1273105648384479253>",
  hypesquad_house_2: "<:HypesquadBrilliance:1273105657771462727>",
  hypesquad_house_3: "<:HypesquadBalance:1273105639358599222>",
  bug_hunter_level_1: "<:BugHunterLevel1:1273105482898472980>",
  bug_hunter_level_2: "<:BugHunterLevel2:1273105492381667328>",
  active_developer: "<:ActiveBotDeveloper:1273105474174058558>",
  verified_developer: "<:EarlyVerifiedBotDeveloper:1273105594542194762>",
  early_supporter: "<:EarlySupporter:1273105584765407289>",
  premium: "<:Nitro:1273105677107335178>",
  guild_booster_lvl1: "<:BoostLevel1:1273107923693731850>",
  guild_booster_lvl2: "<:BoostLevel2:1273107931575095377>",
  guild_booster_lvl3: "<:BoostLevel3:1273107940177346610>",
  guild_booster_lvl4: "<:BoostLevel4:1273107949358813288>",
  guild_booster_lvl5: "<:BoostLevel5:1273107958909243483>",
  guild_booster_lvl6: "<:BoostLevel6:1273107966865965056>",
  guild_booster_lvl7: "<:BoostLevel7:1273107976722452510>",
  guild_booster_lvl8: "<:BoostLevel8:1273107984611934322>",
  guild_booster_lvl9: "<:BoostLevel9:1273107994543919105>",
  bot_commands: "<:HasSlashCommands:1273108807798755400>",
  automod: "<:UsesAutoMod:1273105734862770221>",
  application_guild_subscription: "<:HasPremium:1273105604067721258>",
  legacy_username: "<:PreviouslyKnownAs:1273105685760049297>",
  quest_completed: "<:CompletedAQuest:1273105700645502987>",
};

module.exports.Icons = {
  silk: SILK_ICONS,
  presence: PRESENCE_ICONS,
  os: OS_ICONS,
  badges: BADGE_ICONS,
  online: "<:i:1273105436807139438>",
  offline: "<:i:1273105449318875228>",
  blank: "<:i:1273123564173918268>",
  boat: "<:i:1273105457661087755>",
};

module.exports.ChannelTypeNames = {
  0: "text",
  2: "voice",
  4: "category",
  5: "announcement",
  13: "stage",
  14: "hub directory",
  15: "forum",
  16: "media",
};

const EMOJI_SETS = {
  blobs: {
    prefix: "https://cdn.jsdelivr.net/gh/googlefonts/noto-emoji@e456654119cc3a5f9bebb7bbd00512456f983d2d/svg/emoji_u",
    sep: "_",
    suffix: ".svg",
  },
  noto: {
    prefix: "https://cdn.jsdelivr.net/gh/googlefonts/noto-emoji@master/svg/emoji_u",
    sep: "_",
    suffix: ".svg",
  },
  twemoji: {
    prefix: "https://jdecked.github.io/twemoji/v/latest/svg/",
    sep: "-",
    suffix: ".svg",
  },
  mustd: {
    prefix:
      "https://cdn.jsdelivr.net/gh/Mstrodl/mutant-standard-mirror@0435227d9d8c0d6a346c8ae4c12b08a5cdc37041/emoji/",
    sep: "-",
    suffix: ".svg",
  },
  /*apple: {
    prefix: "https://intrnl.github.io/assetsEmoji/AppleColor/emoji_u",
    sep: "_",
    suffix: ".png",
  },
  facebook: {
    prefix: "https://intrnl.github.io/assetsEmoji/facebook/emoji_u",
    sep: "_",
    suffix: ".png",
  },*/
};
EMOJI_SETS["noto-old"] = EMOJI_SETS.blobs;
EMOJI_SETS.mutant = EMOJI_SETS.mustd;
EMOJI_SETS.mutstd = EMOJI_SETS.mustd;
//EMOJI_SETS.ms = EMOJI_SETS.mustd;
EMOJI_SETS.twitter = EMOJI_SETS.twemoji;
//EMOJI_SETS.fb = EMOJI_SETS.facebook;
module.exports.EmojiSets = EMOJI_SETS;

const EmojiData = require("#root/data/emoji.json");
const EMOJI_NAMES = [];
for (const emoji of EmojiData) {
  EMOJI_NAMES[emoji.char] = emoji.name.replace(/ /g, "_");
}
module.exports.EmojiNames = EMOJI_NAMES;

module.exports.RegExp = {
  Emote: /<(?:\u200b|&)?(a)?:(\w+):(\d+)>/,
  Pomelo: /^[a-z0-9._]{1,32}$/,
  Snowflake: /([0-9]{17,21})/,
};

module.exports.ApplicationFlagNames = [
  undefined,
  "~~Embedded Released~~",
  "Create Managed Emoji",
  "Embedded In-App Purchases",
  "Create Group DMs",
  "~~RPC Private Beta~~",
  "Uses AutoMod",
  undefined,
  "~~Allow Assets~~",
  "~~Allow Spectate~~",
  "~~Allow Join Requests~~",
  "~~Has Used RPC~~",
  "Presence Intent",
  "Presence Intent (Unverified)",
  "Guild Members Intent",
  "Guild Members Intent (Unverified)",
  "Suspicious Growth",
  "Embedded",
  "Message Content Intent",
  "Message Content Intent (Unverified)",
  "Embedded (First Party)",
  undefined,
  undefined,
  "Supports Commands",
  "Active",
  undefined,
  "iframe Modals",
  "Social Layer Integration",
];
