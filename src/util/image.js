async function findSuitableImage(msg) {
  const out = {};

  const attachments = [...msg.attachments.values()];
  const attachment = attachments[0];

  if (attachment) {
    const url = attachment.url;
    const parsed = new URL(url);
    if (/(jpe?g|png|gif|webp)$/.test(parsed.pathname)) {
      out.url = url;
    } else if (/(mp4|webm|mov)$/.test(parsed.pathname)) {
      out.video = true;
      out.attachment = true;
      out.url = "attachment://thumb.jpg";

      let thumbUrl = attachment.proxyURL;
      if (thumbUrl.includes("media.discordapp.net") && thumbUrl.includes("?")) {
        if (thumbUrl.endsWith("&")) {
          thumbUrl = thumbUrl += "format=jpeg";
        } else {
          thumbUrl = thumbUrl += "&format=jpeg";
        }
      } else {
        thumbUrl = thumbUrl += "?format=jpeg";
      }

      out.file = await fetch(thumbUrl)
        .then((res) => res.arrayBuffer())
        .then((buf) => Buffer.from(buf));
    }
  } else {
    for (const embed of msg.embeds) {
      if (!embed.url) continue;
      const parsed = new URL(embed.url);
      if (embed.url && /(jpe?g|png|gif|webp)$/.test(parsed.pathname)) {
        out.url = embed.url;
      } else if (embed.image) {
        out.url = embed.image.url;
        break;
      } else if (embed.video) {
        out.video = true;
        out.url = "attachment://thumb.jpg";

        let thumbUrl = embed.video.proxyURL ?? embed.video.url.replace("cdn.discordapp.com", "media.discordapp.net");
        if (thumbUrl.includes("media.discordapp.net") && thumbUrl.includes("?")) {
          if (thumbUrl.endsWith("&")) {
            thumbUrl = thumbUrl += "format=jpeg";
          } else {
            thumbUrl = thumbUrl += "&format=jpeg";
          }
        } else {
          thumbUrl = thumbUrl += "?format=jpeg";
        }
        if (embed.thumbnail?.url) thumbUrl = embed.thumbnail.url;

        let valid = await fetch(thumbUrl, {method: "HEAD"}).then((res) => res.ok);
        if (!valid && thumbUrl.includes("media.discordapp.net")) {
          thumbUrl = await hf.bot.requestHandler
            .request("POST", "/attachments/refresh-urls", true, {
              attachment_urls: [thumbUrl],
            })
            .then((res) => res.refreshed_urls[0].refreshed);
          valid = true;
        }

        if (valid)
          out.file = await fetch(thumbUrl)
            .then((res) => res.arrayBuffer())
            .then((buf) => Buffer.from(buf));
        break;
      }
    }
  }

  return out;
}

async function isGif(url) {
  const type = await fetch(url, {method: "HEAD"}).then((res) => res.headers.get("Content-Type"));
  return type == "image/gif";
}

async function findLastImage(msg, gif = false) {
  const messages = await msg.channel.getMessages(20);
  let img;

  for (const message of messages) {
    if (message.attachments.size > 0) {
      img = [...msg.attachments.values()][0].url;
      if (gif && (await isGif(img))) {
        break;
      } else {
        break;
      }
    } else if (message.embeds.length > 0) {
      img = message.embeds[0]?.thumbnail?.url || message.embeds[0]?.image?.url;
      if (img) {
        if (gif && (await isGif(img))) {
          break;
        } else {
          break;
        }
      }
    }
  }

  return await new Promise((resolve, reject) => {
    if (!img) {
      reject("Image not found in last 20 messages.");
    } else {
      resolve(img);
    }
  });
}

const urlRegex = /((https?):\/)?\/?([^:/\s]+)((\/\w+)*\/)([\w\-.]+)/;

async function getImage(msg, str) {
  const refMsg = msg.referencedMessage;
  if (refMsg) {
    const attachments = [...refMsg.attachments.values()];
    if (attachments[0]?.url) {
      return attachments[0].url;
    } else if (/<a?:[a-zA-Z0-9_]+:([0-9]+)>/.test(refMsg.content)) {
      const id = refMsg.content.match(/<a?:[a-zA-Z0-9_]+:([0-9]+)>/)[1];
      return `https://cdn.discordapp.com/emojis/${id}.png?v=1`;
    } else if (/<@!?([0-9]+)>/.test(refMsg.content)) {
      const id = refMsg.content.match(/<@!?([0-9]+)>/)[1];
      const user = await hf.bot.requestHandler.request("GET", "/users/" + id, true);
      if (user) return `https://cdn.discordapp.com/avatars/${id}/${user.avatar}.png?size=1024`;
    }
  }

  const img = await findLastImage(msg, false);
  if (!str) {
    if (img) return img;
  }

  const attachments = [...msg.attachments.values()];
  if (attachments[0]?.url) {
    return attachments[0]?.url;
  } else if (urlRegex.test(str)) {
    return str;
  } else if (/<a?:[a-zA-Z0-9_]+:([0-9]+)>/.test(str)) {
    const id = str.match(/<a?:[a-zA-Z0-9_]+:([0-9]+)>/)[1];
    return `https://cdn.discordapp.com/emojis/${id}.png?v=1`;
  } else if (/<@!?([0-9]+)>/.test(str)) {
    const id = str.match(/<@!?([0-9]+)>/)[1];
    const user = await hf.bot.requestHandler.request("GET", "/users/" + id, true);
    if (user) return `https://cdn.discordapp.com/avatars/${id}/${user.avatar}.png?size=1024`;
  } else if (img) {
    return img;
  }

  return null;
}

module.exports = {findSuitableImage, isGif, findLastImage, getImage};
