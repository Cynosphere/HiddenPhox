const {Message} = require("@projectdysnomia/dysnomia");

const fs = require("node:fs");
const httpSignature = require("@peertube/http-signature");

const events = require("#lib/events.js");
const logger = require("#lib/logger.js");
const {hasFlag} = require("#lib/guildSettings.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {MessageFlags, ApplicationCommandOptionTypes, Permissions} = require("#util/dconstants.js");
const {getUploadLimit} = require("#util/misc.js");
const {htmlToMarkdown} = require("#util/html.js");

const FRIENDLY_USERAGENT = "HiddenPhox/fedimbed (https://gitdab.com/Cynosphere/HiddenPhox)";

const URLS_REGEX = /(?:\s|^|\]\()(\|\|\s*)?(https?:\/\/[^\s<]+[^<.,:;"'\]|)\s])(\s*\)?\|\||\s*[\S]*?\))?/g;
const SPOILER_REGEX = /(?:\s|^)\|\|([\s\S]+?)\|\|/;

const BSKY_POST_REGEX =
  /^\/profile\/(did:plc:[a-z0-9]+|(did:web:)?[a-z0-9][a-z0-9.-]+[a-z0-9]*)\/post\/([a-z0-9]+)\/?$/i;

const PATH_REGEX = {
  mastodon: /^\/@(.+?)\/(\d+)\/?/,
  mastodon2: /^\/(.+?)\/statuses\/\d+\/?/,
  pleroma: /^\/objects\/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}\/?/,
  pleroma2: /^\/notice\/[A-Za-z0-9]+\/?/,
  misskey: /^\/notes\/[a-z0-9]+\/?/,
  gotosocial: /^\/@(.+?)\/statuses\/[0-9A-Z]+\/?/,
  lemmy: /^\/post\/\d+\/?/,
  honk: /^\/u\/(.+?)\/h\/(.+?)\/?/,
  pixelfed: /^\/p\/(.+?)\/(.+?)\/?/,
  cohost: /^\/[A-Za-z0-9]+\/post\/\d+-[A-Za-z0-9-]+\/?/,
  bluesky: BSKY_POST_REGEX,
};

const PLATFORM_COLORS = {
  mastodon: 0x2791da,
  pleroma: 0xfba457,
  akkoma: 0x593196,
  misskey: 0x99c203,
  calckey: 0x31748f,
  firefish: 0xf07a5b, // YCbCr interpolated from the two logo colors
  gotosocial: 0xff853e,
  lemmy: 0x14854f,
  birdsitelive: 0x1da1f2,
  iceshrimp: 0x8e82f9, // YCbCr interpolated as the accent color is a gradient
  pixelfed: 0x10c5f8,
  cohost: 0x83254f,
  bluesky: 0x0085ff,
};

const BSKY_DOMAINS = [
  "bsky.app",
  "bskye.app",
  "boobsky.app",
  "vxbsky.app",
  "cbsky.app",
  "fxbsky.app",
  "girlcockbsky.app",
  "bsyy.app",
  "bskyx.app",
  "bsky.brid.gy",
];

//eventually:tm:
/*const TW_DOMAINS = [
  "tw.c7.pm",
  "tw.counter-strike.gay",
  "twitter.com",
  "x.com",
  "fxtwitter.com",
  "fixupx.com",
  "vxtwitter.com",
  "fixvx.com",
  "stupidpenisx.com",
  "girlcockx.com",
];*/

const domainCache = new Map();
domainCache.set("cohost.org", "cohost"); // no nodeinfo

async function resolvePlatform(url) {
  const urlObj = new URL(url);
  if (domainCache.has(urlObj.hostname)) return domainCache.get(urlObj.hostname);

  const res = await fetch(urlObj.origin + "/.well-known/nodeinfo", {
    headers: {"User-Agent": FRIENDLY_USERAGENT},
  }).then((res) => res.text());

  if (!res.startsWith("{")) {
    logger.error("fedimbed", `No nodeinfo for "${urlObj.hostname}"???`);
    domainCache.set(urlObj.hostname, null);
    return null;
  }

  const probe = JSON.parse(res);

  if (!probe?.links) {
    logger.error("fedimbed", `No nodeinfo for "${urlObj.hostname}"???`);
    domainCache.set(urlObj.hostname, null);
    return null;
  }

  const nodeinfo = await fetch(probe.links[probe.links.length - 1].href, {
    headers: {"User-Agent": FRIENDLY_USERAGENT},
  }).then((res) => res.json());

  if (!nodeinfo?.software?.name) {
    logger.error("fedimbed", `Got nodeinfo for "${urlObj.hostname}", but missing software name.`);
    domainCache.set(urlObj.hostname, null);
    return null;
  }

  domainCache.set(urlObj.hostname, nodeinfo.software.name);
  return nodeinfo.software.name;
}

const keyId = "https://hf.c7.pm/actor#main-key";
const privKey = fs.readFileSync(require.resolve("#root/priv/private.pem"));
async function signedFetch(url, options) {
  const urlObj = new URL(url);

  const headers = {
    host: urlObj.host,
    date: new Date().toUTCString(),
  };

  const headerNames = ["(request-target)", "host", "date"];

  httpSignature.sign(
    {
      getHeader: (name) => headers[name.toLowerCase()],
      setHeader: (name, value) => (headers[name] = value),
      method: options.method ?? "GET",
      path: urlObj.pathname,
    },
    {
      keyId,
      key: privKey,
      headers: headerNames,
      authorizationHeaderName: "signature",
    }
  );

  options.headers = Object.assign(headers, options.headers ?? {});

  return await fetch(url, options);
}

// https://tenor.com/view/12766761978490409957
// https://github.com/bluesky-social/atproto/blob/main/packages/api/src/rich-text/unicode.ts
const encoder = new TextEncoder();
const decoder = new TextDecoder();
function utf16Slice(utf8, start, end) {
  return decoder.decode(utf8.slice(start, end));
}

function processBlueskyFacets(str, facets) {
  const utf16 = str;
  const utf8 = encoder.encode(utf16);

  const splitString = [];

  let start = 0;
  for (const facet of facets) {
    splitString.push({text: utf16Slice(utf8, start, facet.index.byteStart)});

    splitString.push({
      text: utf16Slice(utf8, facet.index.byteStart, facet.index.byteEnd),
      features: facet.features,
    });
    start = facet.index.byteEnd;
  }
  splitString.push({text: utf16Slice(utf8, start)});

  for (const part of splitString) {
    if (part.features)
      for (const feature of part.features) {
        switch (feature.$type) {
          case "app.bsky.richtext.facet#link": {
            if (
              part.text === feature.uri ||
              part.text === feature.uri.replace(/^https?:\/\//, "") ||
              part.text.endsWith("...")
            ) {
              part.text = feature.uri;
            } else {
              part.text = `[${part.text}](${feature.uri})`;
            }
            break;
          }
          case "app.bsky.richtext.facet#mention": {
            part.text = `[${part.text}](https://bsky.app/profile/${feature.did})`;
            break;
          }
          case "app.bsky.richtext.facet#tag": {
            part.text = `[${part.text}](https://bsky.app/hashtag/${feature.tag})`;
            break;
          }
          default:
            break;
        }
      }
  }

  return splitString.map((part) => part.text).join("");
}

async function blueskyQuoteEmbed(quote) {
  const embeds = [];
  const videos = [];

  let hidden = false;
  let adult = false;
  let spoiler = false;
  const tags = [];

  for (const label of quote.labels) {
    if (label.val === "!hide") {
      hidden = true;
      spoiler = true;
    } else if (
      label.val === "porn" ||
      label.val === "sexual" ||
      label.val === "graphic-media" ||
      label.val === "nudity"
    ) {
      adult = true;
      spoiler = true;
      tags.push(label.val);
    }
  }

  const mainEmbed = {
    color: PLATFORM_COLORS.bluesky,
    url: `https://bsky.app/profile/${quote.author.handle}/post/${quote.uri.substring(quote.uri.lastIndexOf("/") + 1)}`,
    author: {name: "Quoted Post", icon_url: "https://cdn.discordapp.com/emojis/1308640087759654922.png"},
    title: `${quote.author.displayName} (@${quote.author.handle})`,
    thumbnail: {
      url: quote.author.avatar,
    },
    description: quote.value.text,
    footer: {
      text: "Bluesky",
      icon_url: "https://bsky.app/static/apple-touch-icon.png",
    },
    timestamp: quote.value.createdAt,
  };

  if (quote.value.facets?.length > 0) {
    mainEmbed.description = processBlueskyFacets(mainEmbed.description, quote.value.facets);
  }

  if (quote.embeds?.[0]) {
    const embed = quote.embeds[0];
    switch (embed.$type) {
      case "app.bsky.embed.images#view": {
        embeds.push(...embed.images.map((image) => ({...mainEmbed, image: {url: image.fullsize}})));
        break;
      }
      case "app.bsky.embed.video#view": {
        const lookup = await fetch(`https://plc.directory/${quote.author.did}`).then((res) => res.json());
        const domain = lookup.service.find((service) => service.id === "#atproto_pds").serviceEndpoint;
        const videoUrl = `${domain}/xrpc/com.atproto.sync.getBlob?did=${quote.author.did}&cid=${embed.cid}`;

        const contentType = await fetch(videoUrl, {
          method: "HEAD",
        }).then((res) => res.headers.get("Content-Type"));

        videos.push({url: videoUrl, desc: embed.alt, type: contentType});

        embeds.push({...mainEmbed, fields: [{name: "\u200b", value: `[Video Link](${videoUrl})`}]});
        break;
      }
      case "app.bsky.embed.recordWithMedia#view": {
        if (embed.media.$type === "app.bsky.embed.images#view") {
          embeds.push(...embed.media.images.map((image) => ({...mainEmbed, image: {url: image.fullsize}})));
        } else if (embed.media.$type === "app.bsky.embed.video#view") {
          const lookup = await fetch(`https://plc.directory/${quote.author.did}`).then((res) => res.json());
          const domain = lookup.service.find((service) => service.id === "#atproto_pds").serviceEndpoint;
          const videoUrl = `${domain}/xrpc/com.atproto.sync.getBlob?did=${quote.author.did}&cid=${embed.media.cid}`;

          const contentType = await fetch(videoUrl, {
            method: "HEAD",
          }).then((res) => res.headers.get("Content-Type"));

          videos.push({url: videoUrl, desc: embed.alt, type: contentType});

          embeds.push({...mainEmbed, fields: [{name: "\u200b", value: `[Video Link](${videoUrl})`}]});
        }
        break;
      }
      case "app.bsky.embed.external#view": {
        if (embed.external.uri.includes("tenor.com")) {
          const url = new URL(embed.external.uri);
          url.searchParams.delete("hh");
          url.searchParams.delete("ww");
          embeds.push({...mainEmbed, image: {url: url.toString()}});
        } else {
          embeds.push(mainEmbed);
        }
        break;
      }
      default: {
        embeds.push(mainEmbed);
        break;
      }
    }
  } else {
    embeds.push(mainEmbed);
  }

  return {embeds, videos, adult, hidden, spoiler, tags};
}

async function bluesky(msg, url, spoiler = false) {
  const quoteOnly = await hasFlag(msg.guildID, "bskyQuoteOnly");

  // really...
  if (url.includes("bsky.brid.gy")) url = url.replace("bsky.brid.gy/r/https://", "");

  const urlObj = new URL(url);
  urlObj.hostname = "bsky.app";
  url = urlObj.toString();

  const postMatch = urlObj.pathname.match(BSKY_POST_REGEX);
  if (!postMatch) return {};

  const [, user, , postId] = postMatch;
  const postUri = `at://${user}/app.bsky.feed.post/${postId}`;

  const res = await fetch(
    `https://public.api.bsky.app/xrpc/app.bsky.feed.getPostThread?uri=${postUri}&depth=0&parentHeight`,
    {
      headers: {
        "User-Agent": FRIENDLY_USERAGENT,
        Accept: "application/json",
      },
    }
  );

  if (!res.ok) throw new Error(`Got non-OK status: ${res.status}`);

  const data = await res.json();

  if (!data?.thread || !("$type" in data.thread) || data.thread.$type !== "app.bsky.feed.defs#threadViewPost")
    throw new Error(`Did not get a valid Bluesky thread`);

  const {post} = data.thread;
  let hasQuote;

  const guild = msg.guildID && hf.bot.guilds.has(msg.guildID) ? hf.bot.guilds.get(msg.guildID) : null;
  const channel = guild ? guild.channels.get(msg.channel.id) : msg.channel;
  const channelNsfw = channel?.nsfw;

  let hidden = false;
  let adult = false;
  const tags = [];

  for (const label of post.labels) {
    if (label.val === "!hide") {
      hidden = true;
      spoiler = true;
    } else if (
      label.val === "porn" ||
      label.val === "sexual" ||
      label.val === "graphic-media" ||
      label.val === "nudity"
    ) {
      adult = true;
      spoiler = true;
      tags.push(label.val);
    }
  }

  const videos = [];
  const embeds = [];
  let sendWait = false;

  const mainEmbed = {
    color: PLATFORM_COLORS.bluesky,
    url,
    title: `${post.author.displayName} (@${post.author.handle})`,
    description: post.record.text,
    thumbnail: {
      url: post.author.avatar,
    },
    footer: {
      text: "Bluesky",
      icon_url: "https://bsky.app/static/apple-touch-icon.png",
    },
    timestamp: post.record.createdAt,
  };

  if (post.record.facets?.length > 0) {
    mainEmbed.description = processBlueskyFacets(mainEmbed.description, post.record.facets);
  }

  if (data.thread.parent) {
    const reply = data.thread.parent.post;
    mainEmbed.author = {
      name: `Replying to: ${reply.author.displayName} (${reply.author.handle})`,
      icon_url: "https://cdn.discordapp.com/emojis/1308640078825787412.png",
      url: `https://bsky.app/profile/${reply.author.handle}/post/${reply.uri.substring(
        reply.uri.lastIndexOf("/") + 1
      )}`,
    };
  }

  if (post.embed) {
    switch (post.embed.$type) {
      case "app.bsky.embed.images#view": {
        embeds.push(...post.embed.images.map((image) => ({...mainEmbed, image: {url: image.fullsize}})));
        break;
      }
      case "app.bsky.embed.video#view": {
        const lookup = await fetch(`https://plc.directory/${post.author.did}`).then((res) => res.json());
        const domain = lookup.service.find((service) => service.id === "#atproto_pds").serviceEndpoint;
        const videoUrl = `${domain}/xrpc/com.atproto.sync.getBlob?did=${post.author.did}&cid=${post.embed.cid}`;

        const contentType = await fetch(videoUrl, {
          method: "HEAD",
        }).then((res) => res.headers.get("Content-Type"));

        videos.push({url: videoUrl, desc: post.embed.alt, type: contentType});

        embeds.push({...mainEmbed, fields: [{name: "\u200b", value: `[Video Link](${videoUrl})`}]});
        break;
      }
      case "app.bsky.embed.record#view": {
        hasQuote = true;
        const quote = post.embed.record;
        const quoteData = await blueskyQuoteEmbed(quote);

        if (quoteData.videos.length > 0) videos.push(...quoteData.videos);

        embeds.push(mainEmbed, ...quoteData.embeds);

        if (quoteData.adult) adult = true;
        if (quoteData.hidden) hidden = true;
        if (quoteData.spoiler) spoiler = true;
        if (quoteData.tags.length > 0) tags.push(...quoteData.tags);

        break;
      }
      case "app.bsky.embed.recordWithMedia#view": {
        hasQuote = true;

        if (post.embed.media.$type === "app.bsky.embed.images#view") {
          embeds.push(...post.embed.media.images.map((image) => ({...mainEmbed, image: {url: image.fullsize}})));
        } else if (post.embed.media.$type === "app.bsky.embed.video#view") {
          const lookup = await fetch(`https://plc.directory/${post.author.did}`).then((res) => res.json());
          const domain = lookup.service.find((service) => service.id === "#atproto_pds").serviceEndpoint;
          const videoUrl = `${domain}/xrpc/com.atproto.sync.getBlob?did=${post.author.did}&cid=${post.embed.media.cid}`;

          const contentType = await fetch(videoUrl, {
            method: "HEAD",
          }).then((res) => res.headers.get("Content-Type"));

          videos.push({url: videoUrl, desc: post.embed.alt, type: contentType});

          embeds.push({...mainEmbed, fields: [{name: "\u200b", value: `[Video Link](${videoUrl})`}]});
        }

        const quoteData = await blueskyQuoteEmbed(post.embed.record.record);
        if (quoteData.videos.length > 0) videos.push(...quoteData.videos);
        embeds.push(...quoteData.embeds);

        if (quoteData.adult) adult = true;
        if (quoteData.hidden) hidden = true;
        if (quoteData.spoiler) spoiler = true;
        if (quoteData.tags.length > 0) tags.push(...quoteData.tags);

        break;
      }
      case "app.bsky.embed.external#view": {
        if (post.embed.external.uri.includes("tenor.com")) {
          const url = new URL(post.embed.external.uri);
          url.searchParams.delete("hh");
          url.searchParams.delete("ww");
          embeds.push({...mainEmbed, image: {url: url.toString()}});
        } else {
          embeds.push(mainEmbed);
        }
        break;
      }
      default: {
        embeds.push(mainEmbed);
        break;
      }
    }
  } else {
    embeds.push(mainEmbed);
  }

  if (videos.length > 0) {
    sendWait = true;
    if (msg instanceof Message) await msg.addReaction("\uD83D\uDCE4");
  }

  const files = [];
  if (videos.length > 0) {
    for (const attachment of videos) {
      const size = await fetch(attachment.url, {
        method: "HEAD",
        headers: {
          "User-Agent": FRIENDLY_USERAGENT,
        },
      }).then((res) => Number(res.headers.get("Content-Length")));

      if (size <= getUploadLimit(guild)) {
        const file = await fetch(attachment.url, {
          headers: {
            "User-Agent": FRIENDLY_USERAGENT,
          },
        })
          .then((res) => res.arrayBuffer())
          .then((buf) => Buffer.from(buf));

        files.push({
          filename:
            (spoiler ? "SPOILER_" : "") +
            (attachment.type.indexOf("/") > -1
              ? attachment.type.replace("/", ".").replace("quicktime", "mov")
              : attachment.type + "." + (url.match(/\.([a-z0-9]{3,4})$/)?.[0] ?? "mp4")),
          file,
        });
      }
    }
  }

  const warnings = [];
  if (hidden) {
    warnings.push(":warning: Post marked as hidden");
  }
  if (adult) {
    if (channelNsfw || !msg.guildID) {
      warnings.push(`:warning: Post contains adult content: ${Array.from(new Set(tags)).join(", ")}`);
    } else {
      return {
        response: {
          content: "Not embedding post due to adult content in SFW channel",
        },
      };
    }
  }

  if (quoteOnly && !hasQuote) return {};

  return {
    response: {
      content: `${warnings.length > 0 ? warnings.join("\n") + "\n" : ""}${spoiler ? `|| ${url} ||` : ""}`,
      embeds,
      attachments: files,
      allowedMentions: {
        repliedUser: false,
      },
      messageReference: {
        messageID: msg.id,
      },
    },
    sendWait,
  };
}

async function processUrl(msg, url, spoiler = false, command = false) {
  let canFedi = await hasFlag(msg.guildID, "fedimbed");
  let canBsky = await hasFlag(msg.guildID, "bskyEmbeds");

  if (command === true) {
    canFedi = true;
    canBsky = true;
  }

  let invalidUrl = false;
  let urlObj;
  try {
    urlObj = new URL(url);
  } catch {
    invalidUrl = true;
  }

  if (invalidUrl) return {};

  if (BSKY_DOMAINS.includes(urlObj.hostname.toLowerCase())) {
    if (canBsky) {
      return await bluesky(msg, url, spoiler);
    } else {
      return {};
    }
  }
  if (!canFedi) return {};

  // some lemmy instances have old reddit frontend subdomains
  // but these frontends are just frontends and dont actually expose the API
  if (urlObj.hostname.startsWith("old.")) {
    urlObj.hostname = urlObj.hostname.replace("old.", "");
    url = urlObj.href;
  }

  let platform = (await resolvePlatform(url)) ?? "<no nodeinfo>";
  let color = PLATFORM_COLORS[platform];
  let platformName = platform
    .replace("gotosocial", "GoToSocial")
    .replace("birdsitelive", '"Twitter" (BirdsiteLive)')
    .replace(/^(.)/, (_, c) => c.toUpperCase())
    .replace("Cohost", "cohost");

  const images = [];
  const videos = [];
  const audios = [];
  let content,
    cw,
    author,
    timestamp,
    title,
    poll,
    emotes = [],
    sensitive = false;

  // Fetch post
  let rawPostData;
  try {
    rawPostData = await signedFetch(url, {
      headers: {
        "User-Agent": FRIENDLY_USERAGENT,
        Accept: "application/activity+json",
      },
    }).then((res) => res.text());
  } catch (err) {
    logger.error("fedimbed", `Failed to signed fetch "${url}", retrying unsigned: ${err}`);
  }
  if (!rawPostData) {
    try {
      rawPostData = await fetch(url, {
        headers: {
          "User-Agent": FRIENDLY_USERAGENT,
          Accept: "application/activity+json",
        },
      }).then((res) => res.text());
    } catch (err) {
      logger.error("fedimbed", `Failed to fetch "${url}": ${err}`);
    }
  }

  let postData;
  if (rawPostData?.startsWith("{")) {
    try {
      postData = JSON.parse(rawPostData);
    } catch (err) {
      logger.error("fedimbed", `Failed to decode JSON for "${url}": ${err}\n  "${rawPostData}"`);
    }
  } else {
    logger.warn("fedimbed", `Got non-JSON for "${url}": ${rawPostData}`);
  }

  if (postData?.error) {
    logger.error("fedimbed", `Received error for "${url}": ${postData.error}`);
  }

  if (!postData) {
    // We failed to get post.
    // Assume it was due to AFM or forced HTTP signatures and use MastoAPI

    // Follow redirect from /object since we need the ID from /notice
    if (PATH_REGEX.pleroma.test(urlObj.pathname)) {
      url = await signedFetch(url, {
        method: "HEAD",
        headers: {
          "User-Agent": FRIENDLY_USERAGENT,
        },
        redirect: "manual",
      }).then((res) => res.headers.get("location"));
      if (url.startsWith("/")) {
        url = urlObj.origin + url;
      }
      urlObj = new URL(url);
    }

    let redirUrl;
    const options = {};
    const headers = {};
    if (PATH_REGEX.pleroma2.test(urlObj.pathname)) {
      redirUrl = url.replace("notice", "api/v1/statuses");
    } else if (PATH_REGEX.mastodon.test(urlObj.pathname)) {
      const postId = urlObj.pathname.match(PATH_REGEX.mastodon)?.[2];
      redirUrl = urlObj.origin + "/api/v1/statuses/" + postId;
    } else if (PATH_REGEX.mastodon2.test(urlObj.pathname)) {
      redirUrl = url.replace(/^\/(.+?)\/statuses/, "/api/v1/statuses");
    } else if (PATH_REGEX.misskey.test(urlObj.pathname)) {
      let noteId = url.split("/notes/")[1];
      if (noteId.indexOf("/") > -1) {
        noteId = noteId.split("/")[0];
      } else if (noteId.indexOf("?") > -1) {
        noteId = noteId.split("?")[0];
      } else if (noteId.indexOf("#") > -1) {
        noteId = noteId.split("#")[0];
      }
      logger.verbose("fedimbed", "Misskey post ID: " + noteId);
      redirUrl = urlObj.origin + "/api/notes/show/";
      options.method = "POST";
      options.body = JSON.stringify({noteId});
      headers["Content-Type"] = "application/json";
    } else {
      logger.error("fedimbed", `Missing MastoAPI replacement for "${platform}"`);
    }

    if (redirUrl) {
      logger.verbose(
        "fedimbed",
        `Redirecting "${url}" to "${redirUrl}": ${JSON.stringify(options)}, ${JSON.stringify(headers)}`
      );
      let rawPostData2;
      try {
        rawPostData2 = await signedFetch(
          redirUrl,
          Object.assign(options, {
            headers: Object.assign(headers, {
              "User-Agent": FRIENDLY_USERAGENT,
            }),
          })
        ).then((res) => res.text());
      } catch (err) {
        logger.error("fedimbed", `Failed to signed fetch "${url}" via MastoAPI, retrying unsigned: ${err}`);
      }
      if (!rawPostData2) {
        try {
          rawPostData2 = await signedFetch(
            redirUrl,
            Object.assign(options, {
              headers: Object.assign(headers, {
                "User-Agent": FRIENDLY_USERAGENT,
              }),
            })
          ).then((res) => res.text());
        } catch (err) {
          logger.error("fedimbed", `Failed to fetch "${url}" via MastoAPI: ${err}`);
        }
      }

      let postData2;
      if (rawPostData2?.startsWith("{")) {
        postData2 = JSON.parse(rawPostData2);
      } else {
        logger.warn("fedimbed", `Got non-JSON for "${url}" via MastoAPI: ${rawPostData2}`);
      }

      if (!postData2) {
        logger.warn("fedimbed", `Bailing trying to re-embed "${url}": Failed to get post from normal and MastoAPI.`);
      } else if (postData2.error) {
        logger.error(
          "fedimbed",
          `Bailing trying to re-embed "${url}", MastoAPI gave us error: ${JSON.stringify(postData2.error)}`
        );
      } else {
        cw = postData2.spoiler_warning ?? postData2.spoiler_text ?? postData2.cw;
        content =
          postData2.akkoma?.source?.content ??
          postData2.pleroma?.content?.["text/plain"] ??
          postData2.text ??
          postData2.content;
        author = {
          name:
            postData2.account?.display_name ??
            postData2.account?.username ??
            postData2.user?.name ??
            postData2.user?.username,
          handle:
            postData2.account?.fqn ?? `${postData2.account?.username ?? postData2.user?.username}@${urlObj.hostname}`,
          url: postData2.account?.url ?? `${urlObj.origin}/@${postData2.account?.username ?? postData2.user?.username}`,
          avatar: postData2.account?.avatar ?? postData2.user?.avatarUrl,
        };
        timestamp = postData2.created_at ?? postData2.createdAt;
        emotes = postData2.emojis.filter((x) => !x.name.endsWith("#.")).map((x) => ({name: `:${x.name}:`, url: x.url}));
        sensitive = postData2.sensitive;

        const attachments = postData2.media_attachments ?? postData2.files;
        if (attachments) {
          for (const attachment of attachments) {
            const contentType = await fetch(attachment.url, {
              method: "HEAD",
            }).then((res) => res.headers.get("Content-Type"));

            if (contentType) {
              if (contentType.startsWith("image/")) {
                images.push({
                  url: attachment.url,
                  desc: attachment.description ?? attachment.comment,
                  type: contentType,
                });
              } else if (contentType.startsWith("video/")) {
                videos.push({
                  url: attachment.url,
                  desc: attachment.description ?? attachment.comment,
                  type: contentType,
                });
              } else if (contentType.startsWith("audio/")) {
                audios.push({
                  url: attachment.url,
                  desc: attachment.description ?? attachment.comment,
                  type: contentType,
                });
              }
            } else {
              const type = attachment.type?.toLowerCase();

              const fileType =
                attachment.pleroma?.mime_type ?? type.indexOf("/") > -1
                  ? type
                  : type +
                    "/" +
                    (url.match(/\.([a-z0-9]{3,4})$/)?.[0] ?? type == "image"
                      ? "png"
                      : type == "video"
                      ? "mp4"
                      : "mpeg");
              if (type.startsWith("image")) {
                images.push({
                  url: attachment.url,
                  desc: attachment.description ?? attachment.comment,
                  type: fileType,
                });
              } else if (type.startsWith("video")) {
                videos.push({
                  url: attachment.url,
                  desc: attachment.description ?? attachment.comment,
                  type: fileType,
                });
              } else if (type.startsWith("audio")) {
                audios.push({
                  url: attachment.url,
                  desc: attachment.description ?? attachment.comment,
                  type: fileType,
                });
              }
            }
          }
        }
        if (!spoiler && postData2.sensitive && attachments.length > 0) {
          spoiler = true;
        }

        if (postData2.poll) {
          poll = {
            end: new Date(postData2.poll.expires_at),
            total: postData2.poll.votes_count,
            options: postData2.poll.options.map((o) => ({
              name: o.title,
              count: o.votes_count,
            })),
          };
        }
      }
    }
  } else {
    if (postData.id) {
      const realUrlObj = new URL(postData.id);
      if (realUrlObj.origin != urlObj.origin) {
        platform = await resolvePlatform(postData.id);
        color = PLATFORM_COLORS[platform];
        platformName = platform.replace("gotosocial", "GoToSocial").replace(/^(.)/, (_, c) => c.toUpperCase());
        url = postData.id;
      }
    }

    content = postData._misskey_content ?? postData.source?.content ?? postData.content;
    cw = postData.summary;
    timestamp = postData.published;
    sensitive = postData.sensitive;

    if (postData.tag) {
      let tag = postData.tag;
      // gts moment
      if (!Array.isArray(tag)) tag = [tag];
      emotes = tag.filter((x) => !!x.icon).map((x) => ({name: x.name, url: x.icon.url}));
    }

    // NB: gts doesnt send singular attachments as array
    const attachments = Array.isArray(postData.attachment) ? postData.attachment : [postData.attachment];
    for (const attachment of attachments) {
      if (attachment.mediaType) {
        if (attachment.mediaType.startsWith("video/")) {
          videos.push({
            url: attachment.url,
            desc: attachment.name ?? attachment.description ?? attachment.comment,
            type: attachment.mediaType,
          });
        } else if (attachment.mediaType.startsWith("image/")) {
          images.push({
            url: attachment.url,
            desc: attachment.name ?? attachment.description ?? attachment.comment,
            type: attachment.mediaType,
          });
        } else if (attachment.mediaType.startsWith("audio/")) {
          audios.push({
            url: attachment.url,
            desc: attachment.name ?? attachment.description ?? attachment.comment,
            type: attachment.mediaType,
          });
        }
      } else if (attachment.url) {
        const contentType = await fetch(attachment.url, {
          method: "HEAD",
        }).then((res) => res.headers.get("Content-Type"));

        if (contentType) {
          if (contentType.startsWith("image/")) {
            images.push({
              url: attachment.url,
              desc: attachment.name ?? attachment.description ?? attachment.comment,
              type: contentType,
            });
          } else if (contentType.startsWith("video/")) {
            videos.push({
              url: attachment.url,
              desc: attachment.name ?? attachment.description ?? attachment.comment,
              type: contentType,
            });
          } else if (contentType.startsWith("audio/")) {
            audios.push({
              url: attachment.url,
              desc: attachment.name ?? attachment.description ?? attachment.comment,
              type: contentType,
            });
          }
        } else {
          const type = attachment.type?.toLowerCase();

          const fileType =
            type.indexOf("/") > -1
              ? type
              : type +
                "/" +
                (url.match(/\.([a-z0-9]{3,4})$/)?.[0] ?? type == "image" ? "png" : type == "video" ? "mp4" : "mpeg");
          if (type.startsWith("image")) {
            images.push({
              url: attachment.url,
              desc: attachment.name ?? attachment.description ?? attachment.comment,
              type: fileType,
            });
          } else if (type.startsWith("video")) {
            videos.push({
              url: attachment.url,
              desc: attachment.name ?? attachment.description ?? attachment.comment,
              type: fileType,
            });
          } else if (type.startsWith("audio")) {
            audios.push({
              url: attachment.url,
              desc: attachment.name ?? attachment.description ?? attachment.comment,
              type: fileType,
            });
          }
        }
      } else {
        logger.warn("fedimbed", `Unhandled attachment structure! ${JSON.stringify(attachment)}`);
      }
    }

    if (!spoiler && postData.sensitive && attachments.length > 0) {
      spoiler = true;
    }

    if (postData.image?.url) {
      const imageUrl = new URL(postData.image.url);
      const contentType = await fetch(postData.image.url, {
        method: "HEAD",
      }).then((res) => res.headers.get("Content-Type"));
      images.push({
        url: postData.image.url,
        desc: "",
        type: contentType ?? "image/" + imageUrl.pathname.substring(imageUrl.pathname.lastIndexOf(".") + 1),
      });
    }

    if (postData.name) title = postData.name;

    // Author data is not sent with the post with AS2
    const authorData = await signedFetch(postData.actor ?? postData.attributedTo, {
      headers: {
        "User-Agent": FRIENDLY_USERAGENT,
        Accept: "application/activity+json",
      },
    })
      .then((res) => res.json())
      .catch((err) => {
        // only posts can be activity+json right now, reduce log spam
        if (platform !== "cohost") logger.error("fedimbed", `Failed to get author for "${url}": ${err}`);
      });

    if (authorData) {
      const authorUrlObj = new URL(authorData.url ?? authorData.id);
      author = {
        name: authorData.name,
        handle: `${authorData.preferredUsername}@${authorUrlObj.hostname}`,
        url: authorData.url,
        avatar: authorData.icon?.url,
      };
    } else {
      // bootleg author, mainly for cohost
      const authorUrl = postData.actor ?? postData.attributedTo;
      const authorUrlObj = new URL(authorUrl);
      const name = authorUrlObj.pathname.substring(authorUrlObj.pathname.lastIndexOf("/") + 1);
      author = {
        name,
        handle: `${name}@${authorUrlObj.hostname}`,
        url: authorUrl,
      };
    }

    if (postData.endTime && postData.oneOf && postData.votersCount) {
      poll = {
        end: new Date(postData.endTime),
        total: postData.votersCount,
        options: postData.oneOf.map((o) => ({
          name: o.name,
          count: o.replies.totalItems,
        })),
      };
    }
  }

  // We could just continue without author but it'd look ugly and be confusing.
  if (!author) {
    logger.warn("fedimbed", `Bailing trying to re-embed "${url}": Failed to get author.`);
    return {};
  }

  // Start constructing embed
  content = content ?? "";
  cw = cw ?? "";

  content = htmlToMarkdown(content);

  for (const emote of emotes) {
    content = content.replaceAll(emote.name, `[${emote.name}](${emote.url})`);
  }

  cw = htmlToMarkdown(cw);

  let desc = "";
  let MAX_LENGTH = 3999;
  if ((cw != "" || sensitive) && images.length == 0 && videos.length == 0 && audios.length == 0) {
    const ors = content.split("||");
    desc += `||${content.replaceAll("||", "|\u200b|")}||`;
    MAX_LENGTH -= ors.length - 1;
    MAX_LENGTH -= 4;

    if (cw != "") {
      desc = "\u26a0 " + cw + "\n\n" + desc;
      MAX_LENGTH -= 4 - cw.length;
    }
  } else {
    desc = content;
  }

  if (desc.length > MAX_LENGTH) {
    if (desc.endsWith("||")) {
      desc = desc.substring(0, MAX_LENGTH - 2);
      desc += "\u2026||";
    } else {
      desc = desc.substring(0, MAX_LENGTH) + "\u2026";
    }
  }

  const user = author.name ? `${author.name} (${author.handle})` : author.handle;

  const baseEmbed = {
    color,
    url,
    timestamp,
    description: desc,
    title: title ?? user,
    author: title
      ? {
          name: user,
          url: author.url,
        }
      : null,
    footer: {
      text: platformName,
    },
    thumbnail: {
      url: author.avatar,
    },
    fields: [],
  };
  if (images.length > 0) {
    if (images.length > 1) {
      const links = images.map((attachment, index) => `[Image ${index + 1}](${attachment.url})`).join("\u3000\u3000");

      if (links.length <= 1024)
        baseEmbed.fields.push({
          name: "Images",
          value: links,
          inline: true,
        });
    } else {
      baseEmbed.fields.push({
        name: "Image",
        value: `[Click for image](${images[0].url})`,
        inline: true,
      });
    }
  }
  if (videos.length > 0) {
    if (videos.length > 1) {
      baseEmbed.fields.push({
        name: "Videos",
        value: videos.map((attachment, index) => `[Video ${index + 1}](${attachment.url})`).join("\u3000\u3000"),
        inline: true,
      });
    } else {
      baseEmbed.fields.push({
        name: "Video",
        value: `[Click for video](${videos[0].url})`,
        inline: true,
      });
    }
  }
  if (audios.length > 0) {
    if (audios.length > 1) {
      baseEmbed.fields.push({
        name: "Audios",
        value: audios.map((attachment, index) => `[Audio ${index + 1}](${attachment.url})`).join("\u3000\u3000"),
        inline: true,
      });
    } else {
      baseEmbed.fields.push({
        name: "Audio",
        value: `[Click for audio](${audios[0].url})`,
        inline: true,
      });
    }
  }

  if (poll) {
    baseEmbed.fields.push({
      name: "Poll",
      value:
        poll.options
          .map((o) => {
            const percent = o.count / poll.total;
            const bar = Math.round(percent * 30);

            return `**${o.name}** (${o.count}, ${Math.round(percent * 100)}%)\n\`[${"=".repeat(bar)}${" ".repeat(
              30 - bar
            )}]\``;
          })
          .join("\n\n") + `\n\n${poll.total} votes \u2022 Ends <t:${Math.floor(poll.end.getTime() / 1000)}:R>`,
    });
  }

  let sendWait = false;
  if (videos.length > 0 || audios.length > 0 || images.length > 4) {
    sendWait = true;
    if (msg instanceof Message) await msg.addReaction("\uD83D\uDCE4");
  }

  const embeds = [];
  const files = [];

  const guild = msg.channel?.guild ?? (msg.guildID ? hf.bot.guilds.get(msg.guildID) : false);

  if (images.length > 0) {
    if (images.length <= 4) {
      for (const attachment of images) {
        const embed = Object.assign({}, baseEmbed);
        embed.image = {
          url: attachment.url,
        };
        embeds.push(embed);
      }
    } else if (images.length > 4 && images.length <= 10) {
      for (const attachment of images) {
        const size = await fetch(attachment.url, {
          method: "HEAD",
          headers: {
            "User-Agent": FRIENDLY_USERAGENT,
          },
        }).then((res) => Number(res.headers.get("Content-Length")));

        if (size <= getUploadLimit(guild)) {
          const file = await fetch(attachment.url, {
            headers: {
              "User-Agent": FRIENDLY_USERAGENT,
            },
          })
            .then((res) => res.arrayBuffer())
            .then((buf) => Buffer.from(buf));

          files.push({
            filename:
              (cw != "" || spoiler ? "SPOILER_" : "") +
              (attachment.type.indexOf("/") > -1
                ? attachment.type.replace("/", ".")
                : attachment.type + "." + (url.match(/\.([a-z0-9]{3,4})$/)?.[0] ?? "png")),
            file,
            description: attachment.desc,
          });
        }
      }
      embeds.push(baseEmbed);
    } else {
      const ten = images.slice(0, 10);

      for (const attachment of ten) {
        const size = await fetch(attachment.url, {
          method: "HEAD",
          headers: {
            "User-Agent": FRIENDLY_USERAGENT,
          },
        }).then((res) => Number(res.headers.get("Content-Length")));

        if (size <= getUploadLimit(guild)) {
          const file = await fetch(attachment.url, {
            headers: {
              "User-Agent": FRIENDLY_USERAGENT,
            },
          })
            .then((res) => res.arrayBuffer())
            .then((buf) => Buffer.from(buf));

          files.push({
            filename:
              (cw != "" || spoiler ? "SPOILER_" : "") +
              (attachment.type.indexOf("/") > -1
                ? attachment.type.replace("/", ".")
                : attachment.type + "." + (url.match(/\.([a-z0-9]{3,4})$/)?.[0] ?? "png")),
            file,
            description: attachment.desc,
          });
        }
      }

      if (images.length <= 14) {
        const fourteen = images.slice(10, 14);

        for (const attachment of fourteen) {
          const embed = Object.assign({}, baseEmbed);
          embed.image = {
            url: attachment.url,
          };
          embeds.push(embed);
        }
      } else if (images.length <= 18) {
        const fourteen = images.slice(10, 14);

        for (const attachment of fourteen) {
          const embed = Object.assign({}, baseEmbed);
          embed.image = {
            url: attachment.url,
          };
          embeds.push(embed);
        }

        const eighteen = images.slice(14, 18);
        const _embed = {
          color: baseEmbed.color,
          url: baseEmbed.url + "?_",
          title: "Additional Images",
        };

        for (const attachment of eighteen) {
          const embed = Object.assign({}, _embed);
          embed.image = {
            url: attachment.url,
          };
          embeds.push(embed);
        }
      }
    }
  } else {
    embeds.push(baseEmbed);
  }

  if (videos.length > 0) {
    for (const attachment of videos) {
      const size = await fetch(attachment.url, {
        method: "HEAD",
        headers: {
          "User-Agent": FRIENDLY_USERAGENT,
        },
      }).then((res) => Number(res.headers.get("Content-Length")));

      if (size <= getUploadLimit(guild)) {
        const file = await fetch(attachment.url, {
          headers: {
            "User-Agent": FRIENDLY_USERAGENT,
          },
        })
          .then((res) => res.arrayBuffer())
          .then((buf) => Buffer.from(buf));

        files.push({
          filename:
            (cw != "" || spoiler ? "SPOILER_" : "") +
            (attachment.type.indexOf("/") > -1
              ? attachment.type.replace("/", ".").replace("quicktime", "mov")
              : attachment.type + "." + (url.match(/\.([a-z0-9]{3,4})$/)?.[0] ?? "mp4")),
          file,
        });
      }
    }
  }
  if (audios.length > 0) {
    for (const attachment of audios) {
      const size = await fetch(attachment.url, {
        method: "HEAD",
        headers: {
          "User-Agent": FRIENDLY_USERAGENT,
        },
      }).then((res) => Number(res.headers.get("Content-Length")));

      if (size <= getUploadLimit(guild)) {
        const file = await fetch(attachment.url, {
          headers: {
            "User-Agent": FRIENDLY_USERAGENT,
          },
        })
          .then((res) => res.arrayBuffer())
          .then((buf) => Buffer.from(buf));

        files.push({
          filename:
            (cw != "" || spoiler ? "SPOILER_" : "") +
            (attachment.type.indexOf("/") > -1
              ? attachment.type.replace("/", ".").replace("mpeg", "mp3").replace("vnd.wave", "wav").replace("x-", "")
              : attachment.type + "." + (url.match(/\.([a-z0-9]{3,4})$/)?.[0] ?? "mp3")),
          file,
        });
      }
    }
  }

  return {
    response: {
      content:
        cw != "" && (images.length > 0 || videos.length > 0 || audios.length > 0)
          ? `:warning: ${cw} || ${url} ||`
          : spoiler
          ? `|| ${url} ||`
          : "",
      embeds,
      attachments: files,
      allowedMentions: {
        repliedUser: false,
      },
      messageReference: {
        messageID: msg.id,
      },
    },
    sendWait,
  };
}

events.add("messageCreate", "fedimbed", async function (msg) {
  if (msg.author.id == hf.bot.user.id) return;
  if (!msg.guildID) return;
  if (!((await hasFlag(msg.guildID, "fedimbed")) || (await hasFlag(msg.guildID, "bskyEmbeds")))) return;
  if (!msg.content || msg.content == "") return;

  if (URLS_REGEX.test(msg.content)) {
    const urls = msg.content.match(URLS_REGEX);
    for (let url of urls) {
      const hasSpoiler = SPOILER_REGEX.test(url);
      url = url
        .replace(/\|/g, "")
        .replace(/^\]\(/, "")
        .replace(/\s*[\S]*?\)$/, "")
        .trim()
        .replace("#\u200b", "#")
        .replace("#%E2%80%8B", "#");
      let urlObj;
      try {
        urlObj = new URL(url);
      } catch {
        // noop
      }
      for (const service of Object.keys(PATH_REGEX)) {
        const regex = PATH_REGEX[service];
        if (urlObj && regex.test(urlObj.pathname)) {
          logger.verbose("fedimbed", `Hit "${service}" for "${url}", processing now.`);
          try {
            const {response, sendWait} = await processUrl(msg, url, hasSpoiler);
            await msg.channel.createMessage(response).then(() => {
              if (sendWait) {
                msg.removeReaction("\uD83D\uDCE4");
              }

              if ((msg.flags & MessageFlags.SUPPRESS_EMBEDS) === 0) {
                msg.edit({flags: MessageFlags.SUPPRESS_EMBEDS}).catch(() => {});
              }
            });
          } catch (err) {
            logger.error("fedimbed", `Error processing "${url}":\n` + err.stack);
          }
          break;
        }
      }
    }
  }
});

const fedimbedCommand = new InteractionCommand("fedimbed");
fedimbedCommand.helpText = "Better embeds for fediverse (Mastodon, Pleroma, etc) and Bluesky posts";
fedimbedCommand.options.url = {
  name: "url",
  type: ApplicationCommandOptionTypes.STRING,
  description: "URL to attempt to parse for re-embedding",
  required: true,
  default: "",
};
fedimbedCommand.options.spoiler = {
  name: "spoiler",
  type: ApplicationCommandOptionTypes.BOOLEAN,
  description: "Send embed spoilered",
  required: false,
  default: false,
};
fedimbedCommand.permissions = Permissions.embedLinks | Permissions.attachFiles;
fedimbedCommand.callback = async function (interaction) {
  let url = this.getOption(interaction, "url");
  const spoiler = this.getOption(interaction, "spoiler");

  url = url
    .replace(/\|/g, "")
    .replace(/^\]\(/, "")
    .replace(/\s*[\S]*?\)$/, "")
    .trim()
    .replace("#\u200b", "#")
    .replace("#%E2%80%8B", "#");
  let urlObj;
  try {
    urlObj = new URL(url);
  } catch (err) {
    return {
      content: `Failed to parse URL:\`\`\`\n${err}\`\`\``,
      flags: MessageFlags.EPHEMERAL,
    };
  }

  let hasService = false;
  for (const service of Object.keys(PATH_REGEX)) {
    const regex = PATH_REGEX[service];
    if (urlObj && regex.test(urlObj.pathname)) {
      hasService = true;
      break;
    }
  }

  if (hasService || BSKY_DOMAINS.includes(urlObj.hostname.toLowerCase())) {
    try {
      const {response} = await processUrl(interaction, url, spoiler, true);

      if (!response)
        return {
          content: "Failed to process URL.",
          flags: MessageFlags.EPHEMERAL,
        };

      delete response.messageReference;
      return response;
    } catch (err) {
      logger.error("fedimbed", `Error processing "${url}":\n` + err.stack);
      return {
        content: "Failed to process URL.",
        flags: MessageFlags.EPHEMERAL,
      };
    }
  } else {
    return {
      content: "Did not get a valid service for this URL.",
      flags: MessageFlags.EPHEMERAL,
    };
  }
};
hf.registerCommand(fedimbedCommand);
