const Command = require("../lib/command.js");
const CATEGORY = "general";

const help = new Command("help");
help.category = CATEGORY;
help.helpText = "Lists all commands";
help.usage = "[command] or [--category]";
help.callback = function (msg, line) {
  const sorted = {};

  for (const cmd of hf.commands.values()) {
    const cat = cmd.category.toLowerCase();
    if (!sorted[cat]) {
      sorted[cat] = [];
    }
    sorted[cat].push(cmd);
  }

  if (line == "") {
    const embed = {
      title: "HiddenPhox Help",
      fields: [],
    };

    for (const cat in sorted) {
      embed.fields.push({
        name: cat.toUpperCase().charAt(0) + cat.toLowerCase().substring(1),
        value: `${sorted[cat].length} Commands\n\`${
          hf.config.prefix
        }help --${cat.toLowerCase()}\``,
        inline: true,
      });
    }

    return {embed};
  } else if (line.startsWith("--")) {
    const cat = line.replace("--", "").toLowerCase().trim();

    if (sorted[cat]) {
      const embed = {
        title: `HiddenPhox Help: Category > ${
          cat.toUpperCase().charAt(0) + cat.toLowerCase().substring(1)
        }`,
        fields: [],
      };

      for (const cmd of sorted[cat]) {
        embed.fields.push({
          name: hf.config.prefix + cmd.name,
          value: cmd.helpText,
          inline: true,
        });
      }

      return {embed};
    } else {
      return "Category not found.";
    }
  } else {
    let cmd = hf.commands.get(line.toLowerCase().trim());
    if (!cmd) {
      for (const c of hf.commands.values()) {
        if (c.hasAlias(line.toLowerCase().trim())) {
          cmd = c;
          break;
        }
      }
    }

    if (cmd) {
      const aliases = cmd.getAliases();

      const embed = {
        title: `HiddenPhox Help: Command > \`${hf.config.prefix}${cmd.name}\``,
        description: cmd.helpText,
        fields: [
          {
            name: "Category",
            value:
              cmd.category.toUpperCase().charAt(0) +
              cmd.category.toLowerCase().substring(1),
            inline: true,
          },
        ],
      };
      if (aliases.length > 0) {
        embed.fields.push({
          name: "Aliases",
          value: aliases.join(", "),
          inline: true,
        });
      }
      if (cmd.usage) {
        embed.fields.push({
          name: "Usage",
          value: `${hf.config.prefix}${cmd.name} ${cmd.usage}`,
          inline: true,
        });
      }

      if (cmd.description) {
        embed.fields.push({
          name: "Full Description",
          value: cmd.description,
        });
      }

      return {embed};
    } else {
      return "Command not found.";
    }
  }
};
hf.registerCommand(help);

const ping = new Command("ping");
ping.category = CATEGORY;
ping.helpText = "Pong";
ping.description = "Measures response times to Discord.";
ping.addAlias("p");
ping.callback = async function (msg) {
  const newMsg = await msg.channel.createMessage({
    content: "Pong.",
    allowedMentions: {
      repliedUser: false,
    },
    messageReference: {
      messageID: msg.id,
    },
  });
  const rtt = Math.floor(
    newMsg.timestamp - (msg.editedTimestamp || msg.timestamp)
  );
  const gateway = hf.bot.shards.get(
    hf.bot.guildShardMap[hf.bot.channelGuildMap[msg.channel.id]] || 0
  ).latency;

  newMsg.edit({
    content: `Pong. RTT: \`${rtt}ms\`, Gateway: \`${gateway}ms\``,
    allowedMentions: {
      repliedUser: false,
    },
  });
};
hf.registerCommand(ping);

const invite = new Command("invite");
invite.category = CATEGORY;
invite.helpText = "Returns invite link for the bot.";
invite.callback = function () {
  return `<https://discord.com/oauth2/authorize?client_id=${hf.config.client_id}&scope=bot>`;
};
hf.registerCommand(invite);
