const fs = require("node:fs");
const {resolve} = require("node:path");

const logger = require("#lib/logger.js");

for (const file of fs.readdirSync(resolve(__dirname, "utility"), {withFileTypes: true})) {
  if (file.isDirectory()) continue;
  try {
    require(`#modules/utility/${file.name}`);
  } catch (err) {
    logger.error("hf:modules:utility", `Failed to load "${file.name}": ${err}`);
  }
}
