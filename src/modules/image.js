const Command = require("../lib/command.js");
const CATEGORY = "image";

const {getImage} = require("../lib/utils.js");

const dumpyConvert = require("dumpy").convert;
const Jimp = require("jimp");

async function createImageCallback(msg, url, callback, filename) {
  msg.channel.sendTyping();

  let img;
  try {
    img = await getImage(msg, url);
  } catch (e) {
    if (e == "Image not found in last 20 messages.") {
      return "Image not found. Please give URL, attachment, user mention or custom emoji.";
    } else {
      return ":warning: An internal error occurred.";
    }
  }

  const out = await callback(img);

  if (out) {
    return {file: {file: out, name: filename}};
  } else {
    return ":warning: Nothing was returned.";
  }
}

const dumpy = new Command("dumpy");
dumpy.category = CATEGORY;
dumpy.helpText = "Among Us Dumpy GIF Creator";
dumpy.usage = "<width> [url]";
dumpy.callback = async function (msg, line, [width, url]) {
  if (isNaN(parseInt(width))) url = width;
  width = Math.min(Math.max(isNaN(parseInt(width)) ? 10 : width, 2), 48);

  return await createImageCallback(
    msg,
    url,
    async (img) => await dumpyConvert(img, width),
    "dumpy.gif"
  );
};
hf.registerCommand(dumpy);

const hooh = new Command("hooh");
hooh.category = CATEGORY;
hooh.helpText = "Mirror bottom to top";
hooh.usage = "[url]";
hooh.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (url) => {
      const img = await Jimp.read(url);
      const half1 = img
        .clone()
        .crop(
          0,
          img.bitmap.height / 2,
          img.bitmap.width,
          img.bitmap.height / 2
        );
      const half2 = half1.clone().mirror(false, true);

      return await img
        .composite(half1, 0, img.bitmap.height / 2)
        .composite(half2, 0, 0)
        .getBufferAsync(Jimp.MIME_PNG);
    },
    "hooh.png"
  );
};
hf.registerCommand(hooh);

const woow = new Command("woow");
woow.category = CATEGORY;
woow.helpText = "Mirror top to bottom";
woow.usage = "[url]";
woow.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (url) => {
      const img = await Jimp.read(url);
      const half1 = img
        .clone()
        .crop(0, 0, img.bitmap.width, img.bitmap.height / 2);
      const half2 = half1.clone().mirror(false, true);

      return await img
        .composite(half1, 0, 0)
        .composite(half2, 0, img.bitmap.height / 2)
        .getBufferAsync(Jimp.MIME_PNG);
    },
    "woow.png"
  );
};
hf.registerCommand(woow);

const haah = new Command("haah");
haah.category = CATEGORY;
haah.helpText = "Mirror left to right";
haah.usage = "[url]";
haah.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (url) => {
      const img = await Jimp.read(url);
      const half1 = img
        .clone()
        .crop(0, 0, img.bitmap.width / 2, img.bitmap.height);
      const half2 = half1.clone().mirror(true, false);

      return await img
        .composite(half1, 0, 0)
        .composite(half2, img.bitmap.width / 2, 0)
        .getBufferAsync(Jimp.MIME_PNG);
    },
    "haah.png"
  );
};
hf.registerCommand(haah);

const waaw = new Command("waaw");
waaw.category = CATEGORY;
waaw.helpText = "Mirror right to left";
waaw.usage = "[url]";
waaw.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (url) => {
      const img = await Jimp.read(url);
      const half1 = img
        .clone()
        .crop(img.bitmap.width / 2, 0, img.bitmap.width / 2, img.bitmap.height);
      const half2 = half1.clone().mirror(true, false);

      return await img
        .composite(half1, img.bitmap.width / 2, 0)
        .composite(half2, 0, 0)
        .getBufferAsync(Jimp.MIME_PNG);
    },
    "waaw.png"
  );
};
hf.registerCommand(waaw);

const invert = new Command("invert");
invert.category = CATEGORY;
invert.helpText = "Invert an image";
invert.usage = "[url]";
invert.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (img) => {
      return await Jimp.read(img).then((x) =>
        x.invert().getBufferAsync(Jimp.MIME_PNG)
      );
    },
    "invert.png"
  );
};
hf.registerCommand(invert);

const flip = new Command("flip");
flip.category = CATEGORY;
flip.helpText = "Flip an image horizontally";
flip.usage = "[url]";
flip.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (img) => {
      return await Jimp.read(img).then((x) =>
        x.mirror(true, false).getBufferAsync(Jimp.MIME_PNG)
      );
    },
    "flip.png"
  );
};
hf.registerCommand(flip);

const flop = new Command("flop");
flop.category = CATEGORY;
flop.helpText = "Flip an image vertically";
flop.usage = "[url]";
flop.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (img) => {
      return await Jimp.read(img).then((x) =>
        x.mirror(false, true).getBufferAsync(Jimp.MIME_PNG)
      );
    },
    "flop.png"
  );
};
hf.registerCommand(flop);

const jpeg = new Command("jpeg");
jpeg.category = CATEGORY;
jpeg.helpText = "Turn an image into a compressed mess";
jpeg.usage = "[url]";
jpeg.addAlias("needsmorejpeg");
jpeg.callback = async function (msg, line) {
  return await createImageCallback(
    msg,
    line,
    async (img) => {
      return await Jimp.read(img).then((x) =>
        x.quality(1).getBufferAsync(Jimp.MIME_JPEG)
      );
    },
    "jpeg.jpg"
  );
};
hf.registerCommand(jpeg);
