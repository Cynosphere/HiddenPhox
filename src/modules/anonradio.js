/*const GUILD_ID = "300436792916836352";
const THREAD_ID = "1054106209273253888";
const MESSAGE_ID = "1054109813132509245";*/

if (hf.__anonradio_timeout) {
  clearTimeout(hf.__anonradio_timeout);
}

/*async function updateNowPlaying() {
  let playing;
  try {
    playing = await fetch("https://anonradio.net/playing").then((res) => res.text());
  } catch (err) {
    try {
      playing = await fetch("http://anonradio.net/playing").then((res) => res.text());
    } catch (err) {
      //
    }
  }
  let schedule;
  try {
    schedule = await fetch("https://anonradio.net/schedule/").then((res) => res.text());
  } catch (err) {
    try {
      schedule = await fetch("http://anonradio.net/schedule/").then((res) => res.text());
    } catch (err) {
      //
    }
  }

  if (!playing || !schedule) {
    hf.__anonradio_timeout = setTimeout(updateNowPlaying, 2000);
    return;
  }

  let lines = schedule.split("\n");
  lines = lines.slice(4, lines.length - 2);
  const line = lines[0];

  if (!line) {
    hf.__anonradio_timeout = setTimeout(updateNowPlaying, 2000);
    return;
  }

  const [_, _time, id, name] = line.match(/^(.{3,4} .{4})\s+(.+?) {2}(.+?)$/);

  const liveNow = {name: "ident", id: "aNONradio"};

  if (name.includes("<- Live NOW")) {
    liveNow.id = id;
    liveNow.name = name.replace("<- Live NOW", "").trim();
  }

  let title = "";
  let subtitle = "";

  if (playing.includes("listeners with a daily peak of")) {
    title = `${liveNow.name} (\`${liveNow.id}\`)`;
    subtitle = playing;
  } else if (playing.startsWith("Coming up")) {
    title = playing;
  } else {
    const metadataLine = playing.match(/\[(\d+)\/(\d+)\/(\d+)\] \((.+?)\): (.+)/);
    const current = metadataLine?.[1] ?? "??";
    const peakDay = metadataLine?.[2] ?? "??";
    const peakMonth = metadataLine?.[3] ?? "??";
    const dj = metadataLine?.[4] ?? "unknown";
    const metadata = metadataLine?.[5] ?? "unknown";

    if (metadata == "https://archives.anonradio.net" || liveNow.name == "Synth Battle Royale") {
      title = `${liveNow.name} (\`${dj}\`)`;
    } else {
      title = `${metadata} (\`${dj}\`)`;
    }
    subtitle = `${current} listening with a daily peak of ${peakDay} and ${peakMonth} peak for the month.`;
  }

  let openmicTime = "";
  if (liveNow.id == "openmic") {
    try {
      const icecast = await fetch("http://anonradio.net:8010/status-json.xsl")
        .then((res) => res.text())
        .then((data) => JSON.parse(data.replace(/"title": - ,/g, '"title":" - ",')));
      const streamData = icecast.icestats.source.find((src) => src.listenurl == "http://anonradio.net:8010/openmic");
      if (streamData && streamData.stream_start_iso8601) {
        const startTime = new Date(streamData.stream_start_iso8601).getTime();
        title = `${streamData.title} (\`${
          streamData.server_name ? streamData.server_name + " | " + liveNow.id : liveNow.id
        }\`)`;
        openmicTime = `-\\*- OpenMIC DJ has been streaming since <t:${Math.round(startTime / 1000)}:R> -\\*-\n`;
      }
    } catch {
      //
    }
  }

  const now = Date.now();
  const timestamp = `<t:${Math.floor(now / 1000)}:T>`;

  const content = `${title}\n${subtitle}\n${openmicTime}`.trim();
  const thread = hf.bot.guilds.get(GUILD_ID)?.threads.get(THREAD_ID);
  if (thread) {
    try {
      const msg = await thread.getMessage(MESSAGE_ID);
      const oldContent = msg.content.replace(/<t:\d+:T>\n/, "").trim();
      if (oldContent !== content) {
        await thread.editMessage(MESSAGE_ID, {
          content: timestamp + "\n" + content,
        });
      }
    } catch {
      //
    }
  }

  hf.__anonradio_timeout = setTimeout(updateNowPlaying, 2000);
}
updateNowPlaying();

/*hf.timer.add(
  "anonradio",
  async () => {
    await new Promise((resolve, reject) => {
      setTimeout(() => reject("timeout"), 1900);
      updateNowPlaying().then(() => resolve(true));
    }).catch(() => {});
  },
  2000
);*/
