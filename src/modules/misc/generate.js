const Command = require("#lib/command.js");

const {safeString} = require("#util/misc.js");
const {formatTime} = require("#util/time.js");

const GENERATE_HEADERS = {
  Accept: "application/json",
  "Content-Type": "application/json",
};

const generate = new Command("generate");
generate.category = "misc";
generate.helpText = "Generate images from prompt via craiyon";
generate.callback = async function (msg, line) {
  if (!line || line.length === 0) return "Arguments required.";
  msg.channel.sendTyping();

  const start = Date.now();
  let retries = 0;

  let request = await fetch("https://backend.craiyon.com/generate", {
    method: "POST",
    headers: GENERATE_HEADERS,
    body: JSON.stringify({prompt: line}),
  });
  while (request.status !== 200) {
    request = await fetch("https://backend.craiyon.com/generate", {
      method: "POST",
      headers: GENERATE_HEADERS,
      body: JSON.stringify({prompt: line}),
    });
    retries++;
  }

  const data = await request.json();
  const images = data.images
    .map((img) => Buffer.from(img, "base64"))
    .map((img, index) => ({contents: img, name: `${index}.jpg`}));

  const title = `Responses for "${safeString(line)}"`;

  const out = {
    content: `Generated in ${formatTime(Date.now() - start)}${retries > 0 ? " with " + retries + " retries" : ""}`,
    embeds: [],
    files: images,
  };

  let splitIndex = 0;
  for (const index in images) {
    if (index % 3 == 0) splitIndex++;
    out.embeds.push({
      title,
      url: "https://www.craiyon.com/?" + splitIndex,
      image: {
        url: `attachment://${index}.jpg`,
      },
    });
  }

  return out;
};
hf.registerCommand(generate);
