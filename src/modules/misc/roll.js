const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");
const {ApplicationCommandOptionTypes} = require("#util/dconstants.js");

const roll = new Command("roll");
roll.category = "misc";
roll.helpText = "Roll a dice";
roll.usage = "<sides>";
roll.callback = function (msg, line) {
  if (line == null || line == "") line = 6;
  line = Number(line);
  if (Number.isNaN(line)) line = 6;
  if (line < 0) line = Math.abs(line);

  if (line == 0) return ":hole:";
  if (line == 1) return ":one:";

  const res = Math.floor(Math.random() * line);

  if (line == 2) {
    return `:coin:: ${res == 1 ? "Heads" : "Tails"}`;
  }

  return `:game_die: (d${line}): ${1 + res}`;
};
hf.registerCommand(roll);

const rollInteraction = new InteractionCommand("roll");
rollInteraction.helpText = "Roll a dice";
rollInteraction.options.sides = {
  name: "sides",
  type: ApplicationCommandOptionTypes.NUMBER,
  description: "How many sides to pick from",
  required: false,
  default: 6,
};
rollInteraction.callback = async function (interaction) {
  const sides = this.getOption(interaction, "sides");

  return roll.callback(interaction, sides);
};
hf.registerCommand(rollInteraction);
