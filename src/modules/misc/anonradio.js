const Command = require("#lib/command.js");

const {formatTime} = require("#util/time.js");

const DAYS = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const anonradio = new Command("anonradio");
anonradio.category = "misc";
anonradio.helpText = "aNONradio.net schedule";
anonradio.callback = async function () {
  const now = new Date();

  let playing;
  try {
    playing = await fetch("https://anonradio.net/playing").then((res) => res.text());
  } catch (err) {
    try {
      playing = await fetch("http://anonradio.net/playing").then((res) => res.text());
    } catch (err) {
      //
    }
  }
  let schedule;
  try {
    schedule = await fetch("https://anonradio.net/schedule/").then((res) => res.text());
  } catch (err) {
    try {
      schedule = await fetch("http://anonradio.net/schedule/").then((res) => res.text());
    } catch (err) {
      //
    }
  }

  if (!playing || !schedule) return "Failed to fetch schedule.";

  const icecast = await fetch("http://anonradio.net:8010/status-json.xsl")
    .then((res) => res.text())
    .then((data) => JSON.parse(data.replace(/"title": - ,/g, '"title":" - ",')));

  let lines = schedule.split("\n");
  lines = lines.slice(4, lines.length - 2);

  const parsedLines = [];

  for (const line of lines) {
    const [_, time, id, name] = line.match(/^(.{3,4} .{4})\s+(.+?) {2}(.+?)$/);

    const tmp = time.split(" ");
    const day = tmp[0];
    let hour = tmp[1];
    const currentDay = now.getUTCDay();
    const targetDay = DAYS.indexOf(day);
    const delta = (targetDay + 7 - currentDay) % 7;

    let currentYear = now.getUTCFullYear();
    const currentMonth = now.getUTCMonth() + 1;
    const currentDateDay = now.getUTCDate();
    let targetMonth = currentMonth;
    const lastDay = new Date(currentYear, currentMonth, 0).getDate();
    let targetDateDay = currentDateDay + delta;
    if (targetDateDay > lastDay) {
      targetMonth = currentMonth === 12 ? 1 : currentMonth + 1;
      targetDateDay = 1;
      if (currentMonth === 12) currentYear++;
    }

    hour = hour.slice(0, 2) + ":" + hour.slice(-2);

    const timestamp =
      Date.parse(`${DAYS[targetDay]}, ${currentYear}-${targetMonth}-${targetDateDay} ${hour} UTC`) / 1000;

    let nameOut = name;

    if (time == "Sat 0300") nameOut = name.replace("Open Mic - Anyone can stream", "Synth Battle Royale");

    parsedLines.push({
      timestamp,
      id,
      name: nameOut.replace("  <- Up NEXT", ""),
    });
  }

  let liveNow = {name: "ident", id: "aNONradio"};

  if (parsedLines[0].name.includes("<- Live NOW")) {
    liveNow = parsedLines.splice(0, 1)[0];
    liveNow.name = liveNow.name.replace("  <- Live NOW", "");
  }

  let title = "";
  let subtitle = "";

  if (playing.includes("listeners with a daily peak of")) {
    title = `${liveNow.name} (\`${liveNow.id}\`)`;
    subtitle = playing;
  } else {
    const [_, current, peakDay, peakMonth, dj, metadata] = playing.match(/\[(\d+)\/(\d+)\/(\d+)\] \((.+?)\): (.+)/);

    if (metadata == "https://archives.anonradio.net" || liveNow.name == "Synth Battle Royale") {
      title = `${liveNow.name} (\`${dj}\`)`;
    } else {
      title = `${metadata} (\`${dj}\`)`;
    }
    subtitle = `${current} listening with a daily peak of ${peakDay} and ${peakMonth} peak for the month.`;
  }

  let openmicTime = "";
  if (liveNow.id == "openmic") {
    const streamData = icecast.icestats.source.find((src) => src.listenurl == "http://anonradio.net:8010/openmic");
    if (streamData && streamData.stream_start_iso8601) {
      const startTime = new Date(streamData.stream_start_iso8601).getTime();
      openmicTime = `-\\*- OpenMIC DJ has been streaming for ${formatTime(Date.now() - startTime)} -\\*-\n`;
    }
  }

  return {
    embeds: [
      {
        title: subtitle,
        author: {
          name: title,
          url: "http://anonradio.net:8000/anonradio",
        },
        description: openmicTime + "__Schedule:__",
        fields: parsedLines.map((line) => ({
          inline: true,
          name: `${line.name} (\`${line.id}\`)`,
          value: `<t:${line.timestamp}:R>`,
        })),
      },
    ],
  };
};
hf.registerCommand(anonradio);
