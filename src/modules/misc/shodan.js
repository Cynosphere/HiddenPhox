const Command = require("#lib/command.js");

const REGEX_IPV4 = /^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)(\.(?!$)|$)){4}$/;

const shodan = new Command("shodan");
shodan.category = "misc";
shodan.helpText = "Look up an IP on Shodan InternetDB";
shodan.callback = async function (msg, line) {
  if (!line || line == "") return "Arguments required.";
  if (!REGEX_IPV4.test(line)) return "Invalid IP address.";

  const data = await fetch("https://internetdb.shodan.io/" + line).then((res) => res.json());

  if (data.detail) return data.detail;

  return {
    embed: {
      title: `Results for \`${data.ip}\``,
      fields: [
        {
          name: "Hostnames",
          value: data.hostnames.length > 0 ? data.hostnames.map((str) => `\`${str}\``).join(" ") : "None",
          inline: true,
        },
        {
          name: "Open ports",
          value: data.ports.length > 0 ? data.ports.join(", ") : "None",
          inline: true,
        },
        {
          name: "Tags",
          value: data.tags.length > 0 ? data.tags.map((str) => `\`${str}\``).join(", ") : "None",
          inline: true,
        },
        {
          name: "CPEs",
          value: data.cpes.length > 0 ? data.cpes.map((str) => `\`${str}\``).join(" ") : "None",
          inline: true,
        },
        {
          name: "Vulnerabilities",
          value: data.vulns.length > 0 ? data.vulns.map((str) => `\`${str}\``).join(" ") : "None",
          inline: true,
        },
      ],
    },
  };
};
hf.registerCommand(shodan);
