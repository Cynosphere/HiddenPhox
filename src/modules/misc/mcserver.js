const Command = require("#lib/command.js");
const logger = require("#lib/logger.js");

const net = require("node:net");
const {resolveCname, resolveSrv} = require("node:dns/promises");

function readVarInt(data) {
  var result = 0;
  var offset = 0;
  var pos = 0;
  for (; offset < 32; offset += 7) {
    var b = data[pos];
    if (b == -1) {
      throw new Error("Malformed varint");
    }
    result |= (b & 0x7f) << offset;
    if ((b & 0x80) == 0) {
      return result;
    }
    pos++;
  }
  // Keep reading up to 64 bits.
  for (; offset < 64; offset += 7) {
    b = data[pos];
    if (b == -1) {
      throw new Error("Truncated message");
    }
    if ((b & 0x80) == 0) {
      return result;
    }
    pos++;
  }

  return null;
}

function writeVarInt(value) {
  var buf = [];
  // eslint-disable-next-line no-constant-condition
  while (true) {
    if ((value & ~0x7f) === 0) {
      buf.push(value);
      return Buffer.from(buf);
    } else {
      buf.push((value & 0x7f) | 0x80);
      value >>>= 7;
    }
  }
}

const ident = Buffer.from("HiddenPhox (c7.pm) ", "utf8");
const identPort = Buffer.alloc(2);
identPort.writeUInt16BE(3);

const handshake = Buffer.concat([
  writeVarInt(0x0),
  writeVarInt(1073741953),
  writeVarInt(ident.length),
  ident,
  identPort,
  writeVarInt(1),
]);
const handshakeWithLength = Buffer.concat([writeVarInt(handshake.length), handshake]);

const status = Buffer.concat([writeVarInt(1), writeVarInt(0x0)]);

const HANDSHAKE_PACKET = Buffer.concat([handshakeWithLength, status]);

const formattingToAnsi = {
  r: "0",
  l: "1",
  m: "9",
  n: "4",
  o: "3",
  0: "30",
  1: "34",
  2: "32",
  3: "36",
  4: "31",
  5: "35",
  6: "33",
  7: "37",
  8: "30", // "90",
  9: "34", // "94",
  a: "32", // "92",
  b: "36", // "96",
  c: "31", // "91",
  d: "35", // "95",
  e: "33", // "93",
  f: "37", // "97",
};

function queryServer(ip, port, HANDSHAKE_PACKET) {
  return new Promise((resolve, reject) => {
    logger.verbose("mcserver", "querying", ip, port);

    try {
      const client = net.createConnection(
        {
          host: ip,
          port: port,
          timeout: 60000,
        },
        function () {
          logger.verbose("mcserver", "connect");
          client.write(HANDSHAKE_PACKET);
        }
      );
      const timeout = setTimeout(() => {
        logger.verbose("mcserver", "timeout");
        client.destroy();
        resolve("timeout");
      }, 60000);

      let totalData = Buffer.alloc(0);
      let length = 0;
      client.on("data", function (data) {
        if (length == 0) {
          length = readVarInt(data);
          logger.verbose("mcserver", "expected data length", length);
        }
        if (length > 0) {
          totalData = Buffer.concat([totalData, data]);
          if (totalData.length >= length) {
            client.end();
          }
        }
        logger.verbose("mcserver", "data", data.length, totalData.length);
      });

      client.on("close", function (err) {
        if (err) {
          logger.verbose("mcserver", "close with error", err);
          return reject(err);
        }
        const dataAsString = totalData.toString().trim();
        logger.verbose("mcserver", "data as string", dataAsString);
        if (dataAsString.length > 0) {
          try {
            const json = JSON.parse(dataAsString.slice(dataAsString.indexOf("{"), dataAsString.lastIndexOf("}") + 1));
            logger.verbose("mcserver", "close", json);
            clearTimeout(timeout);
            return resolve(json);
          } catch (err) {
            return resolve({error: err});
          }
        } else {
          return resolve({error: new Error("Server returned no data.")});
        }
      });
      client.on("timeout", function () {});
      client.on("error", function (err) {
        logger.verbose("mcserver", "failed:", err);
        resolve({error: err});
      });
    } catch (err) {
      logger.verbose("mcserver", "failed:", err);
      resolve({error: err});
    }
  });
}

async function queryServerIdent(ip, port) {
  return await queryServer(ip, port, HANDSHAKE_PACKET);
}

async function queryServerNoIdent(ip, port) {
  const ident = Buffer.from(ip, "utf8");
  const identPort = Buffer.alloc(2);
  identPort.writeUInt16BE(port);

  const handshake = Buffer.concat([
    writeVarInt(0x0),
    writeVarInt(1073741953),
    writeVarInt(ident.length),
    ident,
    identPort,
    writeVarInt(1),
  ]);
  const handshakeWithLength = Buffer.concat([writeVarInt(handshake.length), handshake]);

  const status = Buffer.concat([writeVarInt(1), writeVarInt(0x0)]);

  const HANDSHAKE_PACKET = Buffer.concat([handshakeWithLength, status]);

  return await queryServer(ip, port, HANDSHAKE_PACKET);
}

const mcserver = new Command("mcserver");
mcserver.category = "misc";
mcserver.helpText = "Query a Minecraft server";
mcserver.callback = async function (msg, line) {
  if (!line || line == "") return "Arguments required.";

  const split = line.split(":");
  let ip = split[0];
  const port = split[1] ?? 25565;

  try {
    const addrs = await resolveSrv("_tcp." + ip);
    if (addrs.length > 0) {
      const mcAddr = addrs.find((a) => a.port == port);
      if (mcAddr?.name) ip = mcAddr.name;
    } else {
      const addrs = await resolveCname("_tcp." + ip);
      if (addrs.length > 0) ip = addrs[0];
    }
  } catch {
    // noop
  }

  await msg.addReaction("\uD83C\uDFD3");

  let data;
  let tried = false;
  try {
    data = await queryServerIdent(ip, port);
  } catch {
    try {
      tried = true;
      data = await queryServerNoIdent(ip, port);
    } catch (err) {
      await msg.removeReaction("\uD83C\uDFD3");
      return `Failed to query:\n\`\`\`\n${data.error.message}\n\`\`\``;
    }
  }

  if (data?.error && !tried) {
    try {
      tried = true;
      data = await queryServerNoIdent(ip, port);
    } catch (err) {
      await msg.removeReaction("\uD83C\uDFD3");
      return `Failed to query:\n\`\`\`\n${data.error.message}\n\`\`\``;
    }
  }

  if (data == "timeout") {
    await msg.removeReaction("\uD83C\uDFD3");
    return "Timed out trying to query.";
  } else if (data?.error) {
    await msg.removeReaction("\uD83C\uDFD3");
    return `Failed to query:\n\`\`\`\n${data.error.message}\n\`\`\``;
  } else {
    let tcpshield = false;
    if (data.version.name == "TCPShield.com") {
      data = await queryServerNoIdent(ip, port);
      tcpshield = true;
    }

    await msg.removeReaction("\uD83C\uDFD3");

    const desc =
      typeof data.description == "string"
        ? data.description
        : data.description?.text ?? "<server didn't properly send motd>";
    const motd = desc.replace(/\u00a7([a-f0-9k-or])/gi, (_, formatting) => {
      const ansi = formattingToAnsi[formatting];
      return ansi ? `\x1b[${ansi}m` : "";
    });

    const players = data.players?.sample?.map((player) => player.name) ?? [];
    const totalPlayers = `(${data.players.online}/${data.players.max})`;
    let image;
    if (data.favicon) {
      image = Buffer.from(data.favicon.slice(data.favicon.indexOf(",")), "base64");
    }

    return {
      embed: {
        title: `Server info for: \`${line}\``,
        fields: [
          {
            name: "MOTD",
            value: `\`\`\`ansi\n${motd}\n\`\`\``,
          },
          {
            name: "Version",
            value: `${data.version.name} (\`${data.version.protocol}\`)`,
            inline: true,
          },
          {
            name: `Players ${players.length > 0 ? totalPlayers : ""}`,
            value: players.length > 0 ? players.join(", ") : totalPlayers,
            inline: players.length == 0,
          },
        ],
        thumbnail: image && {
          url: "attachment://icon.png",
        },
        footer: {
          text: tcpshield ? "Server uses TCPShield" : null,
        },
      },
      file: image && {
        file: image,
        name: "icon.png",
      },
    };
  }
};
hf.registerCommand(mcserver);
