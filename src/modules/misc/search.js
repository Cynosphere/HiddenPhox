const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {ApplicationCommandOptionTypes} = require("#util/dconstants.js");

const {safeString} = require("#util/misc.js");
const {parseHtmlEntities} = require("#util/html.js");

const search = new Command("search");
search.category = "misc";
search.helpText = "Search, powered by LibreX";
search.addAlias("g");
search.addAlias("google");
search.addAlias("ddg");
search.callback = async function (msg, line, args, {results = 2}) {
  const query = args.join(" ");
  if (!hf.config.librex) return "LibreX instance not defined.";
  if (!query || query == "") return "Search query required.";

  const encodedQuery = encodeURIComponent(query);

  if (query.startsWith("!")) {
    const url = `https://api.duckduckgo.com/?q=${encodedQuery}&format=json`;
    const res = await fetch(url);
    if (res.url != url) return res.url;
  }

  const res = await fetch(`${hf.config.librex}/api.php?q=${encodedQuery}&p=0&t=0`).then((res) => res.json());
  if (res.error?.message) {
    if (res.error.message.indexOf("No results found.") > -1) {
      return "Search returned no results.";
    } else {
      return `Search returned error:\n\`\`\`\n${res.error.message}\`\`\``;
    }
  } else {
    const searchResults = Object.values(res)
      .filter((result) => result.did_you_mean == null && typeof result !== "string")
      .splice(0, Number(results));

    if (searchResults.length > 0) {
      let out = `__**Results for \`${safeString(query)}\`**__\n`;
      for (const result of searchResults) {
        if (result.special_response) {
          out += "> " + safeString(parseHtmlEntities(result.special_response.response.split("\n").join("\n> ")));
          out += `\n<${encodeURI(result.special_response.source)}>`;
        } else {
          out += `**${safeString(parseHtmlEntities(result.title)).trim()}** - <${encodeURI(result.url)}>`;
          out += `\n> ${safeString(parseHtmlEntities(result.description))}`;
        }
        out += "\n\n";
      }

      return out.trim();
    } else {
      return "Search returned no results.";
    }
  }
};
hf.registerCommand(search);

const searchInteraction = new InteractionCommand("search");
searchInteraction.helpText = "Search, powered by LibreX";
searchInteraction.options.query = {
  name: "query",
  type: ApplicationCommandOptionTypes.STRING,
  description: "What to search for",
  required: true,
  default: "",
};
searchInteraction.options.results = {
  name: "results",
  type: ApplicationCommandOptionTypes.INTEGER,
  description: "How many results to show",
  required: false,
  min_value: 1,
  max_value: 10,
  default: 2,
};
searchInteraction.callback = async function (interaction) {
  const query = this.getOption(interaction, "query");
  const results = this.getOption(interaction, "results");

  return search.callback(interaction, query, [query], {results});
};
hf.registerCommand(searchInteraction);
