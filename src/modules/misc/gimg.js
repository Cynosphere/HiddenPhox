const Command = require("#lib/command.js");

const GoogleImages = require("google-images");

const imagesClient = new GoogleImages(hf.apikeys.gimg, hf.apikeys.google);

const gimg = new Command("gimg");
gimg.category = "misc";
gimg.helpText = "Search Google Images";
gimg.usage = "[query]";
gimg.addAlias("img");
gimg.callback = async function (msg, line) {
  if (!line) return "No arguments given.";

  const images = await imagesClient.search(line, {
    safe: msg.channel.nsfw && !msg.channel?.topic.includes("[no_nsfw]") ? "off" : "high",
  });

  const index = Math.floor(Math.random() * images.length);
  const image = images[index];

  return {
    embeds: [
      {
        title: image.description,
        url: image.parentPage,
        image: {
          url: image.url,
        },
        footer: {
          text: `Image ${Number(index) + 1}/${images.length}. Rerun to get a different image.`,
        },
      },
    ],
  };
};
hf.registerCommand(gimg);

const fimg = new Command("fimg");
fimg.category = "misc";
fimg.helpText = "Send first result from Google Images";
fimg.usage = "[query]";
fimg.callback = async function (msg, line) {
  if (!line) return "No arguments given.";

  const images = await imagesClient.search(line, {
    safe: msg.channel.nsfw && !msg.channel?.topic.includes("[no_nsfw]") ? "off" : "high",
  });

  const image = images[0];

  return {
    embeds: [
      {
        title: image.description,
        url: image.parentPage,
        image: {
          url: image.url,
        },
      },
    ],
  };
};
hf.registerCommand(fimg);
