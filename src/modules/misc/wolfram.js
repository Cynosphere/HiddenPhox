const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {ApplicationCommandOptionTypes} = require("#util/dconstants.js");
const {Icons} = require("#util/constants.js");

const {safeString} = require("#util/misc.js");

const WA_NO_ANSWER = `${Icons.silk.cross} No answer.`;

const wolfram = new Command("wolfram");
wolfram.category = "misc";
wolfram.helpText = "Wolfram Alpha";
wolfram.usage = "<-v> [query]";
wolfram.addAlias("wa");
wolfram.addAlias("calc");
wolfram.callback = async function (msg, line, args, {verbose, v}) {
  const _verbose = verbose ?? v;
  const query = args.join(" ");

  const req = await fetch(
    `http://api.wolframalpha.com/v2/query?input=${encodeURIComponent(query)}&appid=LH2K8H-T3QKETAGT3&output=json`
  ).then((x) => x.json());

  const data = req.queryresult.pods;

  if (!data) return WA_NO_ANSWER;

  // fake no answer
  //if (data[0].subpods[0].plaintext.includes("geoIP")) return WA_NO_ANSWER;

  if (_verbose) {
    const embed = {
      title: `Result for: \`${safeString(query)}\``,
      fields: [],
      footer: {
        icon_url: "http://www.wolframalpha.com/share.png",
        text: "Powered by Wolfram Alpha",
      },
      image: {
        url: data[1].subpods[0].img.src,
      },
    };

    const extra = data.slice(1, 6);
    for (const x in extra) {
      embed.fields.push({
        name: extra[x].title,
        value: `[${extra[x].subpods[0].plaintext.length > 0 ? extra[x].subpods[0].plaintext : "<click for image>"}](${
          extra[x].subpods[0].img.src
        })`,
        inline: true,
      });
    }

    return {embed};
  } else {
    let image;

    if (data[1].subpods[0].img.src)
      try {
        const res = await fetch(data[1].subpods[0].img.src);
        if (res) {
          const imgData = await res.arrayBuffer();
          image = Buffer.from(imgData);
        }
      } catch {
        //
      }

    let string = "";
    if (data[1].subpods[0].plaintext.length > 0) string = safeString(data[1].subpods[0].plaintext);

    let text;
    if (string.length > 2000 - (6 + safeString(query).length)) {
      text = string;
      string = "Output too long:";
    }

    return {
      content: `\`${safeString(query)}\` -> ${string.length > 0 ? string : ""}`,
      attachments: [
        text && {
          file: text,
          filename: "message.txt",
        },
        image && {
          file: image,
          filename: "wolfram_output.gif",
        },
      ].filter((x) => !!x),
    };
  }
};
hf.registerCommand(wolfram);

const wolframInteraction = new InteractionCommand("wolfram");
wolframInteraction.helpText = "Wolfram Alpha";
wolframInteraction.options.query = {
  name: "query",
  type: ApplicationCommandOptionTypes.STRING,
  description: "What to query Wolfram Alpha for",
  required: true,
  default: "",
};
wolframInteraction.options.verbose = {
  name: "verbose",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Verbose output",
  required: false,
  default: false,
};
wolframInteraction.callback = async function (interaction) {
  const query = this.getOption(interaction, "query");
  const verbose = this.getOption(interaction, "verbose");

  return wolfram.callback(interaction, query, [query], {verbose});
};
hf.registerCommand(wolframInteraction);
