const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {ApplicationCommandOptionTypes} = require("#util/dconstants.js");

const {safeString} = require("#util/misc.js");
const {parseHtmlEntities} = require("#util/html.js");

const yt = new Command("youtube");
yt.addAlias("yt");
yt.category = "misc";
yt.helpText = "Search YouTube";
yt.usage = "[search term]";
yt.callback = async function (msg, line) {
  if (!line) return "Arguments are required.";

  const req = await fetch(
    `https://www.googleapis.com/youtube/v3/search?key=${
      hf.apikeys.google
    }&maxResults=5&part=snippet&type=video&q=${encodeURIComponent(line)}`
  ).then((res) => res.json());

  const topVid = req.items[0];

  let out = `**${safeString(parseHtmlEntities(topVid.snippet.title))}** | \`${safeString(
    parseHtmlEntities(topVid.snippet.channelTitle)
  )}\`\nhttps://youtu.be/${topVid.id.videoId}\n\n**__See Also:__**\n`;

  for (let i = 1; i < 5; i++) {
    const vid = req.items[i];
    if (!vid) continue;

    out += `- **${safeString(parseHtmlEntities(vid.snippet.title))}** | By: \`${safeString(
      parseHtmlEntities(vid.snippet.channelTitle)
    )}\` | <https://youtu.be/${vid.id.videoId}>\n`;
  }

  return out;
};
hf.registerCommand(yt);

const ytInteraction = new InteractionCommand("youtube");
ytInteraction.helpText = "Search Youtube";
ytInteraction.options.search = {
  name: "search",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Search query",
  required: true,
  default: "",
};
ytInteraction.callback = async function (interaction) {
  const search = this.getOption(interaction, "search");

  return yt.callback(interaction, search);
};
hf.registerCommand(ytInteraction);

const fyt = new Command("fyt");
fyt.category = "misc";
fyt.helpText = "Search YouTube and take the first result.";
fyt.usage = "[search term]";
fyt.callback = async function (msg, line) {
  if (!line) return "Arguments are required.";

  const req = await fetch(
    `https://www.googleapis.com/youtube/v3/search?key=${
      hf.apikeys.google
    }&maxResults=2&part=snippet&type=video&q=${encodeURIComponent(line)}`
  ).then((res) => res.json());

  const vid = req.items[0];

  return `**${safeString(parseHtmlEntities(vid.snippet.title))}** | \`${safeString(
    parseHtmlEntities(vid.snippet.channelTitle)
  )}\`\nhttps://youtu.be/${vid.id.videoId}`;
};
hf.registerCommand(fyt);

const fytInteraction = new InteractionCommand("fyt");
fytInteraction.helpText = "Search Youtube and take the first result.";
fytInteraction.options.search = {
  name: "search",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Search query",
  required: true,
  default: "",
};
fytInteraction.callback = async function (interaction) {
  const search = this.getOption(interaction, "search");

  return fyt.callback(interaction, search);
};
hf.registerCommand(fytInteraction);
