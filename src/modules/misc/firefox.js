const Command = require("#lib/command.js");

const firefox = new Command("firefox");
firefox.category = "misc";
firefox.helpText = "Get the approximate latest Firefox user-agent";
firefox.callback = function (msg, line, args, {windows}) {
  const now = Math.floor(Date.now() / 1000);
  const version = Math.floor(124 + (now - 1710892800) / 2419200);
  let os = "X11; Linux x86_64";
  if (windows) os = "Windows NT 10.0; Win64; x64;";

  return `\`Mozilla/5.0 (${os} rv:${version}.0) Gecko/20100101 Firefox/${version}.0\``;
};
hf.registerCommand(firefox);
