const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");
const {ApplicationCommandOptionTypes} = require("#util/dconstants.js");

const {default: Chatsounds, defaultModifiers} = require("sh");

const sh = new Chatsounds({modifiers: defaultModifiers, gitHubToken: hf.apikeys.github});
(async () => {
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "csgo");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "css");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "ep1");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "ep2");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "hl1");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "hl2");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "l4d");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "l4d2");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "portal");
  await sh.useSourcesFromGitHubMsgPack("PAC3-Server/chatsounds-valve-games", "master", "tf2");
  await sh.useSourcesFromGitHub("PAC3-Server/chatsounds", "master", "sounds/chatsounds");
  await sh.useSourcesFromGitHub("Metastruct/garrysmod-chatsounds", "master", "sound/chatsounds/autoadd");
  sh.mergeSources();
})();

const chatsounds = new Command("chatsounds");
chatsounds.addAlias("chatsound");
chatsounds.addAlias("saysound");
chatsounds.category = "misc";
chatsounds.helpText = "Garry's Mod chatsounds as a file output";
chatsounds.callback = async function (msg, line) {
  const context = sh.new(line);
  const audio = await context.buffer({sampleRate: 48000, audioChannels: 2, format: "ogg", codec: "libopus"});

  return {
    flags: 1 << 13,
    attachments: [
      {
        file: audio,
        filename: "voice-message.ogg",
        // FIXME: lazy
        duration_secs: 1,
        waveform: "",
      },
    ],
  };
};
hf.registerCommand(chatsounds);

const chatsoundsInteraction = new InteractionCommand("chatsounds");
chatsoundsInteraction.helpText = "Garry's Mod chatsounds as a file output";
chatsoundsInteraction.options.input = {
  name: "input",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Chatsounds to parse",
  required: true,
  default: "",
};
chatsoundsInteraction.callback = async function (interaction) {
  const input = this.getOption(interaction, "input");

  return chatsounds.callback(interaction, input);
};
hf.registerCommand(chatsoundsInteraction);
