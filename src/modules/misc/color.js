const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");
const {ApplicationCommandOptionTypes} = require("#util/dconstants.js");

const {safeString} = require("#util/misc.js");

const {tinycolor, random: randomColor} = require("@ctrl/tinycolor");
const sharp = require("sharp");
const fs = require("node:fs");

const colornamesRaw = fs.readFileSync(require.resolve("#root/data/colornames.csv"), "utf8");

const color = new Command("color");
color.category = "misc";
color.helpText = "Show information on a color or get a random color";
color.callback = async function (msg, line, args, {truerandom, first}) {
  const query = args.join(" ");
  let color = tinycolor(query),
    random = false;

  if (!query || query == "") {
    color = truerandom ? tinycolor(Math.floor(Math.random() * 0xffffff)) : randomColor();
    random = true;
  }

  if (!color.isValid) {
    const search = `^([0-9a-f]{6}),${query},`;
    if (!first) {
      const colornamesMatches = colornamesRaw.match(new RegExp(search, "img"));
      if (colornamesMatches?.length > 1) {
        const hexes = colornamesMatches.map((m) => m.split(",")[0]);
        const random = hexes[Math.floor(Math.random() * hexes.length)];

        const left = "- #" + hexes.slice(0, Math.ceil(hexes.length / 2)).join("\n- #");
        const right = "- #" + hexes.slice(Math.ceil(hexes.length / 2), hexes.length).join("\n- #");

        return {
          embed: {
            color: parseInt(`0x${random}`),
            footer: {
              text: `Picked #${random} for embed color \u2022 Use \`--first\` or the \`first\` option to bypass this`,
            },
            fields: [
              {name: `Got ${colornamesMatches.length} matches for \`${safeString(query)}\``, value: left, inline: true},
              {name: "\u200b", value: right, inline: true},
            ],
          },
        };
      }
    }

    const colornamesHex = colornamesRaw.match(new RegExp(search, "im"))?.[1];
    if (colornamesHex) {
      color = tinycolor(colornamesHex);
    } else {
      return "Color not valid.";
    }
  }

  const image = await sharp({
    create: {
      width: 128,
      height: 128,
      channels: 3,
      background: {r: color.r, g: color.g, b: color.b},
    },
  })
    .png()
    .toBuffer();
  const hex = color.toHex();
  const fileName = `${hex}.png`;

  const fields = [
    {
      name: "Hex",
      value: color.toHexString(),
      inline: true,
    },
    {
      name: "RGB",
      value: color.toRgbString(),
      inline: true,
    },
    {
      name: "HSL",
      value: color.toHslString(),
      inline: true,
    },
    {
      name: "HSV",
      value: color.toHsvString(),
      inline: true,
    },
    {
      name: "Integer",
      value: color.toNumber(),
      inline: true,
    },
  ];

  if (color.toName() != false) {
    fields.splice(0, 0, {
      name: "Name",
      value: color.toName(),
      inline: true,
    });
  }

  const colornamesName = colornamesRaw.match(new RegExp(`^${hex},([^,]+?),`, "im"))?.[1];
  if (colornamesName) {
    fields.push({
      name: "\u200b",
      value: `**[colornames](https://colornames.org/color/${hex}) Name**\n${colornamesName}`,
    });
  }

  return {
    embeds: [
      {
        title: random ? "Random Color" : "",
        color: color.toNumber(),
        thumbnail: {
          url: `attachment://${fileName}`,
        },
        fields,
      },
    ],
    file: {
      file: image,
      name: fileName,
    },
  };
};
hf.registerCommand(color);

const colorInteraction = new InteractionCommand("color");
colorInteraction.helpText = "Show information on a color or get a random color";
colorInteraction.options.input = {
  name: "input",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Color to get info on",
  required: false,
  default: "",
};
colorInteraction.options.truerandom = {
  name: "truerandom",
  type: ApplicationCommandOptionTypes.BOOLEAN,
  description: "Should the random color give a 'true random' color instead of an adjust color",
  required: false,
  default: false,
};
colorInteraction.options.first = {
  name: "first",
  type: ApplicationCommandOptionTypes.BOOLEAN,
  description: "Take first match for a colornames color by name",
  required: false,
  default: false,
};
colorInteraction.callback = async function (interaction) {
  const input = this.getOption(interaction, "input");
  const truerandom = this.getOption(interaction, "truerandom");
  const first = this.getOption(interaction, "first");

  return color.callback(interaction, input, [input], {truerandom, first});
};
hf.registerCommand(colorInteraction);
