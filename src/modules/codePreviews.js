const {MessageFlags, Routes} = require("oceanic.js");

const events = require("../lib/events.js");
const {hasFlag} = require("../lib/guildSettings.js");

const REGEX_GITHUB =
  /(?:\s|^)https?:\/\/(www\.)?github\.com\/.+?\/.+?\/blob\/([a-zA-Z0-9-_.#/]*)/g;
const REGEX_GITLAB =
  /(?:\s|^)https?:\/\/.+?\/.+?\/.+?\/-\/blob\/([a-zA-Z0-9-_.#/]*)/g;
const REGEX_GITEA =
  /(?:\s|^)https?:\/\/.+?\/.+?\/.+?\/src\/branch\/([a-zA-Z0-9-_.#/]*)/g;

function unindent(str) {
  str = str.replace(/\t/g, "    ");
  const minIndent =
    str
      .match(/^ *(?=\S)/gm)
      ?.reduce((prev, curr) => Math.min(prev, curr.length), Infinity) ?? 0;
  if (!minIndent) return str;
  return str.replace(new RegExp(`^ {${minIndent}}`, "gm"), "");
}

async function processFile(link) {
  const file = await fetch(link).then((res) => res.text());
  const lines = file.replace(/\r/g, "").split("\n");

  const fileName = link.substring(
    link.lastIndexOf("/") + 1,
    link.indexOf("#") == -1 ? link.length : link.indexOf("#")
  );
  const fileType =
    fileName.indexOf(".") == -1
      ? ""
      : fileName.substring(fileName.indexOf(".") + 1);

  const lineStr = link.match(/#L\d+(-L?\d+)?/)?.[0];
  let startLine, endLine;
  let entireFile = false;

  if (lineStr) {
    const [start, end] = lineStr.match(/\d+/g);
    if (!end) {
      startLine = endLine = start;
    } else {
      startLine = start;
      endLine = end;
    }
  } else {
    entireFile = true;
    startLine = 1;
    endLine = lines.length;
  }

  const whichLines = entireFile
    ? ""
    : startLine == endLine
    ? "Line " + startLine
    : "Lines " + startLine + "-" + endLine;

  startLine--;
  endLine--;

  const targetLines = (
    entireFile ? lines : lines.slice(startLine, endLine + 1)
  ).join("\n");

  return (
    "**" +
    fileName +
    ": **" +
    whichLines +
    "\n```" +
    fileType +
    "\n" +
    unindent(targetLines) +
    "\n```\n"
  );
}

events.add("messageCreate", "codePreviews", async function (msg) {
  if (msg.author.id == hf.bot.user.id) return;
  if (!msg.guildID) return;
  if (!(await hasFlag(msg.guildID, "codePreviews"))) return;

  let out = "";

  const githubLinks = msg.content.match(REGEX_GITHUB);
  const gitlabLinks = msg.content.match(REGEX_GITLAB);
  const giteaLinks = msg.content.match(REGEX_GITEA);

  if (githubLinks?.length) {
    for (const link of githubLinks) {
      out += await processFile(link.replace("/blob/", "/raw/"));
    }
  }

  if (gitlabLinks?.length) {
    for (const link of gitlabLinks) {
      out += await processFile(link.replace("/blob/", "/raw/"));
    }
  }

  if (giteaLinks?.length) {
    for (const link of giteaLinks) {
      out += await processFile(link.replace("/src/", "/raw/"));
    }
  }

  if (out.length > 2000) return;

  if (out !== "") {
    // NB: OceanicJS/Oceanic#32
    //await msg.edit({flags: MessageFlags.SUPPRESS_EMBEDS}).catch(() => {});
    await hf.bot.rest
      .authRequest({
        method: "PATCH",
        path: Routes.CHANNEL_MESSAGE(msg.channel.id, msg.id),
        json: {
          flags: MessageFlags.SUPPRESS_EMBEDS,
        },
      })
      .catch(() => {});

    await msg.channel.createMessage({
      content: out,
      allowedMentions: {
        repliedUser: false,
      },
      messageReference: {
        messageID: msg.id,
      },
    });
  }
});
