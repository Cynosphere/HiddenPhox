const Command = require("#lib/command.js");
const CATEGORY = "moderation";

const {formatUsername} = require("#util/misc.js");
const {lookupUser} = require("#util/selection.js");

const tidy = new Command("tidy");
tidy.addAlias("prune");
tidy.addAlias("purge");
tidy.category = CATEGORY;
tidy.helpText = "Clean up messages";
tidy.usage = "[subcommand] <count> <extra>";
tidy.callback = async function (msg, line, [subcommand, count, extra]) {
  const showHelp = !subcommand || subcommand === "" || subcommand === "help";
  if (!showHelp && !msg.guildID) {
    return "Can only be used in guilds.";
  }
  if (!showHelp && !msg.channel.permissionsOf(msg.author.id).has("manageMessages")) {
    return "You do not have `Manage Messages` permission.";
  }
  if (!showHelp && !msg.channel.permissionsOf(hf.bot.user.id).has("manageMessages")) {
    return "I do not have `Manage Messages` permission.";
  }
  if (msg.author.bot) return "Zero-width space your say command.";

  switch (subcommand) {
    case "all": {
      const messages = await msg.channel
        .getMessages({
          before: msg.id,
          limit: count && parseInt(count) > 0 ? parseInt(count) : 10,
        })
        .then((msgs) => msgs.map((m) => m.id));
      await msg.channel.deleteMessages(messages, `Message purge by ${formatUsername(msg.author)}`);

      return `Deleted ${messages.length} message(s).`;
    }
    case "user": {
      const user = await lookupUser(count);
      if (typeof user === "string") {
        return user;
      } else {
        const messages = await msg.channel
          .getMessages({
            before: msg.id,
            limit: extra && parseInt(extra) > 0 ? parseInt(extra) : 10,
          })
          .then((msgs) => msgs.filter((m) => m.author.id == user.id).map((m) => m.id));
        await msg.channel.deleteMessages(
          messages,
          `Message purge by ${formatUsername(msg.author)} targeting ${formatUsername(user)}`
        );

        return `Deleted ${messages.length} message(s).`;
      }
    }
    case "bots": {
      const messages = await msg.channel
        .getMessages({
          before: msg.id,
          limit: count && parseInt(count) > 0 ? parseInt(count) : 50,
        })
        .then((msgs) => msgs.filter((m) => msg.author.bot).map((m) => m.id));
      await msg.channel.deleteMessages(messages, `Message purge by ${formatUsername(msg.author)} targeting bots`);

      return `Deleted ${messages.length} message(s).`;
    }
    case "filter": {
      if (count.length === 0) return "Filter must be at least 1 character long.";

      const messages = await msg.channel
        .getMessages({
          before: msg.id,
          limit: extra && parseInt(extra) > 0 ? parseInt(extra) : 10,
        })
        .then((msgs) => msgs.filter((m) => m.content.indexOf(count) > -1).map((m) => m.id));
      await msg.channel.deleteMessages(messages, `Message purge by ${formatUsername(msg.author)} targeting "${count}"`);

      return `Deleted ${messages.length} message(s).`;
    }
    case "help":
    default:
      return `__Usage__
• \`all <count = 10>\` - Last \`count\` messages.
• \`user [user] <count = 10>\` - Last \`count\` messages from \`user\`.
• \`bots <count = 50>\` - Last \`count\` messages from bots.
• \`filter [string] <count = 10>\` - Last \`count\` messages whose content matches \`string\`.`;
  }
};
hf.registerCommand(tidy);
