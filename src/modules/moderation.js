const Command = require("../lib/command.js");
const CATEGORY = "moderation";

const {lookupUser} = require("../lib/utils.js");

const tidy = new Command("tidy");
tidy.addAlias("prune");
tidy.addAlias("purge");
tidy.category = CATEGORY;
tidy.helpText = "Clean up messages";
tidy.usage = "[subcommand] <count> <extra>";
tidy.callback = async function (msg, line, [subcommand, count, extra]) {
  if (!msg.channel.permissionsOf(msg.author.id).has("MANAGE_MESSAGES")) {
    return "You do not have `Manage Messages` permission.";
  }
  if (!msg.channel.permissionsOf(hf.bot.user.id).has("MANAGE_MESSAGES")) {
    return "I do not have `Manage Messages` permission.";
  }

  switch (subcommand) {
    case "all": {
      const messages = await msg.channel.getMessages({
        after: msg.id,
        limit: count && parseInt(count) > 0 ? parseInt(count) : 10,
      });
      await msg.channel.deleteMessages(
        messages.map((m) => m.id),
        `Message purge by ${msg.author.tag}`
      );

      return `Deleted ${messages.length} messages.`;
    }
    case "user": {
      const user = await lookupUser(count);
      if (typeof user === "string") {
        return user;
      } else {
        const messages = await msg.channel.getMessages({
          after: msg.id,
          limit: extra && parseInt(extra) > 0 ? parseInt(extra) : 10,
        });
        await msg.channel.deleteMessages(
          messages.filter((m) => m.author.id == user.id).map((m) => m.id),
          `Message purge by ${msg.author.tag} targeting ${user.tag}`
        );

        return `Deleted ${messages.length} messages.`;
      }
    }
    case "bots": {
      const messages = await msg.channel.getMessages({
        after: msg.id,
        limit: count && parseInt(count) > 0 ? parseInt(count) : 50,
      });
      await msg.channel.deleteMessages(
        messages.filter((m) => msg.author.bot).map((m) => m.id),
        `Message purge by ${msg.author.tag} targeting bots`
      );

      return `Deleted ${messages.length} messages.`;
    }
    case "filter": {
      if (count.length === 0)
        return "Filter must be at least 1 character long.";

      const messages = await msg.channel.getMessages({
        after: msg.id,
        limit: extra && parseInt(extra) > 0 ? parseInt(extra) : 10,
      });
      await msg.channel.deleteMessages(
        messages.filter((m) => m.content.indexOf(count) > -1).map((m) => m.id),
        `Message purge by ${msg.author.tag} targeting "${count}"`
      );

      return `Deleted ${messages.length} messages.`;
    }
    default:
      return `__Usage__
• \`all <count = 10>\` - Last \`count\` messages.
• \`user [user] <count = 10>\` - Last \`count\` messages from \`user\`.
• \`bots <count = 50>\` - Last \`count\` messages from bots.
• \`filter [string] <count = 10>\` - Last \`count\` messages whose content matches \`string\`.`;
  }
};
hf.registerCommand(tidy);
