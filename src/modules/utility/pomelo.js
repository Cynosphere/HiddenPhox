const Command = require("#lib/command.js");

const {APIEndpoints} = require("#util/dconstants.js");
const {
  RegExp: {Pomelo: POMELO_REGEX},
} = require("#util/constants.js");

const pomelo = new Command("pomelo");
pomelo.category = "utility";
pomelo.helpText = "Check to see if a username is taken or not";
pomelo.usage = "[username] <...username>";
pomelo.callback = async function (msg, line) {
  if (!line || line === "") return "Arguments required.";

  const usernames = line.toLowerCase().split(" ");

  if (usernames.length == 1) {
    const name = usernames[0];
    if (name.length > 32 || !POMELO_REGEX.test(name)) return {reaction: "\u{1f6ab}"};

    const res = await hf.bot.requestHandler.request("POST", APIEndpoints.POMELO_UNAUTHED, false, {
      username: name,
    });
    return {reaction: res.taken ? "\u274c" : "\u2705"};
  } else {
    const lines = [];

    for (const name of usernames) {
      if (name.length > 32 || !POMELO_REGEX.test(name)) {
        lines.push(`\u{1f6ab} \`${name}\``);
      } else {
        try {
          const res = await hf.bot.requestHandler.request("POST", APIEndpoints.POMELO_UNAUTHED, false, {
            username: name,
          });
          lines.push(`${res.taken ? "\u274c" : "\u2705"} \`${name}\``);
        } catch {
          lines.push(`\u26a0\ufe0f \`${name}\``);
        }
      }
    }

    return lines.join("\n");
  }
};
hf.registerCommand(pomelo);
