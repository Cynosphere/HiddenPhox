const Command = require("#lib/command.js");

const {snowflakeToTimestamp} = require("#util/time.js");

const snowflake = new Command("snowflake");
snowflake.category = "utility";
snowflake.helpText = "Converts a snowflake ID into readable time.";
snowflake.usage = "<--twitter> [snowflake]";
snowflake.callback = function (msg, line, [snowflake], {twitter}) {
  const num = Number(snowflake);
  if (!isNaN(num)) {
    const timestamp = Math.floor(snowflakeToTimestamp(num, twitter) / 1000);
    return `The timestamp for \`${snowflake}\` is <t:${timestamp}:F> (<t:${timestamp}:R>)`;
  } else {
    return "Argument provided is not a number.";
  }
};
hf.registerCommand(snowflake);
