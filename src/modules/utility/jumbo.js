const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const sharp = require("sharp");

const {
  RegExp: {Emote: CUSTOM_EMOTE_REGEX},
  EmojiSets,
  EmojiNames,
} = require("#util/constants.js");
const {ApplicationCommandOptionTypes, CDNEndpoints} = require("#util/dconstants.js");
const {getNamesFromString} = require("#util/unicode.js");

const jumbo = new Command("jumbo");
jumbo.category = "utility";
jumbo.helpText = "Gets the raw image of an emoji.";
jumbo.usage = "<emoji>";
jumbo.addAlias("e");
jumbo.addAlias("emote");
jumbo.addAlias("emoji");
jumbo.callback = async function (msg, line) {
  if (!line || line === "") return "Arguments required.";

  if (CUSTOM_EMOTE_REGEX.test(line)) {
    const [, animatedFlag, name, id] = line.match(CUSTOM_EMOTE_REGEX);
    const animated = animatedFlag === "a";

    const url = CDNEndpoints.EMOJI(id, animated);

    return {
      embeds: [
        {
          title: `:${name}: - \`${id}\``,
          url,
          image: {
            url,
          },
        },
      ],
    };
  } else {
    let setName = "twemoji";
    for (const set in EmojiSets) {
      if (line.startsWith(`--${set} `)) {
        setName = set;
        line = line.replace(`--${set} `, "");
      }
    }

    const set = EmojiSets[setName];

    const emoji = Array.from(line)
      .map((char) => char.codePointAt().toString(16))
      .join(set.sep);
    const url = set.prefix + emoji + set.suffix;

    const name = EmojiNames[line]
      ? `\\:${EmojiNames[line]}\\:`
      : await getNamesFromString(line).then((name) => name.map((x) => x[1]).join(", "));

    const emojiFound = await fetch(url, {method: "HEAD"}).then((res) => res.ok);

    if (!emojiFound) {
      return "Emoji not found. The emoji set chosen might not have this emote as an image.";
    }

    if (set.suffix == ".svg") {
      const svg = await fetch(url)
        .then((res) => res.arrayBuffer())
        .then((b) => Buffer.from(b));
      const converted = await sharp(svg, {density: 2400}).resize(1024).toBuffer();

      return {
        embeds: [
          {
            title: `${name} (${emoji.toUpperCase().replace(/[-_]/g, ", ")})`,
            url,
            image: {
              url: "attachment://emoji.png",
            },
          },
        ],
        file: {
          file: converted,
          name: "emoji.png",
        },
      };
    } else {
      return {
        embeds: [
          {
            title: `${name} (${emoji.toUpperCase().replace(/[-_]/g, ", ")})`,
            url,
            image: {
              url,
            },
          },
        ],
      };
    }
  }
};
hf.registerCommand(jumbo);

const jumboInteraction = new InteractionCommand("jumbo");
jumboInteraction.helpText = "Gets the raw image of an emoji.";
jumboInteraction.options.content = {
  name: "content",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Emoji to get image of",
  required: true,
  default: "",
};
jumboInteraction.options.set = {
  name: "set",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Emoji set for non-custom emoji",
  required: false,
  choices: [
    {
      name: "Twemoji (Twitter/Discord)",
      value: "twemoji",
    },
    {
      name: "Noto (Google)",
      value: "noto",
    },
    {
      name: "Noto Old (Blobs)",
      value: "blobs",
    },
    {
      name: "Mutant Standard",
      value: "mustd",
    },
  ],
  default: "twemoji",
};
jumboInteraction.callback = async function (interaction) {
  const content = this.getOption(interaction, "content");
  const set = this.getOption(interaction, "set");

  return jumbo.callback(interaction, `--${set} ${content}`);
};
hf.registerCommand(jumboInteraction);
