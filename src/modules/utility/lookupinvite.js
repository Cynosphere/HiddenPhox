const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {
  APIEndpoints,
  ApplicationCommandOptionTypes,
  CDNEndpoints,
  DEFAULT_GROUP_DM_AVATARS,
} = require("#util/dconstants.js");
const {Icons} = require("#util/constants.js");

const {formatUsername, safeString, formatGuildFeatures} = require("#util/misc.js");
const {snowflakeToTimestamp} = require("#util/time.js");

const lookupinvite = new Command("lookupinvite");
lookupinvite.category = "utility";
lookupinvite.helpText = "Lookup an invite";
lookupinvite.usage = "<invite code>";
lookupinvite.addAlias("linvite");
lookupinvite.addAlias("inviteinfo");
lookupinvite.addAlias("iinfo");
lookupinvite.addAlias("ii");
lookupinvite.callback = async function (msg, line) {
  if (!line || line == "") return "Arguments required.";

  line = line.replace(/(https?:\/\/)?discord(\.gg|(app)?.com\/invite)\//, "");

  if (decodeURIComponent(line).indexOf("../") > -1) return "nuh uh";

  let bail = false;
  let error;
  let invite;
  try {
    invite = await hf.bot.requestHandler.request(
      "GET",
      `${APIEndpoints.INVITE(line)}?with_counts=true&with_expiration=true`
    );
  } catch (err) {
    bail = true;
    error = err;
  }
  if (bail && error) {
    if (error.message.includes("Unknown Invite")) {
      return "Invite provided is not valid.";
    } else {
      return `:warning: Got error \`${safeString(error)}\``;
    }
  }
  if (!invite) return ":warning: No data returned.";

  if (invite.message) {
    if (invite.message == "Unknown Invite") {
      return "Invite provided is not valid.";
    } else {
      return `:warning: Got error \`${invite.code}: "${invite.message}"\``;
    }
  } else {
    const embed = {
      title: `Invite Info: \`${invite.code}\``,
      description: invite.description,
      fields: [],
    };

    const expires = {
      name: "Expires",
      value: invite.expires_at == null ? "Never" : `<t:${Math.floor(new Date(invite.expires_at).getTime() / 1000)}>`,
      inline: true,
    };
    const inviter = invite.inviter
      ? {
          name: "Inviter",
          value: `${
            invite.inviter.global_name
              ? `**${invite.inviter.global_name}** (${formatUsername(invite.inviter)})`
              : `**${formatUsername(invite.inviter)}**`
          } \`(${invite.inviter.id})\``,
          inline: false,
        }
      : null;

    switch (invite.type) {
      case 0: {
        embed.fields.push(
          ...[
            {
              name: "Guild",
              value: `**${invite.guild.name}** \`(${invite.guild.id})\``,
              inline: true,
            },
            {
              name: "Channel",
              value: `**${invite.channel.name}** \`(${invite.channel.id})\``,
              inline: true,
            },
            {
              name: "Boosts",
              value: invite.guild.premium_subscription_count ?? 0,
              inline: true,
            },
            expires,
            {
              name: "Member Count",
              value: `${Icons.online}${invite.approximate_presence_count} online\u3000\u3000${Icons.offline}${invite.approximate_member_count} members`,
              inline: false,
            },
            inviter,
            invite.guild.welcome_screen && {
              name: "Welcome Screen",
              value: `"${invite.guild.welcome_screen.description}"\n${invite.guild.welcome_screen.welcome_channels
                .map(
                  (c) =>
                    `${
                      c.emoji_id ? `[:${c.emoji_name}:](${CDNEndpoints.EMOJI(c.emoji_id, false, true)})` : c.emoji_name
                    } ${c.description} \`(${c.channel_id})\``
                )
                .join("\n")}`,
              inline: false,
            },
          ].filter((x) => !!x)
        );

        if (invite.guild.features) {
          const features = formatGuildFeatures(invite.guild.features);
          embed.fields.push({
            name: `Features (${features.length})`,
            value: features.length > 0 ? features.slice(0, Math.ceil(features.length / 2)).join("\n") : "None",
            inline: true,
          });
          if (features.length > 1) {
            embed.fields.push({
              name: "\u200b",
              value: features.slice(Math.ceil(features.length / 2), features.length).join("\n"),
              inline: true,
            });
          }
        }

        const guildIcon = invite.guild?.icon && CDNEndpoints.GUILD_ICON(invite.guild.id, invite.guild.icon);
        const guildSplash = invite.guild?.splash && CDNEndpoints.GUILD_SPLASH(invite.guild.id, invite.guild.splash);

        if (guildIcon)
          embed.thumbnail = {
            url: guildIcon,
          };

        if (invite.guild && (invite.guild.icon || invite.guild.splash || invite.guild.banner)) {
          const imageLinks = [];
          if (guildIcon) imageLinks.push(`[Icon](${guildIcon})`);
          if (invite.guild.splash) imageLinks.push(`[Splash](${guildSplash})`);
          if (invite.guild.banner)
            imageLinks.push(`[Banner](${CDNEndpoints.BANNER(invite.guild.id, invite.guild.banner)})`);

          embed.fields.push({
            name: "\u200b",
            value: imageLinks.join("\u3000\u3000"),
            inline: false,
          });
        }

        if (guildSplash)
          embed.image = {
            url: guildSplash,
          };

        break;
      }
      case 1: {
        embed.title += " (Group DM)";
        embed.fields.push(
          ...[
            {
              name: "Channel",
              value: `**${
                invite.channel.name ?? invite.channel.recipients.map((member) => member.username).join(", ")
              }** (${invite.channel.id})`,
              inline: false,
            },
            {
              name: "Member Count",
              value: `${Icons.offline}${invite.approximate_member_count} members`,
              inline: true,
            },
            expires,
            invite.channel.name != null && {
              name: "Recipients",
              value: invite.channel.recipients.map((member) => member.username).join("\n"),
              inline: false,
            },
            inviter,
          ].filter((x) => !!x)
        );

        const groupIcon = invite.channel.icon
          ? CDNEndpoints.GDM_ICON(invite.channel.id, invite.channel.icon)
          : DEFAULT_GROUP_DM_AVATARS[snowflakeToTimestamp(invite.channel.id) % DEFAULT_GROUP_DM_AVATARS.length];

        embed.thumbnail = {
          url: groupIcon,
        };
        embed.fields.push({
          name: "\u200b",
          value: `[Icon](${groupIcon})`,
          inline: false,
        });

        break;
      }
      case 2: {
        embed.title += " (Friend)";
        embed.fields.push(expires, inviter);

        const avatarURL = invite.inviter?.avatar && CDNEndpoints.USER_AVATAR(invite.inviter.id, invite.inviter.avatar);

        if (avatarURL) {
          embed.thumbnail = {
            url: avatarURL,
          };
          embed.fields.push({
            name: "\u200b",
            value: `[Avatar](${avatarURL})`,
            inline: false,
          });
        }

        break;
      }
      default:
        return `Unhandled invite type: \`${invite.type}\``;
    }

    return {embed};
  }
};
hf.registerCommand(lookupinvite);

const inviteinfoInteraction = new InteractionCommand("inviteinfo");
inviteinfoInteraction.helpText = "Get information on an invite code";
inviteinfoInteraction.options.invite = {
  name: "invite",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Invite code to get info for",
  required: true,
  default: "",
};
inviteinfoInteraction.callback = async function (interaction) {
  const invite = this.getOption(interaction, "invite");

  return lookupinvite.callback(interaction, invite);
};
hf.registerCommand(inviteinfoInteraction);
