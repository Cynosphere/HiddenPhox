const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {VoiceChannel} = require("@projectdysnomia/dysnomia");

const {
  APIEndpoints,
  ApplicationCommandOptionTypes,
  CDNEndpoints,
  ClanPlaystyle,
  ExplicitContentFilterStrings,
  Games,
  Permissions,
  VerificationLevelStrings,
} = require("#util/dconstants.js");
const {
  RegExp: {Snowflake: SNOWFLAKE_REGEX},
  Icons,
  ChannelTypeNames,
} = require("#util/constants.js");
const {snowflakeToTimestamp} = require("#util/time.js");
const {getGuild, tryGetGuild, formatGuildFeatures} = require("#util/misc.js");

const guildinfo = new Command("guildinfo");
guildinfo.category = "utility";
guildinfo.helpText = "Get information on a guild";
guildinfo.usage = "<guild id>";
guildinfo.addAlias("guild");
guildinfo.addAlias("ginfo");
guildinfo.addAlias("gi");
guildinfo.addAlias("serverinfo");
guildinfo.addAlias("server");
guildinfo.addAlias("sinfo");
guildinfo.addAlias("si");
guildinfo.callback = async function (msg, line, args, {nolocal, debug}) {
  let _guild, clanEmbed, id;
  if (!line || line == "") {
    if (!msg.guildID) return "Not in a guild.";
    const __guild = msg.channel.guild ?? hf.bot.guilds.get(msg.guildID);
    if (__guild) {
      _guild = {source: "local", data: __guild};
    } else {
      _guild = await getGuild(msg.guildID);
    }
    id = msg.guildID;
  } else if (debug) {
    if (!SNOWFLAKE_REGEX.test(line)) return "Not a snowflake.";
    id = line.match(SNOWFLAKE_REGEX)[1];

    try {
      await hf.bot.requestHandler.request("GET", APIEndpoints.GUILD_WIDGET(id), false);
    } catch (err) {
      if (err.code != 50004) {
        return "Guild not found.";
      }
    }

    const sources = await tryGetGuild(id);

    return `\`\`\`js
"local":        ${sources.local}
"preview":      ${sources.preview}${sources.local && sources.preview ? " // false positive, local = true" : ""}
"widget":       ${sources.widget}
"discovery":    ${sources.discovery}
"verification": ${sources.verification}
"clan":         ${sources.clan}
\`\`\``;
  } else {
    if (!SNOWFLAKE_REGEX.test(line)) return "Not a snowflake.";
    const snowflake = line.match(SNOWFLAKE_REGEX)[1];
    _guild = await getGuild(snowflake, nolocal);
    id = snowflake;

    try {
      const clan = await hf.bot.requestHandler.request("GET", APIEndpoints.CLAN(snowflake), true);

      if (clan) {
        const images = [];

        const games = await Promise.all(
          clan.game_ids
            .sort((a, b) => (clan.game_activity[b]?.activity_score ?? 0) - (clan.game_activity[a]?.activity_score ?? 0))
            .map(async (id) => {
              let game = Games.find((x) => x.id == id);
              if (!game) {
                game = await hf.bot.requestHandler.request("GET", APIEndpoints.APPLICATION_RPC(id), false);
              }

              let out = `${game.name} (\`${id}\`)`;

              if (clan.game_activity[id]?.activity_level > 1) {
                out = `:fire: ${out}`;
              } else {
                out = `${Icons.blank} ${out}`;
              }

              return out;
            })
        );

        clan.wildcard_descriptors = clan.wildcard_descriptors.filter((x) => x != "");

        const termLines = [];
        let currentTerm = "";
        for (const index in clan.search_terms) {
          const term = clan.search_terms[index];
          const formattedTerm = `\`\u2004${term.replaceAll(" ", "\u2005")}\u2004\``;
          if (currentTerm.length + 1 + formattedTerm.length > 56) {
            termLines.push(currentTerm);
            currentTerm = formattedTerm;
          } else {
            currentTerm += "\u2004" + formattedTerm;
          }
          if (index == clan.search_terms.length - 1) termLines.push(currentTerm);
        }

        let gameLines = "";
        let gameLines2 = "";
        for (const line of games) {
          if (gameLines.length + line.length + 1 <= 1024) {
            gameLines += line + "\n";
          } else {
            gameLines2 += line + "\n";
          }
        }

        clanEmbed = {
          color: parseInt(clan.brand_color_primary.replace("#", "0x")),
          title: _guild == null ? clan.name : null,
          author: {
            name: clan.tag,
          },
          description: `-# :video_game:${ClanPlaystyle[clan.playstyle] ?? "Unknown"}${
            clan.wildcard_descriptors.length > 0 ? ` \u2022 **${clan.wildcard_descriptors.join(", ")}**` : ""
          }\n\n${clan.description ?? "*No description*"}`,
          fields: [
            !_guild && {
              name: "Member Count",
              value: clan.member_count,
              inline: true,
            },
            clan.search_terms.length > 0 && {
              name: "Interests/Topics/Traits",
              value: termLines.join("\n"),
              inline: false,
            },
            games.length > 0 && {
              name: "Associated Games",
              value: gameLines,
              inline: false,
            },
            gameLines2 != "" && {
              name: "\u200b",
              value: gameLines2,
              inline: false,
            },
            {
              name: "Badge Colors",
              value: `${clan.badge_color_primary}, ${clan.badge_color_secondary}`,
              inline: true,
            },
            {
              name: "Banner/Brand Colors",
              value: `${clan.brand_color_primary}, ${clan.brand_color_secondary}`,
              inline: true,
            },
          ].filter((x) => !!x),
          footer: !_guild ? {text: "Fetched from clan"} : null,
        };

        if (clan.badge_hash) {
          const url = CDNEndpoints.CLAN_BADGE(clan.id, clan.badge_hash);
          images.push(`[Badge](${url})`);
          clanEmbed.author.icon_url = url;
        }
        if (clan.banner_hash) {
          const url = CDNEndpoints.CLAN_BANNER(clan.id, clan.banner_hash);
          images.push(`[Banner](${url})`);
          clanEmbed.image = {url};
        }

        if (images.length > 0) {
          clanEmbed.fields.push({
            name: "\u200b",
            value: images.join("\u3000\u3000"),
            inline: false,
          });
        }
      }
    } catch {
      // noop
    }
  }

  if (!_guild) {
    if (clanEmbed) {
      return {embeds: [clanEmbed]};
    } else {
      try {
        await hf.bot.requestHandler.request("GET", APIEndpoints.GUILD_WIDGET(id), false);
      } catch (err) {
        if (err.code == 50004) {
          return `Guild \`${id}\` is private.`;
        } else {
          return "Guild not found.";
        }
      }
    }
  }

  const guild = _guild.data;
  switch (_guild.source) {
    case "local": {
      const roles = Array.from(guild.roles.values());

      const channelTypeCounts = {};
      let nsfwChannels = 0;
      let hiddenChannels = 0;
      for (const channel of guild.channels.values()) {
        if (!channelTypeCounts[channel.type]) channelTypeCounts[channel.type] = 0;
        channelTypeCounts[channel.type]++;

        if (channel.nsfw) nsfwChannels++;

        let defaultPermissions = channel.permissionOverwrites.get(guild.id);
        if (!defaultPermissions && channel.parentID) {
          defaultPermissions = guild.channels.get(channel.parentID).permissionOverwrites.get(guild.id);
        }
        if (
          defaultPermissions &&
          (channel instanceof VoiceChannel
            ? (defaultPermissions.deny & Permissions.voiceConnect) === Permissions.voiceConnect
            : (defaultPermissions.deny & Permissions.viewChannel) === Permissions.viewChannel)
        ) {
          hiddenChannels++;
        }
      }

      const embed = {
        title: guild.name,
        description: guild.description ?? "*No description.*",
        fields: [
          guild.ownerID && {
            name: "Owner",
            value: `<@${guild.ownerID}>`,
            inline: true,
          },
          guild.vanityURL && {
            name: "Vanity URL",
            value: `https://discord.gg/${guild.vanityURL}`,
            inline: true,
          },
          {
            name: "Created",
            value: `<t:${Math.floor(snowflakeToTimestamp(guild.id) / 1000)}:R>`,
            inline: true,
          },
          {
            name: "Max Members",
            value: guild.maxMembers,
            inline: true,
          },
          {
            name: "Max Video Channel Users",
            value: `Normal: ${guild.maxVideoChannelUsers}\nStage: ${guild.maxStageVideoChannelUsers}`,
            inline: true,
          },
          {
            name: "Verification Level",
            value: VerificationLevelStrings[guild.verificationLevel],
            inline: true,
          },
          {
            name: "Content Filter",
            value: ExplicitContentFilterStrings[guild.explicitContentFilter],
            inline: true,
          },
          {
            name: "Moderation 2FA",
            value: guild.mfaLevel == 0 ? "Off" : "On",
            inline: true,
          },
          {
            name: "Boost Status",
            value: `**Level ${guild.premiumTier}**, ${guild.premiumSubscriptionCount} Boosts`,
            inline: true,
          },
          {
            name: "Locale",
            value: guild.preferredLocale,
            inline: true,
          },
          {
            name: "Default Notifications",
            value: guild.defaultNotifications == 0 ? "All Messages" : "Only Mentions",
            inline: true,
          },
          guild.rulesChannelID && {
            name: "Rules",
            value: `<#${guild.rulesChannelID}>`,
            inline: true,
          },
          guild.systemChannelID && {
            name: "System Messages",
            value: `<#${guild.systemChannelID}>`,
            inline: true,
          },
          guild.publicUpdatesChannelID && {
            name: "Community Updates",
            value: `<#${guild.publicUpdatesChannelID}>`,
            inline: true,
          },
          guild.safetyAlertsChannelID && {
            name: "Safety Alerts",
            value: `<#${guild.safetyAlertsChannelID}>`,
            inline: true,
          },
          {
            name: `Channels (${guild.channels.size})`,
            value:
              Object.entries(channelTypeCounts)
                .map(([type, count]) => `${count} ${ChannelTypeNames[type]}`)
                .join(", ") + `\n${nsfwChannels} age restricted, ${hiddenChannels} hidden`,
            inline: false,
          },
          {
            name: `Roles (${guild.roles.size})`,
            value: `${roles.filter((role) => role.managed).length} managed, ${
              roles.filter((role) => role.tags?.guild_connections).length
            } linked, ${roles.filter((role) => role.tags?.integration_id != null).length} integration`,
            inline: true,
          },
          guild.emojis.length > 0 && {
            name: `Emotes (${guild.emojis.length})`,
            value: `${guild.emojis.filter((e) => e.animated).length} animated, ${
              guild.emojis.filter((e) => e.managed).length
            } managed\n${guild.emojis.filter((e) => !e.available).length} unavailable`,
            inline: true,
          },
          guild.stickers.length > 0 && {
            name: `Stickers (${guild.stickers.length})`,
            value: `${guild.stickers.filter((s) => s.format_type == 1).length} PNG, ${
              guild.stickers.filter((s) => s.format_type == 2).length
            } APNG, ${guild.stickers.filter((s) => s.format_type == 4).length} GIF, ${
              guild.stickers.filter((s) => s.format_type == 3).length
            } Lottie\n${guild.stickers.filter((s) => !s.available).length} unavailable`,
            inline: true,
          },
        ].filter((x) => !!x),
      };

      if (guild.icon) {
        embed.thumbnail = {
          url: CDNEndpoints.GUILD_ICON(guild.id, guild.icon),
        };
      }

      const members = Array.from(guild.members.values());
      const online = members.filter((member) => member.status != null && member.status != "offline").length;
      const bots = members.filter((member) => member.bot).length;
      const verfifiedBots = members.filter((member) => member.bot && (member.user.publicFlags & 65536) != 0).length;
      embed.fields.push({
        name: "Member Count",
        value: `${Icons.online}${online} online\u3000\u3000${Icons.offline}${guild.memberCount} members\n${Icons.silk.cog} ${bots}\u3000\u3000${Icons.silk.tick} ${verfifiedBots}`,
        inline: false,
      });

      const features = formatGuildFeatures(guild.features);

      embed.fields.push({
        name: `Features (${features.length})`,
        value: features.length > 0 ? features.slice(0, Math.ceil(features.length / 2)).join("\n") : "None",
        inline: true,
      });
      if (features.length > 1)
        embed.fields.push({
          name: "\u200b",
          value: features.slice(Math.ceil(features.length / 2), features.length).join("\n"),
          inline: true,
        });

      const images = [];
      if (guild.icon) {
        images.push(`[Icon](${embed.thumbnail.url})`);
      }
      if (guild.banner) {
        images.push(`[Banner](${CDNEndpoints.BANNER(guild.id, guild.banner)})`);
      }
      if (guild.splash) {
        images.push(`[Invite Splash](${CDNEndpoints.GUILD_SPLASH(guild.id, guild.splash)})`);
      }
      if (guild.discoverySplash) {
        images.push(`[Discovery Splash](${CDNEndpoints.DISCOVERY_SPLASH(guild.id, guild.discoverySplash)})`);
      }

      if (images.length > 0) {
        embed.fields.push({
          name: "\u200b",
          value: images.join("\u3000\u3000"),
          inline: false,
        });
      }

      if (clanEmbed) {
        return {embeds: [embed, clanEmbed]};
      } else {
        return {embed};
      }
    }
    case "preview":
    case "verification": {
      const embed = {
        title: guild.name,
        description: guild.description ?? "*No description.*",
        fields: [
          {
            name: "Created",
            value: `<t:${Math.floor(snowflakeToTimestamp(guild.id) / 1000)}:R>`,
            inline: true,
          },
          guild.verification_level && {
            name: "Verification Level",
            value: VerificationLevelStrings[guild.verification_level],
            inline: true,
          },
          (guild.emojis?.length ?? 0) > 0 && {
            name: `Emotes (${guild.emojis.length})`,
            value: `${guild.emojis.filter((e) => e.animated).length} animated, ${
              guild.emojis.filter((e) => e.managed).length
            } managed\n${guild.emojis.filter((e) => !e.available).length} unavailable`,
            inline: true,
          },
          (guild.stickers?.length ?? 0) > 0 && {
            name: `Stickers (${guild.stickers.length})`,
            value: `${guild.stickers.filter((s) => s.format_type == 1).length} PNG, ${
              guild.stickers.filter((s) => s.format_type == 2).length
            } APNG, ${guild.stickers.filter((s) => s.format_type == 4).length} GIF, ${
              guild.stickers.filter((s) => s.format_type == 3).length
            } Lottie\n${guild.stickers.filter((s) => !s.available).length} unavailable`,
            inline: true,
          },
        ].filter((x) => !!x),
        footer: {
          text: `Fetched from ${_guild.source === "verification" ? "membership screening" : "guild preview"}`,
        },
      };

      if (guild.icon) {
        embed.thumbnail = {
          url: CDNEndpoints.GUILD_ICON(guild.id, guild.icon),
        };
      }

      embed.fields.push({
        name: "Member Count",
        value: `${Icons.online}${guild.approximate_presence_count} online\u3000\u3000${Icons.offline}${guild.approximate_member_count} members`,
        inline: false,
      });

      const features = formatGuildFeatures(guild.features);

      embed.fields.push({
        name: `Features (${features.length})`,
        value: features.length > 0 ? features.slice(0, Math.ceil(features.length / 2)).join("\n") : "None",
        inline: true,
      });
      if (features.length > 1)
        embed.fields.push({
          name: "\u200b",
          value: features.slice(Math.ceil(features.length / 2), features.length).join("\n"),
          inline: true,
        });

      const images = [];
      if (guild.icon) {
        images.push(`[Icon](${embed.thumbnail.url})`);
      }
      if (guild.splash) {
        images.push(`[Invite Splash](${CDNEndpoints.GUILD_SPLASH(guild.id, guild.splash)})`);
      }
      if (guild.discovery_splash) {
        images.push(`[Discovery Splash](${CDNEndpoints.DISCOVERY_SPLASH(guild.id, guild.discoverySplash)})`);
      }

      if (images.length > 0) {
        embed.fields.push({
          name: "\u200b",
          value: images.join("\u3000\u3000"),
          inline: false,
        });
      }

      if (clanEmbed) {
        return {embeds: [embed, clanEmbed]};
      } else {
        return {embed};
      }
    }
    case "discovery": {
      if (!guild.store_page) {
        return "Got discovery data but no store page.";
      }

      const guildObj = guild.store_page.guild;

      let invite;
      if (guildObj.invite?.code || guild.store_page.role_subscription.purchase_page_invite?.code) {
        const code = guildObj.invite?.code ?? guild.store_page.role_subscription.purchase_page_invite?.code;
        invite = await hf.bot.requestHandler.request(
          "GET",
          `${APIEndpoints.INVITE(code)}?with_counts=true&with_expiration=true`
        );
      }

      const embed = {
        title: guildObj.name,
        description: invite?.guild?.description ?? "*No description.*",
        fields: [
          {
            name: "Created",
            value: `<t:${Math.floor(snowflakeToTimestamp(guildObj.id) / 1000)}:R>`,
            inline: true,
          },
        ],
        footer: {
          text: "Fetched from discovery" + (invite ? " + invite" : ""),
        },
      };

      if (guildObj.icon_hash) {
        embed.thumbnail = {
          url: CDNEndpoints.GUILD_ICON(guildObj.id, guildObj.icon_hash),
        };
      }

      const invites = [];
      if (guildObj.invite?.code) invites.push(guildObj.invite.code);
      if (guild.store_page.role_subscription.purchase_page_invite?.code)
        invites.push(guild.store_page.role_subscription.purchase_page_invite.code);

      if (invites.length > 0) {
        embed.fields.push({
          name: "Invites",
          value: invites.map((code) => "https://discord.gg/" + code).join("\n"),
          inline: true,
        });
      }

      embed.fields.push({
        name: "Member Count",
        value: `${Icons.online}${guildObj.approximate_presence_count} online\t\t${Icons.offline}${guildObj.approximate_member_count} members`,
        inline: false,
      });

      if (invite?.guild?.features) {
        const features = formatGuildFeatures(invite.guild.features);

        embed.fields.push({
          name: `Features (${features.length})`,
          value: features.length > 0 ? features.slice(0, Math.ceil(features.length / 2)).join("\n") : "None",
          inline: true,
        });
        if (features.length > 1)
          embed.fields.push({
            name: "\u200b",
            value: features.slice(Math.ceil(features.length / 2), features.length).join("\n"),
            inline: true,
          });
      }

      const images = [];
      if (guildObj.icon_hash) {
        images.push(`[Icon](${embed.thumbnail.url})`);
      }
      if (invite?.guild?.splash) {
        images.push(`[Invite Splash](${CDNEndpoints.GUILD_SPLASH(guild.id, guild.splash)})`);
      }
      if (invite?.guild?.banner) {
        images.push(`[Banner](${CDNEndpoints.BANNER(guild.id, guild.banner)})`);
      }

      if (images.length > 0) {
        embed.fields.push({
          name: "\u200b",
          value: images.join("\u3000\u3000"),
          inline: false,
        });
      }

      if (clanEmbed) {
        return {embeds: [embed, clanEmbed]};
      } else {
        return {embed};
      }
    }
    case "widget": {
      let invite;
      if (guild.instant_invite) {
        invite = await hf.bot.requestHandler.request(
          "GET",
          `/invites/${guild.instant_invite.replace(
            /(https?:\/\/)?discord(\.gg|(app)?.com\/invite)\//,
            ""
          )}?with_counts=true&with_expiration=true`
        );
      }

      const embed = {
        title: guild.name,
        description: invite?.guild?.description ?? "*No description.*",
        fields: [
          {
            name: "Created",
            value: `<t:${Math.floor(snowflakeToTimestamp(guild.id) / 1000)}:R>`,
            inline: true,
          },
        ],
        footer: {
          text: "Fetched from widget" + (invite ? " + invite" : ""),
        },
      };

      if (invite) {
        embed.fields.push({
          name: "Invite",
          value: guild.instant_invite,
          inline: true,
        });

        embed.fields.push({
          name: "Member Count",
          value: `${Icons.online}${invite.approximate_presence_count} online\t\t${Icons.offline}${invite.approximate_member_count} members`,
          inline: false,
        });

        const features = formatGuildFeatures(invite.guild.features);

        embed.fields.push({
          name: `Features (${features.length})`,
          value: features.length > 0 ? features.slice(0, Math.ceil(features.length / 2)).join("\n") : "None",
          inline: true,
        });
        if (features.length > 1)
          embed.fields.push({
            name: "\u200b",
            value: features.slice(Math.ceil(features.length / 2), features.length).join("\n"),
            inline: true,
          });

        const images = [];
        if (invite.guild.icon) {
          images.push(`[Icon](${CDNEndpoints.GUILD_ICON(invite.guild.id, invite.guild.icon)})`);
        }
        if (invite.guild.splash) {
          images.push(`[Invite Splash](${CDNEndpoints.GUILD_SPLASH(invite.guild.id, invite.guild.splash)})`);
        }
        if (invite.guild.banner) {
          images.push(`[Banner](${CDNEndpoints.BANNER(invite.guild.id, invite.guild.banner)})`);
        }

        if (images.length > 0) {
          embed.fields.push({
            name: "\u200b",
            value: images.join("\u3000\u3000"),
            inline: false,
          });
        }
      } else {
        embed.fields.push({
          name: "Member Count",
          value: `${Icons.online}${guild.presence_count} online`,
          inline: false,
        });
      }

      if (clanEmbed) {
        return {embeds: [embed, clanEmbed]};
      } else {
        return {embed};
      }
    }
    default:
      return "Guild not found.";
  }
};
hf.registerCommand(guildinfo);

const guildinfoInteraction = new InteractionCommand("guildinfo");
guildinfoInteraction.helpText = "Get information on an guild";
guildinfoInteraction.options.id = {
  name: "id",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Guild ID to get info for",
  required: false,
  default: "",
};
guildinfoInteraction.callback = async function (interaction) {
  const id = this.getOption(interaction, "id");

  return guildinfo.callback(interaction, id, [id], {});
};
hf.registerCommand(guildinfoInteraction);
