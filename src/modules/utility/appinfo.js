const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {
  APIEndpoints,
  API_URL,
  ApplicationCommandOptionTypes,
  ApplicationTypes,
  CDNEndpoints,
  Games,
} = require("#util/dconstants.js");
const {
  ApplicationFlagNames,
  Icons,
  RegExp: {Snowflake: SNOWFLAKE_REGEX},
} = require("#util/constants.js");
const {snowflakeToTimestamp} = require("#util/time.js");
const {getGuild, safeString, formatUsername, flagsFromInt} = require("#util/misc.js");

const appinfo = new Command("appinfo");
appinfo.category = "utility";
appinfo.helpText = "Get information on an application";
appinfo.usage = "[application id]";
appinfo.addAlias("ainfo");
appinfo.addAlias("ai");
appinfo.callback = async function (msg, line) {
  if (!line || line === "") return "Arguments required.";
  if (!SNOWFLAKE_REGEX.test(line)) return "Not a snowflake.";

  const snowflake = line.match(SNOWFLAKE_REGEX)[1];

  try {
    const _app = await hf.bot.requestHandler.request("GET", APIEndpoints.APPLICATION_RPC(snowflake), false);

    let app = _app;
    const game = Games.find((game) => game.id == app.id);
    if (game) {
      app = Object.assign(app, game);
    }

    const assets = await hf.bot.requestHandler.request("GET", APIEndpoints.APPLICATION_ASSETS(app.id), false);

    const embed = {
      title: `${app.name}`,
      description: app.description.length > 0 ? app.description : "*No description.*",
      fields: [
        {
          name: "Created",
          value: `<t:${Math.floor(snowflakeToTimestamp(app.id) / 1000)}:R>`,
          inline: true,
        },
      ],
    };

    if (app.icon) {
      embed.thumbnail = {
        url: CDNEndpoints.APP_ICON(app.id, app.icon),
      };
    }

    if (app.type) {
      embed.fields.push({
        name: "Type",
        value: `${ApplicationTypes[app.type] ?? "<unknown type>"} (\`${app.type}\`)`,
        inline: true,
      });
    }

    if (app.guild_id) {
      const guild = await getGuild(app.guild_id);
      if (guild) {
        embed.fields.push({
          name: "Guild",
          value: `${guild.data.name} (\`${app.guild_id}\`)`,
          inline: true,
        });
      } else {
        embed.fields.push({
          name: "Guild ID",
          value: `\`${app.guild_id}\``,
          inline: true,
        });
      }
    }

    if (app.tags) {
      embed.fields.push({
        name: "Tags",
        value: app.tags.join(", "),
        inline: true,
      });
    }

    if (app.publishers || app.developers) {
      embed.fields.push({
        name: "Game Companies",
        value: `**Developers:** ${
          app.developers?.length > 0 ? app.developers.map((x) => x.name).join(", ") : "<unknown>"
        }\n**Publishers:** ${app.publishers?.length > 0 ? app.publishers.map((x) => x.name).join(", ") : "<unknown>"}`,
        inline: true,
      });
    }

    if (app.executables) {
      embed.fields.push({
        name: "Game Executables",
        value: app.executables
          .map((exe) => `${Icons.os[exe.os] ?? "\u2753"} \`${exe.name}\`${exe.is_launcher ? " (launcher)" : ""}`)
          .join("\n"),
        inline: true,
      });
    }

    if (app.third_party_skus) {
      embed.fields.push({
        name: "Game Distributors",
        value: app.third_party_skus
          .map((sku) =>
            sku.distributor == "steam"
              ? `[Steam](https://steamdb.info/app/${sku.id})`
              : sku.distributor == "discord"
              ? `[Discord](https://discord.com/store/skus/${sku.id})`
              : `${sku.distributor
                  .split("_")
                  .map((x) => x[0].toUpperCase() + x.substring(1).toLowerCase())
                  .join(" ")
                  .replace(" Net", ".net")}: \`${sku.id}\``
          )
          .join("\n"),
        inline: true,
      });
    }

    if (app.bot_public != null || app.integration_public != null) {
      if (
        (app.bot_public && !app.bot_require_code_grant) ||
        (app.integration_public && !app.integration_require_code_grant)
      ) {
        let scope = "bot";
        let permissions = "";
        if (app.install_params) {
          if (app.install_params.scopes) {
            scope = app.install_params.scopes.join("+");
          }
          if (app.install_params.permissions) {
            permissions = "&permissions=" + app.install_params.permissions;
          }
        }
        embed.url = `https://discord.com/oauth2/authorize?client_id=${app.id}&scope=${scope}${permissions}`;
      }

      try {
        const bot = await hf.bot.requestHandler.request("GET", APIEndpoints.USER(app.id), true);

        embed.fields.push({
          name: "Bot",
          value: formatUsername(bot) + ((bot.flags & 65536) != 0 ? " \u2713" : ""),
          inline: false,
        });
      } catch {
        embed.fields.push({
          name: "Bot",
          value: "<app id and bot id mismatch or other error>",
          inline: false,
        });
      }
    }

    if (app.custom_install_url) {
      embed.url = app.custom_install_url;
    }

    if (app.flags > 0) {
      const flags = flagsFromInt(app.flags, ApplicationFlagNames, false).split("\n");

      embed.fields.push({
        name: "Flags",
        value: "- " + flags.slice(0, Math.ceil(flags.length / 2)).join("\n- "),
        inline: true,
      });
      if (flags.length > 1)
        embed.fields.push({
          name: "\u200b",
          value: "- " + flags.slice(Math.ceil(flags.length / 2), flags.length).join("\n- "),
          inline: true,
        });
    }

    const images = [];
    if (app.icon) {
      images.push(`[Icon](${embed.thumbnail.url})`);
    }
    if (app.cover_image) {
      images.push(`[Cover](${CDNEndpoints.APP_ICON(app.id, app.cover_image)}?size=4096)`);
    }
    if (app.splash) {
      images.push(`[Splash](${CDNEndpoints.APP_ICON(app.id, app.splash)}?size=4096)`);
    }

    const links = [];
    if (app.terms_of_service_url) {
      links.push(`[Terms of Service](${app.terms_of_service_url})`);
    }
    if (app.privacy_policy_url) {
      links.push(`[Privacy Policy](${app.privacy_policy_url})`);
    }
    if (assets.length > 0) {
      links.push(`[Assets](${API_URL + APIEndpoints.APPLICATION_ASSETS(app.id)})`);
    }

    if (images.length > 0 || links.length > 0) {
      embed.fields.push({
        name: "\u200b",
        value: (images.join(" | ") + "\n" + links.join(" | ")).trim(),
        inline: false,
      });
    }

    if (assets.length > 0) {
      if (images.length == 0 && links.length == 0) {
        embed.fields.push({
          name: "\u200b",
          value: "\u200b",
          inline: false,
        });
      }

      const mappedAssets = assets.map(
        (asset) =>
          `[${asset.name.length > 32 ? asset.name.substring(0, 32) + "\u2026" : asset.name}](${CDNEndpoints.APP_ASSET(
            app.id,
            asset.id
          )})`
      );

      let left = "- " + mappedAssets.slice(0, Math.ceil(mappedAssets.length / 2)).join("\n- ");
      let right = "- " + mappedAssets.slice(Math.ceil(mappedAssets.length / 2), mappedAssets.length).join("\n- ");

      if (left.length > 1024 || right.length > 1024) {
        const linklessAssets = assets.map((asset) =>
          asset.name.length > 32 ? asset.name.substring(0, 32) + "\u2026" : asset.name
        );
        left = "- " + linklessAssets.slice(0, Math.ceil(linklessAssets.length / 2)).join("\n- ");
        right = "- " + linklessAssets.slice(Math.ceil(linklessAssets.length / 2), linklessAssets.length).join("\n- ");
      }

      if (left.length <= 1024 && right.length <= 1024) {
        embed.fields.push({
          name: `Assets (${assets.length})`,
          value: left,
          inline: true,
        });
        if (mappedAssets.length > 1)
          embed.fields.push({
            name: "\u200b",
            value: right,
            inline: true,
          });
      } else {
        const assetList = assets
          .map((asset) => (asset.name.length > 32 ? asset.name.substring(0, 31) + "\u2026" : asset.name))
          .join(", ");
        if (assetList.length <= 1024) {
          embed.fields.push({
            name: `Assets (${assets.length})`,
            value: assetList,
            inline: false,
          });
        } else {
          embed.fields.push({
            name: `Assets (${assets.length})`,
            value: "*Exceeds 1024 characters.*",
            inline: false,
          });
        }
      }
    }

    return {embed};
  } catch (error) {
    if (error.message === "Unknown Application") {
      try {
        const bot = await hf.bot.requestHandler.request("GET", APIEndpoints.USER(snowflake), true);
        if (bot) {
          return `Application has been deleted.\nBot user: ${formatUsername(bot)}`;
        } else {
          return "ID provided does not point to a valid application.";
        }
      } catch {
        return "ID provided does not point to a valid application.";
      }
    } else {
      return `:warning: Got error \`${safeString(error)}\``;
    }
  }
};
hf.registerCommand(appinfo);

const appinfoInteraction = new InteractionCommand("appinfo");
appinfoInteraction.helpText = "Get information on an application";
appinfoInteraction.options.id = {
  name: "id",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Application ID to get info for",
  required: true,
  default: "",
};
appinfoInteraction.callback = async function (interaction) {
  const id = this.getOption(interaction, "id");

  return appinfo.callback(interaction, id);
};
hf.registerCommand(appinfoInteraction);
