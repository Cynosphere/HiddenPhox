const Command = require("#lib/command.js");

const {APIEndpoints, UserFlags} = require("#util/dconstants.js");
const {
  RegExp: {Snowflake: SNOWFLAKE_REGEX},
} = require("#util/constants.js");
const {flagsFromInt, formatUsername} = require("#util/misc.js");

const _UserFlags = Object.entries(UserFlags).filter(([name]) => name != "NONE");
for (const set of _UserFlags) {
  const bits = Object.entries(set[1].toString("2").split("").reverse().map(Number));
  const index = bits.find(([index, bit]) => bit == 1)[0];
  set[1] = Number(index);
}
const UserFlagsMapped = Object.fromEntries(_UserFlags.map((x) => x.reverse()));

const REGEX_MENTION = /<@!?(\d+)>/;

const flagdump = new Command("flagdump");
flagdump.category = "utility";
flagdump.helpText = "Dumps Discord user flags.";
flagdump.usage = "[flags or user mention]";
flagdump.callback = async function (msg, line, [numOrMention], {id, list}) {
  if (!line || line == "") numOrMention = `<@${msg.author.id}>`;

  let num;
  try {
    num = BigInt(numOrMention);
  } catch {
    // noop
  }
  let out = "";
  if (list) {
    let allFlags = 0n;
    for (const index in UserFlagsMapped) {
      if (UserFlagsMapped[index] == undefined) continue;
      allFlags += 1n << BigInt(index);
    }
    out = `All flags:\n\`\`\`\n${flagsFromInt(allFlags, UserFlagsMapped)}\`\`\``;
  } else if (REGEX_MENTION.test(numOrMention) || SNOWFLAKE_REGEX.test(id)) {
    const targetId = id ?? numOrMention.match(REGEX_MENTION)?.[1];
    if (!targetId) return "Got null ID.";
    let user;
    try {
      user = await hf.bot.requestHandler.request("GET", APIEndpoints.USER(targetId), true);
    } catch (err) {
      if (err.code == 10013) return "Unknown user";
    }

    if (!user) {
      out = "Failed to get user.";
    } else {
      out = `\`${formatUsername(user)}\`'s public flags:\n\`\`\`\n${flagsFromInt(
        user.public_flags,
        UserFlagsMapped
      )}\`\`\``;
    }
  } else if (!Number.isNaN(num)) {
    out = `\`\`\`\n${flagsFromInt(num, UserFlagsMapped)}\`\`\``;
  } else {
    out = `\`${formatUsername(msg.author)}\`'s public flags:\n\`\`\`\n${flagsFromInt(
      msg.author.publicFlags,
      UserFlagsMapped
    )}\`\`\``;
  }

  if (out.length > 2000) {
    return {
      content: `Output too long to send in a message:`,
      attachments: [
        {
          file: out,
          filename: "message.txt",
        },
      ],
    };
  } else {
    return out;
  }
};
hf.registerCommand(flagdump);
