const Command = require("#lib/command.js");

const {APIEndpoints, CDNEndpoints} = require("#util/dconstants.js");
const {Icons} = require("#util/constants.js");

const {formatUsername, getDefaultAvatar} = require("#util/misc.js");
const {lookupUser} = require("#util/selection.js");

const avatar = new Command("avatar");
avatar.category = "utility";
avatar.helpText = "Get avatar of a user";
avatar.usage = "<user>";
avatar.addAlias("av");
avatar.addAlias("avi");
avatar.addAlias("pfp");
avatar.callback = async function (msg, line, [user], {server, guild}) {
  let id = msg.author?.id ?? msg.user?.id;
  const guildObj = msg.channel.guild ?? hf.bot.guilds.get(msg.guildID);

  if (server || guild) {
    if (!msg.guildID) {
      return "`--server/--guild` can only be used within guilds.";
    } else {
      if (!guildObj.icon) return "Server has no icon.";

      const url = CDNEndpoints.GUILD_ICON(guildObj.id, guildObj.icon);

      const res = await fetch(url, {method: "HEAD"});
      const mod = res.headers.get("last-modified");
      let description;
      if (mod) {
        const modDate = Math.floor(new Date(mod).getTime() / 1000);
        description = `Updated <t:${modDate}:R>`;
      }

      return {
        embeds: [
          {
            title: "Server Icon",
            description,
            url,
            image: {
              url,
            },
          },
        ],
      };
    }
  } else if (user) {
    const lookup = await lookupUser(msg, user);
    if (typeof lookup === "string") {
      return lookup;
    } else {
      id = lookup.id;
    }
  }

  let userObj;
  try {
    userObj = await hf.bot.requestHandler.request("GET", APIEndpoints.USER(id), true);
  } catch (err) {
    if (err.code == 10013) {
      return "User not found.";
    } else {
      return `${Icons.silk.error} Failed to get user:\n\`\`\`\n${err}\`\`\``;
    }
  }

  if (!userObj) return `${Icons.silk.error} Failed to get user without erroring.`;

  let member;
  if (guildObj) {
    member = guildObj.members.get(id);
  }

  const baseEmbed = {
    title: `Avatar for \`${formatUsername(userObj)}\``,
  };

  const defaultAvatar = getDefaultAvatar(id, userObj.discriminator ?? 0);
  const normalAvatar = userObj.avatar;
  const guildAvatar = member && member.avatar;

  const normalUrl = normalAvatar ? CDNEndpoints.USER_AVATAR(id, normalAvatar) : defaultAvatar;
  const guildUrl = guildAvatar && CDNEndpoints.GUILD_MEMBER_AVATAR(guildObj.id, id, guildAvatar);

  const res = await fetch(normalUrl, {method: "HEAD"});
  const mod = res.headers.get("last-modified");
  let modStr = "";
  if (mod && normalUrl != defaultAvatar) {
    const modDate = Math.floor(new Date(mod).getTime() / 1000);
    modStr = ` \u2022 Updated <t:${modDate}:R>`;
  }

  let modStrGuild = "";
  if (guildUrl) {
    const guildRes = await fetch(guildUrl, {method: "HEAD"});
    const guildMod = guildRes.headers.get("last-modified");
    if (guildMod) {
      const modDate = Math.floor(new Date(guildMod).getTime() / 1000);
      modStrGuild = ` \u2022 Updated <t:${modDate}:R>`;
    }
  }

  baseEmbed.description =
    `[Normal avatar](${normalUrl})${modStr}` + (guildAvatar ? `\n[Guild avatar](${guildUrl})${modStrGuild}` : "");
  baseEmbed.url = normalUrl;

  const guildEmbed = {...baseEmbed};
  baseEmbed.image = {url: normalUrl};
  guildEmbed.image = {url: guildUrl};

  return {
    embeds: [baseEmbed, guildAvatar && guildEmbed].filter((x) => x != null),
  };
};
hf.registerCommand(avatar);
