const Command = require("#lib/command.js");

const sharp = require("sharp");

const {ActivityTypeNames, CDNEndpoints, Games, HangStatusStrings, HANG_STATUS_ICONS} = require("#util/dconstants.js");
const {Icons} = require("#util/constants.js");
const {formatUsername} = require("#util/misc.js");
const {lookupUser} = require("#util/selection.js");
const {formatTime} = require("#util/time.js");

const HangStatusImages = {};
(async () => {
  for (const key of Object.keys(HANG_STATUS_ICONS)) {
    const svg = await fetch(HANG_STATUS_ICONS[key])
      .then((res) => res.arrayBuffer())
      .then((b) => Buffer.from(b));
    HangStatusImages[key] = await sharp(svg, {density: 2400}).resize(128).toBuffer();
  }
})();

const NOWPLAYING_BAR_LENGTH = 30;

function fixMediaProxyURL(url) {
  if (url.includes("media.discordapp.net")) {
    if (url.includes("/external/")) {
      const split = url.replace("https://media.discordapp.net/external/", "").split("/");
      split.shift();

      let query = "";
      if (split[0].startsWith("%")) {
        query = decodeURIComponent(split.shift());
      }
      split[0] = split[0] + ":/";

      url = split.join("/") + query;
    } else {
      url = url.replace("media.discordapp.net", "cdn.discordapp.com");
    }
  }

  return url;
}

const presence = new Command("presence");
presence.category = "utility";
presence.helpText = "Get presences of a user.";
presence.usage = "<user>";
presence.addAlias("status");
presence.callback = async function (msg, line) {
  if (!msg.guildID) return "Can only be used in guilds.";

  let target;
  if (line) {
    const user = await lookupUser(msg, line);
    if (typeof user === "string") {
      return user;
    } else {
      let member = user;
      const guild = msg.channel.guild ?? hf.bot.guilds.get(msg.guildID);
      if (guild) {
        if (guild.members.has(user.id)) {
          member = guild.members.get(user.id);
        } else {
          const fetched = await guild.fetchMembers({
            userIDs: [user.id],
          });
          member = fetched[0];
        }
        target = member;
      }
    }
  } else {
    target = msg.member;
  }

  if (target) {
    if (!target.clientStatus) return `**${formatUsername(target)}** is offline.`;

    const icons = [];
    for (const platform of Object.keys(target.clientStatus)) {
      const status = target.clientStatus[platform];
      if (status == "offline") continue;

      icons.push(Icons.presence[platform][status]);
    }

    const embeds = [];
    const files = [];

    for (const index in target.activities) {
      const activity = target.activities[index];
      if (activity.type == 4) {
        const embed = {};

        if (activity.emoji) {
          if (activity.emoji.id) {
            const url = CDNEndpoints.EMOJI(activity.emoji.id, activity.emoji.animated);
            embed.author = {
              url,
              icon_url: url,
              name: activity.state ?? "\u200b",
            };
          } else {
            embed.title = `${activity.emoji.name} ${activity.state ?? ""}`;
          }
        } else {
          embed.title = activity.state ?? "";
        }

        embeds.push(embed);
      } else if (activity.type == 6) {
        const embed = {};

        embed.title = "Hang Status";
        embed.description = `Right now I'm \u2013\n**${
          activity.state == "custom" ? activity.details : HangStatusStrings[activity.state]
        }**`;

        if (activity.emoji) {
          embed.thumbnail = {
            url: CDNEndpoints.EMOJI(activity.emoji.id, activity.emoji.animated),
          };
        } else {
          files.push({
            contents: HANG_STATUS_ICONS[activity.state],
            name: `${activity.state}.png`,
          });
          embed.thumbnail = {
            url: `attachment://${activity.state}.png`,
          };
        }

        embeds.push(embed);
      } else {
        const embed = {
          title: `${ActivityTypeNames[activity.type]} **${activity.name}**`,
          fields: [],
        };
        const descLines = [];
        if (activity.type == 2) {
          if (activity.details) {
            let details = activity.details;
            if (activity.name == "Spotify" && activity.sync_id) {
              details = `[${details}](https://open.spotify.com/track/${activity.sync_id})`;
            }
            descLines.push(`**${details}**`);
          }
          if (activity.state) {
            let stateLine = activity.state;
            if (activity.name == "Spotify") stateLine = "by " + stateLine.split("; ").join(", ");
            descLines.push(stateLine);
          }
          if (activity.assets?.large_text) {
            let albumLine = activity.assets.large_text;
            if (activity.name == "Spotify") albumLine = "on " + albumLine;

            if (activity.party?.size) {
              albumLine += ` (${activity.party.size[0]} of ${activity.party.size[1]})`;
            }
            descLines.push(albumLine);
          }
        } else {
          if (activity.details) descLines.push(activity.details);
          if (activity.state) {
            let stateLine = activity.state;
            if (activity.party?.size) {
              stateLine += ` (${activity.party.size[0]} of ${activity.party.size[1]})`;
            }
            descLines.push(stateLine);
          }
        }

        if (activity.timestamps) {
          if (activity.timestamps.start && !activity.timestamps.end) {
            descLines.push(formatTime(Date.now() - activity.timestamps.start).replaceAll(":", "\\:") + " elapsed");
            descLines.push(
              `-# <t:${Math.floor(activity.timestamps.start / 1000)}:R> \u2022 ${activity.timestamps.start}`
            );
          } else if (!activity.timestamps.start && activity.timestamps.end) {
            descLines.push(formatTime(activity.timestamps.end - Date.now()).replaceAll(":", "\\:") + " remaining");
            descLines.push(`-# <t:${Math.floor(activity.timestamps.end / 1000)}:R> \u2022 ${activity.timestamps.end}`);
          } else if (activity.timestamps.start != null && activity.timestamps.end != null) {
            const position = Date.now() - activity.timestamps.start;
            const length = activity.timestamps.end - activity.timestamps.start;

            const timeEnd = formatTime(length);
            const timePos = formatTime(position);

            const progress = position >= length ? 1 : position / length;
            const barLength = Math.round(progress * NOWPLAYING_BAR_LENGTH);

            const bar = `\`[${"=".repeat(barLength)}${" ".repeat(NOWPLAYING_BAR_LENGTH - barLength)}]\``;
            const time = `\`${timePos}${" ".repeat(
              NOWPLAYING_BAR_LENGTH + 2 - timePos.length - timeEnd.length
            )}${timeEnd}\``;

            descLines.push(bar);
            descLines.push(time);
          }
        }

        if (activity.assets?.large_text && activity.type != 2) {
          embed.fields.push({
            name: "Large Text",
            value: activity.assets.large_text,
          });
        }

        if (activity.assets?.small_text) {
          embed.fields.push({
            name: "Small Text",
            value: activity.assets.small_text,
          });
        }

        if (activity.application_id) {
          embed.fields.push({
            name: "Application ID",
            value: `\`${activity.application_id}\``,
          });
        }

        embed.description = descLines.join("\n");

        if (activity.assets) {
          if (activity.assets.large_image != null) {
            const image_links = [];

            let largeUrl;
            if (activity.assets.large_image.startsWith("mp:")) {
              largeUrl = activity.assets.large_image.replace("mp:", "https://media.discordapp.net/");
            } else if (activity.assets.large_image.startsWith("spotify:")) {
              largeUrl = activity.assets.large_image.replace("spotify:", "https://i.scdn.co/image/");
            } else {
              largeUrl = CDNEndpoints.APP_ASSET(activity.application_id, activity.assets.large_image);
            }

            image_links.push(`[Large Image](${fixMediaProxyURL(largeUrl)})`);

            let smallUrl;
            if (activity.assets.small_image != null) {
              if (activity.assets.small_image.startsWith("mp:")) {
                smallUrl = activity.assets.small_image.replace("mp:", "https://media.discordapp.net/");
              } else if (activity.assets.small_image.startsWith("spotify:")) {
                smallUrl = activity.assets.small_image.replace("spotify:", "https://i.scdn.co/image/");
              } else {
                smallUrl = CDNEndpoints.APP_ASSET(activity.application_id, activity.assets.small_image);
              }

              image_links.push(`[Small Image](${fixMediaProxyURL(smallUrl)})`);
            }

            const largeImage = await fetch(largeUrl)
              .then((res) => res.arrayBuffer())
              .then((b) => Buffer.from(b));
            const presenceImage = sharp(largeImage).resize(60, 60);
            if (smallUrl) {
              const smallImage = await fetch(smallUrl)
                .then((res) => res.arrayBuffer())
                .then((b) => Buffer.from(b));
              const smallImageBuffer = await sharp(smallImage).resize(20, 20).toBuffer();

              presenceImage.composite([
                {
                  input: smallImageBuffer,
                  gravity: "southeast",
                },
              ]);
            }

            files.push({
              contents: await presenceImage.toBuffer(),
              name: `${index}.png`,
            });
            embed.thumbnail = {
              url: `attachment://${index}.png`,
            };
            embed.fields.push({
              name: "\u200b",
              value: image_links.join("\u3000\u3000"),
            });
          } else if (!activity.assets.large_image && activity.assets.small_image != null) {
            let smallUrl;
            if (activity.assets.small_image.startsWith("mp:")) {
              smallUrl = activity.assets.small_image.replace("mp:", "https://media.discordapp.net/");
            } else if (activity.assets.small_image.startsWith("spotify:")) {
              smallUrl = activity.assets.small_image.replace("spotify:", "https://i.scdn.co/image/");
            } else {
              smallUrl = CDNEndpoints.APP_ASSET(activity.application_id, activity.assets.small_image);
            }

            const smallImage = await fetch(smallUrl)
              .then((res) => res.arrayBuffer())
              .then((b) => Buffer.from(b));
            const presenceImage = await sharp(smallImage).resize(40, 40).toBuffer();

            files.push({
              contents: presenceImage,
              name: `${index}.png`,
            });
            embed.thumbnail = {
              url: `attachment://${index}.png`,
            };

            embed.fields.push({
              name: "\u200b",
              value: `[Small Image](${fixMediaProxyURL(smallUrl)})`,
            });
          }
        }

        if (activity.application_id && !activity.assets?.large_image && !activity.assets?.small_image) {
          const game = Games.find((game) => game.id == activity.application_id);
          if (game?.icon) {
            embed.thumbnail = {
              url: `${CDNEndpoints.APP_ICON(game.id, game.icon)}?size=40&keep_aspect_ratio=false`,
            };
            embed.fields.push({
              name: "\u200b",
              value: `[App Icon](${embed.thumbnail.url.replace("size=40&", "")})`,
            });
          }
        }

        embeds.push(embed);
      }
    }

    return {
      content: `Presence for **${formatUsername(target)}**: ${icons.join(" ")}`,
      embeds,
      files,
    };
  } else {
    return ":warning: Could not get user???";
  }
};
hf.registerCommand(presence);
