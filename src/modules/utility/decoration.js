const Command = require("#lib/command.js");

const {APIEndpoints, CDNEndpoints} = require("#util/dconstants.js");

const {formatUsername} = require("#util/misc.js");
const {lookupUser} = require("#util/selection.js");

const decoration = new Command("decoration");
decoration.category = "utility";
decoration.helpText = "Get decoration of a user";
decoration.usage = "<user>";
decoration.addAlias("decor");
decoration.callback = async function (msg, line, [user]) {
  let id = msg.author.id;

  if (user) {
    const lookup = await lookupUser(msg, user);
    if (typeof lookup === "string") {
      return lookup;
    } else {
      id = lookup.id;
    }
  }

  const userObj = await hf.bot.requestHandler.request("GET", APIEndpoints.USER(id), true);

  let decor, decorRes;
  try {
    decorRes = await fetch(`https://decor.fieryflames.dev/api/users/${id}/decoration`, {
      headers: {
        "User-Agent": "HiddenPhox/decoration (https://gitdab.com/Cynosphere/HiddenPhox)",
      },
    }).then((res) => res.json());

    if (decorRes.presetId) {
      // horror
      const presets = await fetch(`https://decor.fieryflames.dev/api/decorations/presets`, {
        headers: {
          "User-Agent": "HiddenPhox/decoration (https://gitdab.com/Cynosphere/HiddenPhox)",
        },
      }).then((res) => res.json());

      decorRes.preset = presets.find((p) => p.id === decorRes.presetId);
    }

    decor = `https://decorcdn.fieryflames.dev/${decorRes.animated ? "a_" : ""}${decorRes.hash}.png`;
  } catch {
    // noop
  }

  if (!userObj.avatar_decoration_data && !decor) return "This user does not have a decoration.";

  const normalUrl =
    userObj.avatar_decoration_data && CDNEndpoints.AVATAR_DECORATION(userObj.avatar_decoration_data.asset);

  const baseEmbed = {
    title: `Decoration for \`${formatUsername(userObj)}\``,
  };

  baseEmbed.fields = [];

  if (normalUrl) {
    const listing = await hf.bot.requestHandler
      .request("GET", APIEndpoints.STORE_PUBLISHED_LISTING(userObj.avatar_decoration_data.sku_id), true)
      .catch(() => {});

    baseEmbed.fields.push({
      name: `Discord ${userObj.avatar_decoration_data.asset.startsWith("a_") ? "(Animated)" : ""}`,
      value: `**${
        listing?.sku
          ? `[${listing?.sku?.name}](https://discord.com/shop#itemSkuId=${userObj.avatar_decoration_data.sku_id})`
          : "Unknown"
      }** (\`${userObj.avatar_decoration_data.sku_id}\`)\n[Image](${normalUrl})`,
      inline: true,
    });
  }

  if (decor) {
    baseEmbed.fields.push({
      name: `Decor ${decor.indexOf("/a_") > -1 ? "(Animated)" : ""}`,
      value: `**${decorRes.alt}**\n${
        decorRes.preset ? `\u3000from preset **${decorRes.preset.name}**\n` : ""
      }[Image](${decor})`,
      inline: true,
    });
  }

  baseEmbed.url = normalUrl || decor;

  const decorEmbed = {...baseEmbed};
  baseEmbed.image = {url: normalUrl || decor};
  decorEmbed.image = {url: decor};

  return {
    embeds: [normalUrl && baseEmbed, decor && decorEmbed].filter((x) => x != null),
  };
};
hf.registerCommand(decoration);
