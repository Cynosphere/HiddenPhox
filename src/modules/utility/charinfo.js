const Command = require("#lib/command.js");
const InteractionCommand = require("#lib/interactionCommand.js");

const {ApplicationCommandOptionTypes} = require("#util/dconstants.js");
const {getNamesFromString} = require("#util/unicode.js");

const charinfo = new Command("charinfo");
charinfo.category = "utility";
charinfo.helpText = "Get information about a set of characters.";
charinfo.usage = "[characters]";
charinfo.addAlias("char");
charinfo.callback = async function (msg, line) {
  if (!line || line == "") {
    if (msg.messageReference?.messageID) {
      const reply = await msg.channel.getMessage(msg.messageReference.messageID);
      line = reply.content;
    } else {
      return "Arguments or reply required.";
    }
  }

  const names = await getNamesFromString(line);
  const chars = [...line];
  const lines = names
    .map(
      ([code, name], index) =>
        `[\`\\u${code}${
          chars[index].length > 1
            ? ` (${chars[index]
                .split("")
                .map((c) => `\\u${c.codePointAt().toString(16)}`)
                .join("")})`
            : ""
        }\`](<http://www.fileformat.info/info/unicode/char/${code}>): ${name} - ${chars[index]
          .replace("_", "\\_")
          .replace("`", "\\`")
          .replace("*", "\\*")
					.replace("<", "\\<")
					.replace(">", "\\>")}`
    )
    .join("\n");

  if (lines.length > 2000) {
    return {
      content: "Output too long:",
      attachments: [
        {
          file: lines,
          filename: "message.txt",
        },
      ],
    };
  } else {
    return lines;
  }
};
hf.registerCommand(charinfo);

const charinfoInteraction = new InteractionCommand("charinfo");
charinfoInteraction.helpText = "Get information about a set of characters.";
charinfoInteraction.options.content = {
  name: "content",
  type: ApplicationCommandOptionTypes.STRING,
  description: "Characters to get info for",
  required: true,
  default: "",
};
charinfoInteraction.callback = async function (interaction) {
  const content = this.getOption(interaction, "content");

  return charinfo.callback(interaction, content);
};
hf.registerCommand(charinfoInteraction);

