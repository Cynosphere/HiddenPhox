const Command = require("#lib/command.js");

const {APIEndpoints, CDNEndpoints} = require("#util/dconstants.js");

const {formatUsername} = require("#util/misc.js");
const {lookupUser} = require("#util/selection.js");

const banner = new Command("banner");
banner.category = "utility";
banner.helpText = "Get banner of a user";
banner.usage = "<user>";
banner.callback = async function (msg, line, [user], {server, guild}) {
  let id = msg.author?.id ?? msg.user?.id;

  if (server || guild) {
    if (!msg.guildID) {
      return "`--server/--guild` can only be used within guilds.";
    } else {
      const guildObj = msg.channel.guild ?? hf.bot.guilds.get(msg.guildID);
      if (!guildObj.banner) return "This guild does not have a banner.";

      const url = CDNEndpoints.BANNER(guildObj.id, guildObj.banner);

      const res = await fetch(url, {method: "HEAD"});
      const mod = res.headers.get("last-modified");
      let description;
      if (mod) {
        const modDate = Math.floor(new Date(mod).getTime() / 1000);
        description = `Updated <t:${modDate}:R>`;
      }

      return {
        embeds: [
          {
            title: "Server Banner",
            description,
            url,
            image: {
              url,
            },
          },
        ],
      };
    }
  } else if (user) {
    const lookup = await lookupUser(msg, user);
    if (typeof lookup === "string") {
      return lookup;
    } else {
      id = lookup.id;
    }
  }

  const userObj = await hf.bot.requestHandler.request("GET", APIEndpoints.USER(id), true);

  let memberObj;
  if (msg.guildID) {
    memberObj = await hf.bot.requestHandler.request("GET", APIEndpoints.GUILD_MEMBER(msg.guildID, id), true);
  }

  if (!userObj.banner && !memberObj?.banner) return "This user does not have a banner.";

  const url = userObj.banner && CDNEndpoints.BANNER(userObj.id, userObj.banner);
  const guildUrl = memberObj?.banner && CDNEndpoints.GUILD_MEMBER_BANNER(msg.guildID, userObj.id, memberObj.banner);

  const res = await fetch(url, {method: "HEAD"});
  const mod = res.headers.get("last-modified");
  let modStr;
  if (mod) {
    const modDate = Math.floor(new Date(mod).getTime() / 1000);
    modStr = `Updated <t:${modDate}:R>`;
  }

  let modStrGuild;
  if (guildUrl) {
    const guildRes = await fetch(guildUrl, {method: "HEAD"});
    const guildMod = guildRes.headers.get("last-modified");
    if (guildMod) {
      const modDate = Math.floor(new Date(guildMod).getTime() / 1000);
      modStrGuild = `Updated <t:${modDate}:R>`;
    }
  }

  return {
    embeds: [
      url && {
        title: `Banner for \`${formatUsername(userObj)}\``,
        description: modStr,
        url,
        image: {
          url,
        },
      },
      guildUrl && {
        title: `Server banner for \`${formatUsername(userObj)}\``,
        description: modStrGuild,
        url: guildUrl,
        image: {
          url: guildUrl,
        },
      },
    ].filter((x) => !!x),
  };
};
hf.registerCommand(banner);
