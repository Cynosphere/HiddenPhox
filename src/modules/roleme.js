const Command = require("#lib/command.js");

const {selectionMessage} = require("#util/selection.js");

hf.database.run(
  "CREATE TABLE IF NOT EXISTS roleme (guild TEXT NOT NULL PRIMARY KEY, roles TEXT NOT NULL) WITHOUT ROWID"
);

function setRoles(id, roles) {
  return new Promise((resolve, reject) => {
    hf.database.run(
      "REPLACE INTO roleme VALUES ($key,$value)",
      {
        $value: JSON.stringify(roles),
        $key: id,
      },
      (err) => {
        if (err == null) {
          resolve(true);
        } else {
          reject(err);
        }
      }
    );
  });
}

function getRoles(id) {
  return new Promise((resolve, reject) => {
    hf.database.get(
      "SELECT roles FROM roleme WHERE guild = $key",
      {
        $key: id,
      },
      (err, row) => {
        if (err == null) {
          resolve(JSON.parse(row?.roles ?? "[]"));
        } else {
          reject(err);
        }
      }
    );
  });
}

async function lookupRole(msg, str, filter) {
  let roles;
  if (filter) {
    roles = msg.channel.guild.roles.filter(filter).values();
  } else {
    roles = msg.channel.guild.roles.values();
  }

  const selection = [];
  for (const role of roles) {
    if (role.name.toLowerCase() == str.toLowerCase()) {
      return role;
    } else if (role.name.toLowerCase().indexOf(str.toLowerCase()) > -1) {
      selection.push({
        value: role,
        key: role.id,
        display: role.name,
      });
    }
  }

  selection.sort((a, b) => a.display - b.display);

  if (selection.length == 0) {
    return "No results";
  } else if (selection.length == 1) {
    return selection[0].value;
  } else {
    selection.splice(20);

    try {
      return await selectionMessage(msg, "Multiple roles found, please pick from this list:", selection);
    } catch (out) {
      return out;
    }
  }
}

const roleme = new Command("roleme");
roleme.category = "guildutility";
roleme.helpText = "Self assignable roles";
roleme.usage = "help";
roleme.callback = async function (msg, line, args) {
  if (!msg.guildID) return "This command can only be used in guilds.";

  const subcommand = args.shift();
  const argStr = args.join(" ");

  switch (subcommand) {
    case "add": {
      if (!msg.channel.permissionsOf(msg.author.id).has("manageRoles")) {
        return "You do not have `Manage Roles` permission.";
      }
      if (!msg.channel.permissionsOf(hf.bot.user.id).has("manageRoles")) {
        return "I do not have `Manage Roles` permission.";
      }
      if (msg.author.bot) return "Zero-width space your say command.";

      const role = await lookupRole(msg, argStr);
      if (role === "No results") return role;

      const roles = await getRoles(msg.guildID);
      if (roles.includes(role.id)) {
        return "Role already assignable.";
      } else {
        roles.push(role.id);
        await setRoles(msg.guildID, roles);

        return `Added **${role.name}** to assignable roles.`;
      }
    }
    case "delete": {
      if (!msg.channel.permissionsOf(msg.author.id).has("manageRoles")) {
        return "You do not have `Manage Roles` permission.";
      }
      if (!msg.channel.permissionsOf(hf.bot.user.id).has("manageRoles")) {
        return "I do not have `Manage Roles` permission.";
      }
      if (msg.author.bot) return "Zero-width space your say command.";

      const roles = await getRoles(msg.guildID);

      const role = await lookupRole(msg, argStr, (role) => roles.includes(role.id));
      if (role === "No results") return role;

      if (!roles.includes(role.id)) {
        return "Role not in assignable roles.";
      } else {
        await setRoles(
          msg.guildID,
          roles.filter((r) => r !== role.id)
        );

        return `Removed **${role.name}** from assignable roles.`;
      }
    }
    case "list": {
      const roles = await getRoles(msg.guildID);

      if (roles.length === 0) {
        return "No roles are assignable.";
      } else {
        return {
          embeds: [
            {
              title: "Assignable Roles",
              description: roles.map((r) => `<@&${r}>`).join("\n"),
            },
          ],
        };
      }
    }
    case "give": {
      if (!msg.channel.permissionsOf(hf.bot.user.id).has("manageRoles")) {
        return "I do not have `Manage Roles` permission.";
      }
      if (msg.author.bot) return "Zero-width space your say command.";

      const roles = await getRoles(msg.guildID);

      const role = await lookupRole(msg, argStr, (role) => roles.includes(role.id));
      if (role === "No results") return role;

      if (!roles.includes(role.id)) {
        return "Role not in assignable roles.";
      } else if (msg.member.roles.includes(role.id)) {
        return "You already have this role.";
      } else {
        await msg.member.addRole(role.id, "Self assigned role");
        return {reaction: "\uD83D\uDC4D"};
      }
    }
    case "take": {
      if (!msg.channel.permissionsOf(hf.bot.user.id).has("manageRoles")) {
        return "I do not have `Manage Roles` permission.";
      }
      if (msg.author.bot) return "Zero-width space your say command.";

      const roles = await getRoles(msg.guildID);

      const role = await lookupRole(
        msg,
        argStr,
        (role) => roles.includes(role.id) && msg.member.roles.includes(role.id)
      );
      if (role === "No results") return role;

      if (!roles.includes(role.id)) {
        return "Role not in assignable roles.";
      } else if (!msg.member.roles.includes(role.id)) {
        return "You do not have this role.";
      } else {
        await msg.member.removeRole(role.id, "Self unassigned role");
        return {reaction: "\uD83E\uDEF3"};
      }
    }
    case "help":
      return `**__RoleMe Subcommands__**
\u2022 \`<role name>\` - Toggles assignment of a role.
\u2022 \`give\` - Assigns yourself a role.
\u2022 \`take\` - Unassigns yourself a role.
\u2022 \`list\` - Lists assignable roles.

**[Manage Roles]**
\u2022 \`add\` - Adds a new role to assignable roles.
\u2022 \`delete\` - Deletes a new role from assignable roles.`;
    default: {
      if (!msg.channel.permissionsOf(hf.bot.user.id).has("manageRoles")) {
        return "I do not have `Manage Roles` permission.";
      }
      if (msg.author.bot) return "Zero-width space your say command.";

      const roles = await getRoles(msg.guildID);

      const role = await lookupRole(msg, (subcommand + " " + argStr).trim(), (role) => roles.includes(role.id));
      if (role === "No results") return role;

      if (!roles.includes(role.id)) return "Role not in assignable roles.";

      if (msg.member.roles.includes(role.id)) {
        await msg.member.removeRole(role.id, "Self unassigned role");
        return {reaction: "\uD83E\uDEF3"};
      } else {
        await msg.member.addRole(role.id, "Self assigned role");
        return {reaction: "\uD83D\uDC4D"};
      }
    }
  }
};
hf.registerCommand(roleme);
