const {Client, Collection} = require("oceanic.js");
const logger = require("./lib/logger.js");
const fs = require("fs");
const {resolve} = require("path");
const sqlite3 = require("sqlite3");

const config = require("../config.json");
const apikeys = require("../apikeys.json");
const Command = require("./lib/command.js");
const events = require("./lib/events.js");
const timer = require("./lib/timer.js");

const bot = new Client({
  auth: config.token,
  defaultImageFormat: "png",
  defaultImageSize: 1024,
  gateway: {
    intents: ["ALL"],
  },
});

const commands = new Collection();

const database = new sqlite3.Database(resolve(__dirname, "..", "database.db"));

function registerCommand(cmdObj) {
  if (cmdObj instanceof Command) {
    commands.set(cmdObj.name, cmdObj);
    const aliases = cmdObj.getAliases();
    logger.info(
      "hf:cmd",
      `Registered command '${cmdObj.name}'${
        aliases.length > 0 ? ` (aliases: ${aliases.join(", ")})` : ""
      }`
    );
  }
}

global.hf = {
  bot,
  config,
  apikeys,
  commands,
  registerCommand,
  events,
  timer,
  database,
};

const CommandDispatcher = require("./lib/commandDispatcher.js");

for (const file of fs.readdirSync(resolve(__dirname, "modules"))) {
  require(resolve(__dirname, "modules", file));
  logger.info("hf:modules", `Loaded module: '${file}'`);
}

bot.on("messageCreate", async (msg) => {
  await CommandDispatcher(msg);
});
bot.on("messageUpdate", (msg, oldMsg) => {
  const oneDay = Date.now() - 86400000;
  if (
    msg.timestamp > oneDay &&
    !msg.hasRan &&
    oldMsg &&
    oldMsg.content !== msg.content
  ) {
    CommandDispatcher(msg);
  }
});

bot.once("ready", async () => {
  logger.info("hf:main", "Connected to Discord.");
  logger.info("hf:main", `Logged in as: ${bot.user.tag} (${bot.user.id})`);

  const channel = await bot.users.get(config.owner_id)?.createDM();
  if (channel) {
    channel.createMessage({
      content: "<:ms_tick:503341995348066313> Loaded HiddenPhox.",
    });
  }
  bot.on("ready", () => {
    logger.info("hf:main", "Reconnected to Discord.");
  });
});

bot.on("error", (err) => {
  logger.error("hf:main", "Catching error: " + err);
});
bot.on("warn", (err) => {
  logger.warn("hf:main", "Catching warn: " + err);
});

bot.on("shardDisconnect", (err, id) => {
  logger.verbose("hf:shard", `Disconnecting from shard ${id}: ${err}`);
});
bot.on("shardResume", (id) => {
  logger.verbose("hf:shard", "Resuming on shard " + id);
});
bot.on("shardPreReady", (id) => {
  logger.verbose("hf:shard", `Shard ${id} getting ready`);
});
bot.on("shardReady", (id) => {
  logger.verbose("hf:shard", `Shard ${id} ready`);
});
bot.on("unknown", (packet, id) => {
  logger.verbose(
    "hf:main",
    `Shard ${id} caught unknown packet: ${JSON.stringify(packet)}`
  );
});

bot.connect();
